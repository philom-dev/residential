import React from "react";

const AllprojectsContext = React.createContext({
    AllProjects: [],
    setAllProjects: () => { }
});

export default AllprojectsContext;