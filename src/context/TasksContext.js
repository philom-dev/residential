import React from "react";

const TasksContext = React.createContext({
    allTasksContext: [],
    setAllTasksContext: () => { }
});

export default TasksContext;