import React, { useEffect } from 'react';
import Overview from './components/Overview';
import Navbar from './components/Navbar';
import { useState } from 'react';
import Task from "./components/Task";
import "./App.css"
import AllTasks from './components/Issues/tasks';
import IssueAndExpense from './components/Issues/IssueAndExepense';
import SignIn from './components/Auth/Signin';
import AllprojectsContext from './context/AllprojectsContext';
import SelectedProjectContext from './context/selectdProjectContext';
import updatesContext from './context/UpdatesContext';
import CountContext from './context/CountContext';
import axios from 'axios';
import ProjectTaskContext from './context/ProjectTaskContext';
import TasksContext from './context/TasksContext';
import { RefreshBtn } from './components/RefreshBtn.style';
import Documents from './components/Documents';
import { Redirect, Route, Switch, withRouter } from 'react-router';
import FolderDetails from './components/FolderDetails';
import NotFound from './components/NotFound';
import CreateProject from './components/CreateProject';
import CreateTask from './components/CreateTask';
import AllFiles from './components/AllFiles';
import AllMedia from './components/AllMedia';
import CustomCalendar from './components/CustomCalendar';
import DailyProgressReport from './components/DailyProgressReport';
import IssuesAndExpensesContext from './context/IssuesAndExpensesContext';
import TaskChart from './components/TaskChart';
import Settings from './components/Settings';
import Analysis from './components/Analysis';
import AnalysisDetails from './components/AnalysisDetails';
import { useLocation } from 'react-router-dom';
import notification2 from "./resources/notification2.aac"
import Module from './components/Module';
import { initializeApp } from "firebase/app";
import { getAnalytics, logEvent} from "firebase/analytics";
import { getMessaging, getToken, onMessage} from "firebase/messaging";
import 'intro.js/introjs.css';
import introJs from "intro.js"
import { Steps, Hints } from 'intro.js-react'; 
import Ratelist from "./components/Ratelist"
import RFI from "./components/RFI"
import RFIDetails from './components/RFIDetails';
import Modelling from './components/Modelling';
import CostSaved from './components/CostSaved';
import CostLeakages from './components/CostLeakages';
import Reports from './components/Reports';
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css";
import VisualProgress from './components/VisualProgress';
import CCTV from './components/cctv';

function App() {


  let noti = [
    { title: "Work Done", key: "workdone", body: "KCDPL has completed shuttering of Plot 153A Basement" },
    { title: "Issue", key: "issue", body: "Perfect raised an issue for Basement roof structure" },
    { title: "Work Done", key: "workdone", body: "Aryan has completed reinforcement of Plot 13 Foundation" },
    { title: "Work Done", key: "workdone", body: "Perfect started the shuttering of Plot 7 Foundation" },
    { title: "Work Done", key: "workdone", body: "NDCC has completed concreting of Plot 103 Stilt" },
  ]
  let [ updates, setUpdates ] = useState(noti);
  let [count, setCount] = useState(0);
  const [tasks, setTasks] = useState([])
  const updatesContextt = {updates, setUpdates}
  const countContextt = {count, setCount}
  const tasksContext = {tasks, setTasks}
  

  const firebaseConfig = {
    apiKey: "AIzaSyAGLut_qi52Y5eZcx8imtphVOu3j6V2bSo",
    authDomain: "temlin-b29c9.firebaseapp.com",
    projectId: "temlin-b29c9",
    storageBucket: "temlin-b29c9.appspot.com",
    messagingSenderId: "1048537949060",
    appId: "1:1048537949060:web:4ec45cad8c547c5641f3d1",
    measurementId: "G-6R9JMEM3MV"
  };

   // for notification recieved when web is open :
   useEffect(()=>{
    const app = initializeApp(firebaseConfig);
    const messaging = getMessaging();
    
    getToken(messaging,{vapidKey: "BD3RTRx85gq4abImu99JpJbH8BdfyYwHrzXq79k1SzuJpQwh-cPOycEbFzWIo5FhOX7JwAAzcYR8faUqJ24sJ4E"}).then(async (token) => {
      if(token){
        console.log(token);
        const options = {
          headers: {
              'Content-type': 'application/json',
              "Authorization": 'Bearer ' + localStorage.getItem('token')
          }
        };
        let data = {
          token: token
        }
        let res = await axios.post(process.env.REACT_APP_DURL + 'tokenregister',data,options)
      } else {
        console.log('token not available');
      }
    }).catch(err => {
      console.log(err, "Error occured while getting token");
    })

    function playSound() {
      document.addEventListener('load',() => {
        document.querySelector('#sound').play()
      })
    }

    // onMessage(messaging,(payload) => {
      // document.querySelector('#updates').innerHTML = "message from foreground"
    //   document.querySelector('#sound').play()
    //   console.log("Foreground Message received. ", payload);
    // });
},[]);
navigator.serviceWorker.register('/firebase-messaging-sw.js')

useEffect(()=>{
  navigator.serviceWorker.addEventListener('message', function (event) {
  console.log(event);
    document.querySelector('#sound').muted = false
    document.querySelector('#sound').play()
  let data = {
    title: event.data.notification.title,
    body: event.data.notification.body,
    key: event.data.data?.title,
    time: new Date()
  }
  new Notification(data.title, {
    body: data.body,
  })
  

  setUpdates((n)=>[data,...n])
})

},[])

  const [ AllProjects, setAllProjects ] = useState([])
  const [ ProjectTasks, setProjectTasks ] = useState([])
  const [ SelectedProject, setSelectedProject ] = useState()
  let [ overview, showOverview ] = useState(true);
  let [ task, showTask ] = useState(false);
  let [ isAllTasks, showAllTask ] = useState(false);
  let [ isIssueexp, setissuesexp ] = useState(false);
  let [ Authorized, setAuthorized ] = useState(false);
  let [docs, setDocs] = useState(false);
  const [ isIssue, setIssue ] = useState(true)
  let user = localStorage.getItem('user')
  const ProjectContext = { AllProjects, setAllProjects }
  const TaskContext = { ProjectTasks, setProjectTasks }
  const SelectedProjects = { SelectedProject, setSelectedProject}
  const IssuesAndExepensesCxt = {isIssue, setIssue}
  let location = useLocation();

  useEffect(async () => {
    try {
      const res = localStorage.getItem('token')
      console.log("🚀 ~ file: App.js ~ line 51 ~ useEffect ~ res", res)
      if (res) {
        setAuthorized(true)
      }
      else {
        setAuthorized(false)
      }
      const userid = localStorage.getItem('userid')
      const options = {
        headers: {
            'Content-type': 'application/json',
            "Authorization": 'Bearer ' + localStorage.getItem('token')
        }
    };
      const resdata = await axios.get(process.env.REACT_APP_DURL + 'getUserProject/' + userid, options)
      setAllProjects(resdata.data.ProjectId)
      setSelectedProject(resdata?.data?.ProjectId[ 0 ]?._id)
      localStorage.setItem('pid', resdata?.data?.ProjectId[ 0 ]?._id)
    }
    catch (err) {
      console.log(err)
    }
  }, []);



   return <React.Fragment>
    <audio  muted id="sound">
   <source src={notification2}  type='audio/aac' />
   Browser not supporting!
   </audio>
   <TasksContext.Provider value={tasksContext}>
   <CountContext.Provider value={countContextt} >
   <updatesContext.Provider value={updatesContextt} >
   <AllprojectsContext.Provider value={ ProjectContext }>
      <IssuesAndExpensesContext.Provider value={IssuesAndExepensesCxt}>
      <SelectedProjectContext.Provider value={ SelectedProjects }>
        <ProjectTaskContext.Provider value={ TaskContext }>
          <div>
            { Authorized ?
              <div style={{display:'flex'}}>
                <Navbar user={ user } />
               <div className='main-page'>
                 <Switch>
                  <Route path='/folderDetails/:id' component={FolderDetails}/>
                             <Route path='/Livesite' component={ CCTV } />
                  <Route path='/editTask/:id' render={ (props) => <CreateTask { ...props } buttonName="Update Task" /> } />
                  <Route path='/tasks/taskDetails' component={Task} />
                  <Route path ='/analysisDetails' render={(props)=><AnalysisDetails {...props}/>} />
                  <Route path='/documents/allFiles' component={AllFiles} />
                  <Route path='/documents/allMedia' component={AllMedia} />
                  <Route path='/overview' render={(props)=> <Overview user={ user } {...props}/>}/>
                  <Route path='/tasks' render={(props)=> <AllTasks {...props}/>} />  
                  <Route path='/ratelist' render={(props)=> <Ratelist {...props}/>} />
                  <Route path='/rfi' render={(props)=> <RFI {...props}/>} /> 
                  <Route path='/rfidetails' render={(props)=> <RFIDetails {...props}/>} /> 
                  <Route path='/reports' render={(props)=> <Reports {...props}/>} /> 
                  <Route path='/modelling' render={(props)=> <Modelling {...props}/>} />  
                  {/* <Route path='/train' render={(props)=> <Trainn {...props}/>} />   */}
                  <Route path='/issuesAndExpenses' render={(props)=><IssueAndExpense {...props}/>} />
                  <Route path='/documents' render={(props)=><Documents {...props}/>} />
                  <Route path='/settings' render={(props)=><Settings {...props}/>}/>
                  <Route path='/module' render={(props)=><Module {...props}/>}/>
                  <Route path='/costSaved' render={(props)=><CostSaved {...props}/>}/>
                  <Route path='/costLeakages' render={(props)=><CostLeakages {...props}/>}/>
                  <Route path='/dpr' render={(props)=><DailyProgressReport {...props}/>} />
                  <Route path ='/analysis' render={(props)=><Analysis {...props}/>} />
                  <Route path='/visualProgress' render={(props)=><VisualProgress {...props} />} />
                  <Route path='/createProject' component={CreateProject}/>
                  <Route path='/createTask' render={(props) => <CreateTask {...props} buttonName="Create Task"/>}/>
                  {/* <Route path='/calendar' component={CustomCalendar} /> */}
                  {/* <Route path ='/taskchart' component={TaskChart} /> */}
                  <Redirect from='/' exact to='/overview' />
                  <Route path='/*' component={NotFound}/>
                  </Switch>
                </div>
              </div>
              : <SignIn/>
            }
          </div>
          
        </ProjectTaskContext.Provider>
      </SelectedProjectContext.Provider>
        </IssuesAndExpensesContext.Provider>
    </AllprojectsContext.Provider>
    </updatesContext.Provider>
    </CountContext.Provider>
    </TasksContext.Provider>
  </React.Fragment>
}

export default App;
