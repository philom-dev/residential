import React, { useContext, useEffect, useState } from 'react';
import AllprojectsContext from '../context/AllprojectsContext';
import SelectedProjectContext from '../context/selectdProjectContext';
import { Link } from 'react-router-dom';
import styles from "./Navbar.module.css";
import dashboardicon from "../resources/icons/home_dark.svg"
import docsicon from "../resources/icons/documents_dark.svg"
import reportsicon from "../resources/icons/reports_dark.svg"
import ratelistlogo from "../resources/icons/ratelist_dark.svg";
import rfilogo from "../resources/icons/rfi_dark.svg";
import tasksicon from "../resources/icons/tasks.svg"
import issuesicon from "../resources/icons/issues_dark.svg";
import logouticon from "../resources/icons/logout_dark.svg";
import analysisicon from "../resources/icons/analysis.svg"
import plusicon from "../resources/icons/add_dark.svg"
import settingsicon from "../resources/icons/settings_dark.svg"
import createicon from "../resources/icons/create.svg"
import temlinlogo from "../resources/icons/temlinlogo.png";
import menuicon from "../resources/icons/menu_dark.svg"
import cctv from "../resources/icons/cctv.png"
import m3mlogo from "../resources/m3mlogo.png"

import Dropdown from "react-dropdown"


function Navbar(props) {

    const [ user, setuser ] = useState({})
    const { AllProjects } = useContext(AllprojectsContext)
    const { SelectedProject, setSelectedProject } = useContext(SelectedProjectContext)
    const [navbar, setNavbar] = useState(false)
    const [projectName, setProjectName] = useState('')

    function _setuser() {
        let res = localStorage.getItem('user')
        res = JSON.parse(res)
        setuser(res)
    }
    function _Logout() {
        localStorage.removeItem('pid')
        localStorage.removeItem('tid')
        localStorage.removeItem('user')
        localStorage.removeItem('userid')
        localStorage.removeItem('token')
        window.location.reload();
        window.location.pathname = '';
    }
    useEffect(() => {
        _setuser()
        setSelectedProject(localStorage.getItem('pid'))
        
    }, [])

    useEffect(()=>{
        AllProjects.length !== 0 && AllProjects.find(i => {
            if(i._id == localStorage.getItem('pid')){
                setProjectName(i.Name)
            }
        })
    })

    let getProjects = () => {
        let arr = []
        AllProjects.length !== 0 && AllProjects.map(i => {
            arr.push({
                value: i._id,
                label: i.Name
            })
        })
        return arr;
    }

    return <React.Fragment>
        <nav className={styles.topnav}>
       <div className={styles.projectName}>
       <div className={styles.box}>
       <Dropdown arrowClassName={styles.dropdownArrow} className={styles.dropdownContainer} menuClassName={styles.dropdownMenu} controlClassName={styles.dropdown} 
       options={getProjects()} onChange={(e)=>{
            setSelectedProject(e.value);
            localStorage.setItem('pid', e.value)
       }} value={getProjects().find(i => i.value == SelectedProject)}/>
        <div className={styles.dropdownIcon}><i className="fas fa-angle-down"></i></div>
       </div>
        </div>

        <div className={styles.user}>
        <div className={styles.userName}>
                    <img style={ { width: '100px', height: '40px', borderRadius: 0 } } src={ m3mlogo } />
            <h3>{user.Name}</h3>
            <div style={{backgroundColor: navigator.onLine == true ? '#2AC371' : '#D53539'}}></div>
        </div>
        <div className={styles.divider}></div>
        <li className={styles.nav__items}>
            <Link to='/settings'><img src={settingsicon}/></Link>
            <Link style={{textDecoration:'underline'}} to='/settings'>Settings</Link>
        </li>
        </div>
        
    </nav>

<nav className={styles.sidebar}>
  <li className={styles.menuIcon}>
    <div><img src={menuicon}/></div>
    </li>
   <ul style={{marginTop:'40px'}}>
   <li className={styles.nav__items}>
    <Link to='/overview'><img src={dashboardicon}/></Link>
    <Link to='/overview' className={styles.navLink}>Home</Link>
    </li>
                <li className={ styles.nav__items }>
                    <Link to='/Livesite'><img src={ cctv } /></Link>
                    <Link to='/Livesite' className={ styles.navLink }>Live site</Link>
                </li>

    <li className={styles.nav__items}>
    <Link to='/documents'><img src={docsicon}/></Link>
    <Link to='/documents' className={styles.navLink}>Documents</Link>
    </li>

    <li className={styles.nav__items}>
    <Link to='/reports'><img src={reportsicon}/></Link>
    <Link to='/reports' className={styles.navLink}>Reports</Link>
    </li>

    <li className={styles.nav__items}>
    <Link to='/issuesAndExpenses'><img src={issuesicon}/></Link>
    <Link to='/issuesAndExpenses' className={styles.navLink}>Issues And Expenses</Link>
    </li>
      
                {/* <li className={styles.nav__items}>
    <Link to='/analysis'><img src={analysisicon}/></Link>
    <Link to='/analysis' className={styles.navLink}>Analysis</Link>
    </li> */}

                {/* <li  className={styles.nav__items}>
     <Link to='/ratelist'><img src={ratelistlogo}/></Link>
     <Link to='/ratelist' className={styles.navLink}>Ratelist</Link>
    </li> */}

    <li  className={styles.nav__items}>
    <Link to='/rfi'><img src={rfilogo}/></Link>
    <Link to='/rfi' className={styles.navLink}>RFI</Link>
    </li>

    <li className={styles.nav__items}>
    <Link to='/createTask'><img src={plusicon}/></Link>
    <Link to='/createTask' className={styles.navLink}>Create Task</Link>
    </li>
  </ul>
  <ul style={{marginTop:'auto'}} >
  <li className={styles.nav__items}>
    <div><img src={logouticon}/></div>
    <div onClickCapture={() => _Logout()} className={styles.navLink}>Logout</div>
    </li>
  </ul>
</nav>
</React.Fragment>
}

export default Navbar;
