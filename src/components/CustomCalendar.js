import React, {useEffect, useState} from "react"
import moment from 'moment';
import { datesGenerator } from 'dates-generator';
import styled from "styled-components";
import styles from "./CustomCalendar.module.css";
import axios from "axios";
import calendaricon from "../resources/icons/calendar.svg"
import {Button} from "./Button.style"
import {Card} from "./Card.style"

function CustomCalander(props){

    const [selectedDate, setSelectedDate] = useState(new Date());
    const [dates, setDates] = useState([]);
    const [list, setList] = useState([])
    const [showCalendar, setShowCalendar] = useState(false)
    const [startDate, setStartDate] = useState('');
    const [isStartDate, setIsStartDate] = useState(false)
    const [endDate, setEndDate] = useState('');
    const [isEndDate, setIsEndDate] = useState(false);
    const [calendar, setCalendar] = useState({
        month: selectedDate.getMonth(),
        year: selectedDate.getFullYear(),
    });
    const [bg, setBg] = useState('rgba(26, 194, 26, 0.295)')
    
    const months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
    const days = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat']

    let _getList = async () => {
      const options = {
        headers: {
            'Content-type': 'application/json',
            "Authorization": 'Bearer ' + localStorage.getItem('token')
        }
    };
      let res = await axios.get(process.env.REACT_APP_DURL + `${props.listName}/` + localStorage.getItem('pid'),options)
      if(res.statusText === 'OK'){
        setList(res.data)
      }
    }

    const onClickNext = () => {
        const body = { month: calendar.nextMonth, year: calendar.nextYear };
        const { dates, nextMonth, nextYear, previousMonth, previousYear } = datesGenerator(body);
    
        setDates([ ...dates ]);
        setCalendar({
          ...calendar,
          month: calendar.nextMonth,
          year: calendar.nextYear,
          nextMonth,
          nextYear,
          previousMonth,
          previousYear
        });
      }
    
      const onClickPrevious = () => {
        const body = { month: calendar.previousMonth, year: calendar.previousYear };
        const { dates, nextMonth, nextYear, previousMonth, previousYear } = datesGenerator(body);
    
        setDates([ ...dates ]);
        setCalendar({
          ...calendar,
          month: calendar.previousMonth,
          year: calendar.previousYear,
          nextMonth,
          nextYear,
          previousMonth,
          previousYear
        });
      }
    
      const onSelectDate = (date) => {
          if(isStartDate === false){
            setStartDate(date);
            setIsStartDate(true)
            setBg('rgba(255, 59, 59, 0.329)');
          } else {
            setEndDate(date)
            setIsEndDate(true)
           }
       } 

      const onSet = () => {
      setBg('rgba(26, 194, 26, 0.295)')
       if(isStartDate && !isEndDate){
         props.onChange(startDate);
        // console.log(startDate)
       } else if(isStartDate && isEndDate){
         props.onChange(startDate, endDate)
        //  console.log(startDate);
        //  console.log(endDate);
       }

        setIsStartDate(false)
        setIsEndDate(false)
        setShowCalendar(false)
        document.body.style.filter = 'none'
      }

      const onCloseHandler = () => {
        setBg('rgba(26, 194, 26, 0.295)')
        setIsStartDate(false)
        setIsEndDate(false)
        setShowCalendar(false)
        document.body.style.filter = 'none'
      }

      useEffect(() => {
        const body = {
          month: calendar.month,
          year: calendar.year
        };
        const { dates, nextMonth, nextYear, previousMonth, previousYear } = datesGenerator(body);
      
        setDates([ ...dates ]);
        setCalendar({
          ...calendar,
          nextMonth,
          nextYear,
          previousMonth,
          previousYear
        });
       ;
      }, [])

      useEffect(()=>{
        _getList();
      },[props.listName])

      return (
        <React.Fragment>
          <div className={styles.calendarIcon} onClick={()=>{
            setShowCalendar(true)
            if(props.blur){
              document.getElementById(props.blur).style.filter = 'blur(3px)'
            }}}><img src={calendaricon}/></div>

        {showCalendar && <Card padding='0' className={styles.calendar}>
        <div className={styles.headerButtons}>
      <Button padding='4px 8px' color="#0A60BD" onClick={onClickPrevious} >
      <i className="fas fa-arrow-left"></i>
      </Button>
      <Button padding='0' color='white' onClick={onCloseHandler} style={{color:'#848EA0',fontSize:'1.5rem'}} className="fas fa-times"></Button>
      <Button padding='4px 8px' color="#0A60BD" onClick={onClickNext}>
      <i className="fas fa-arrow-right"></i>
      </Button>
        </div>
          <div className={styles.monthText}>
              <p>{months[calendar.month]}</p>
              <p>{calendar.year}</p>
          </div>
            
      
                <table className={styles.calendarTable} cellSpacing='0'>
                  <tbody>
                    <tr>
                      {days.map((day) => (
                        <td key={day}>
                          <div className={styles.days}>
                            {day}
                          </div>
                         </td>
                          
                       ))}
                    </tr>
      
                    {dates.length > 0 && dates.map((week) => (
                      <tr>
                        {week.map((day) => (
                          <td style={{padding: '4px'}}>
                            <div key={day.jsDate.substr(0,10)} id='day' className={styles.day} onClick={(e)=>{
                              onSelectDate(new Date(day.year,day.month,day.date))
                              e.target.style.backgroundColor = bg;
                              }} >
                              <p className={`bg ${styles.eachDay} ${day.jsDate.substr(0,10) === moment(new Date()).format('DD/MM/YYYY') && styles.today}`}>
                               {props.showCount && <p className={styles.dayCount} style={{color:`${list.filter((i, index) => moment(i.createdate).format('DD/MM/YYYY') === day.jsDate.substr(0,10)).length === 0 ? 'rgba(91, 187, 91, 0.466)' : '#D53539'}`}}>
                                {list.filter((i, index) => moment(i.createdate).format('DD/MM/YYYY') === day.jsDate.substr(0,10)).length} 
                               </p>}
                                <p style={{fontSize:'1.1rem',fontWeight:'500',zIndex:'-5'}}>{day.date}</p>
                                </p>
                              </div>
                          </td>
                        ))}
                      </tr>
                    ))}
                  </tbody>
                </table>

                <div className={styles.calendarFooter}>
                <div className={styles.refreshButton} onClick={()=>_getList()}><i className="fas fa-redo-alt"></i></div>
                {props.content || <Button color="#0A60BD" onClick={()=>onSet()}>OK</Button>}
                </div>
            </Card>}
      </React.Fragment>
      );
      
}

export default CustomCalander;