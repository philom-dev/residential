import axios from 'axios';
import React, { useRef , useState} from 'react';
import App from '../../App';
import { useHistory } from 'react-router-dom';
import styles from "./Signin.module.css"
import logo from "../../logo/logo.jpg"
import {Card} from "../Card.style"
import {Button} from "../Button.style"

const SignIn = () => {
    let [type, setType] = useState('password')
    const history = useHistory();
    const Number = useRef()
    const password = useRef()

    const _signin = async (e) => {
        e.preventDefault();
        const data = {
            mobileNumber: Number.current,
            password: password.current
        }
        const options = {
            headers: {
                'Content-type': 'application/json',
                "Access-Control-Allow-Origin": "*",
            }
        };
        const res = await axios.post(process.env.REACT_APP_DURL + 'login', data, options)
        localStorage.setItem('token', res.data.token)
        localStorage.setItem('userid', res.data.user._id)
        localStorage.setItem('user', JSON.stringify(res.data.user))
        window.location.reload();
    }

    let showHandler = (e) => {
        e.preventDefault();
        if(type === 'password'){
            setType('text')
        } else {
            setType('password')
        }
    }
  
    return (
        <div className={styles.main}>
            <Card className={styles.login}>
            <form onSubmit={_signin}>
                <img src={logo}/>
                <h2>Welcome back!</h2>
                <h3>Please login to your account.</h3>
                <input onChange={(e)=>Number.current = e.target.value} className={styles.formInput} type='text' placeholder='Mobile Number'/>
                <div className={styles.password}>
                <input onChange={(e)=>password.current = e.target.value} className={styles.formInput} type={type} autoComplete='off' placeholder='Password'/>
                <button type='button' onClick={showHandler}><i className="far fa-eye"></i></button>
                </div>
                {/* <div style={{display:'flex',justifyContent:'space-between',maxWidth: '400px'}}>
                    <div className={styles.checkbox}>
                    <input id='checkbox' type='checkbox'/>
                    <label htmlFor='checkbox'>Remember Me</label>
                    </div>
                    <div>
                        <a href='#'>Forgot Password</a>
                    </div>
                </div> */}
                <Button width='40%' type="submit" color="#0A60BD">Login</Button>
            </form>
            </Card>
        </div>
    )
}

export default SignIn;