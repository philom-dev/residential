import React, { useEffect, useState, useContext } from 'react';
import styles from "./DailyProgressReport.module.css";
import axios from 'axios';
import SelectedProjectContext from '../context/selectdProjectContext';
import AllprojectsContext from '../context/AllprojectsContext';
import moment from 'moment';
import CustomCalander from './CustomCalendar';
import { CustomContainer } from "./CustomContainer.style";
import {Card} from "./Card.style"
import {Button} from "./Button.style"
import pdficon from "../resources/icons/pdficon.svg"
import excellogo from "../resources/icons/excellogo.svg"
import { analytics } from "./Analytics"
import { logEvent } from "@firebase/analytics";


function DailyProgressReport(){
    let [date, setDate] = useState(new Date())
    let [selectedDate, setSelectedDate] = useState(moment(date).format('DD-MM-YYYY'))
    const { SelectedProject, setSelectedProject } = useContext(SelectedProjectContext)
    const { AllProjects } = useContext(AllprojectsContext)
    let [plannedCost, setPlannedCost] = useState();
    let [actualCost, setActualCost] = useState();
    let [projectName, setProjectName] = useState('')
    let [expenses, setExpenses] = useState([])
    let [volume, setVolume] = useState();
    let [surfaceArea, setSurfaceArea] = useState()
    let [manCost, setManCost] = useState();
    let [machineCost, setMachineCost] = useState();
    let [materialCost, setMaterialCost] = useState();
    let [man, setMan] = useState([]);
    let [machine, setMachine] = useState([]);
    let [material, setMatrial] = useState([])
    let [tasks, setTasks] = useState([])
    let [ startDate, setStartDate ] = useState('01-08-2021')
    let [ endDate, setEndDate ] = useState(moment().format('DD-MM-YYYY'))
    let [filter, setFilter] = useState(false)
    let [tutorial, setTutorial] = useState(localStorage.getItem('dprtutorial') ? false : true)


    let manHandler = () => {
        document.querySelector('.manTable').style.display = 'table'
    }

    let machineHandler = () => {
        document.querySelector('.machineTable').style.display ='table'
    }

    let materialHandler = () => {
        document.querySelector('.materialTable').style.display = 'table'
    }

    let changeHandler = (date) => {
        console.log(moment(date).format("DD-MM-YYYY"));
        setSelectedDate(moment(date).format("DD-MM-YYYY"))
    }

    let excelHandler = (startdate, enddate) => {
        if(!startdate || !enddate){
            alert('Choose two dates');
        } else {
          setStartDate(moment(startdate).format('DD-MM-YYYY'))
          setEndDate(moment(enddate).format('DD-MM-YYYY'))
        }
     }

     let calendarHandler = (s,e) => {
        if(s && !e){
            changeHandler(s)
        } else if(s && e) {
            setStartDate(moment(s).format('DD-MM-YYYY'))
            setEndDate(moment(e).format('DD-MM-YYYY'))
        }
    }

    let _getProjectDetails = async () => {
        const options = {
            headers: {
                'Content-type': 'application/json',
                "Authorization": 'Bearer ' + localStorage.getItem('token')
            }
        };
        let res = await axios.get(process.env.REACT_APP_DURL + 'getProject/'+ SelectedProject, options)
        setProjectName(res.data.Name)
    }


    let _getDprDetails = async () => {
        const options = {
            headers: {
                'Content-type': 'application/json',
                "Authorization": 'Bearer ' + localStorage.getItem('token')
            }
        };
        let res = await axios.get(process.env.REACT_APP_DURL + "dpr/" + SelectedProject + "/" + selectedDate, options );
        setPlannedCost(res.data[0].plannedcost)
        setExpenses(res.data[0].Expense)
        setActualCost(res.data[0].actualcost)
        setVolume(res.data[0].Volume);
        setSurfaceArea(res.data[0].surfacearea);
        setManCost(res.data[0].TotalManCost)
        setMachineCost(res.data[0].TotalMachineCost);
        setMaterialCost(res.data[0].TotalMaterialCost)
        setMan(res.data[0].man);
        setMachine(res.data[0].machine)
        setMatrial(res.data[0].material)
        setTasks(res.data[0].Task)
    }

    let excelDownloadHandler = async ()=>{
        const options = {
            headers: {
                'Content-type': 'application/json',
                "Authorization": 'Bearer ' + localStorage.getItem('token')
            }
        };
        if(!startDate || !endDate){
            console.log('Please specify start and end date');
        } else {
           let res = await axios.get(process.env.REACT_APP_DURL+ 'downloadexcel/' + SelectedProject + '/' + startDate + '/' + endDate);
            
            //  for analytics -----
            const params = {
                Project_wise: localStorage.getItem('pid'),
                User_wise: localStorage.getItem('userid'),
                projectName: SelectedProject !== undefined ? AllProjects.find(i => i._id == SelectedProject).Name : '',
                userName: JSON.parse(localStorage.getItem('user')).Name,
                TodaysDate: moment().format('DD-MM-YY hh:mm:ss A'),
                SlectedDate: '',
                taskid: localStorage.getItem('tid') || '',
                taskname: ''
            }
            logEvent(analytics, "download_excel",params)
            // -----------
        }
     }

    let askForUpdates = async () => {
        let res = await axios.post(process.env.REACT_APP_DURL + 'sendPushDpr/' + SelectedProject)
        if(res.status== 200){

            let tasks = res.data
            tasks.map(async (task) => {
                let taskUsers = task.users
                let workUsers = []

                task.workdone.labour.map(l => {
                    workUsers.push(l?.user?._id);
                })

                task.workdone.Machine.map(mac => {
                    workUsers.push(mac?.user?._id);
                })

                task.workdone.Material.map(mat => {
                    workUsers.push(mat?.user?._id);
                })

                workUsers = new Set(workUsers)
                  
                let uselessUsers = [];

                taskUsers.map((k) => {
                    if(!Array.from(workUsers).includes(k)){
                        uselessUsers.push(k);
                    }
                })

                let tokenData = {
                    usersList: uselessUsers
                }
                let tokenArr = []

                let tokenRes = await axios.post(process.env.REACT_APP_DURL + 'getUserToken',tokenData)
                if(tokenRes.status == 200){
                    tokenRes.data.map(t => {
                        tokenArr.push(t.token[0]);
                    })
                }

                // implement api :
                let data = {
                    token: tokenArr,
                    messageTitle: `Work Done`,
                    messageBody: `Work pending in task: ${task.taskname}`,
                    imageUrl: 'http://res.cloudinary.com/dlzo8dt7m/image/upload/v1626373909/IMG-20210715-WA0010_vt6qjx.jpg',
                    key: 'workdone'
                  }
                 let push = await axios.post(process.env.REACT_APP_DURL + "sendPush", data)
                 if(push.status == 200){
                     console.log(push.data.message);
                 }
             })
        }
    }

    useEffect(()=> {
        _getDprDetails();
        _getProjectDetails();
    },[SelectedProject,selectedDate])

    return <div className={styles.dpr}> 

        <div className={styles.heading}>
        <h1 className={styles.pageHeading}>Daily Progress Report</h1>
        <div className={styles.calendar}>
                <CustomCalander onChange={calendarHandler} />
                <h5>{selectedDate}</h5>
        </div>
        </div>

        <div className={styles.cards}>
           <div style={{display:'flex',alignItems:'center'}}>
           <Card className={styles.card}>
               <h2>Actual Cost</h2>
               <h4 className='rupee'>₹{actualCost?.toFixed(3)}</h4>
           </Card>
            <Card color={plannedCost >= actualCost ? '#2AC371' : '#D53539'} className={styles.card}>
               <h2 style={{color:'white'}}>{plannedCost >= actualCost ? 'Profit' : 'Loss'}</h2>
               <h4 className='rupee' style={{color:'white'}}>{isNaN(Math.abs(plannedCost - actualCost)) ? '0' : `₹${Math.abs(plannedCost - actualCost)?.toFixed(3)}`}</h4>
            </Card>
           </div>
           <div className={styles.headerButtons}>
            <Button onClick={askForUpdates} color="#0A60BD">Request Updates</Button>
            <Button color="#22242E" bordered onClick={() => {
                //  for analytics -----
                const params = {
                    Project_wise: localStorage.getItem('pid'),
                    User_wise: localStorage.getItem('userid'),
                    projectName: SelectedProject !== undefined ? AllProjects.find(i => i._id == SelectedProject).Name : '',
                    userName: JSON.parse(localStorage.getItem('user')).Name,
                    TodaysDate: moment().format('DD-MM-YY hh:mm:ss A'),
                    SlectedDate: '',
                    taskid: localStorage.getItem('tid') || '',
                    taskname: ''
                }
                logEvent(analytics, "download_pdf",params)
                // -----------
                 window.print()
                 }}><i class="fas fa-file-pdf"></i> Download Pdf</Button>
            <Button color="#1D6F42"><a style={{color:"white"}} onClick={excelDownloadHandler} download href={`${process.env.REACT_APP_DURL}downloadexcel/${SelectedProject}/${startDate}/${endDate}`}><i class="fas fa-file-excel"></i> Download Excel</a></Button>
          </div>
        </div>

        <div className={styles.table}>
            <div className={styles.tableHeader}>
                <p style={{display:'flex',alignItems:'center'}}><p>Task Name</p>
                <div className={styles.radio}>
                             <div>
                             <input checked={!filter ? true : false} onChange={()=>setFilter(false)} id='updated' name='task' type="radio" />
                             <label htmlFor='updated'>Updated</label>
                             </div>
                             <div>
                             <input checked={filter ? true : false} onChange={()=>setFilter(true)} id='nonupdated' name='task' type="radio" />
                             <label htmlFor='nonupdated'>Non-Updated</label>
                             </div>
                         </div>
                </p>
                <p>Actual Cost</p>
                <p>Profit/Loss</p>
                <p>Work Done</p>
            </div>
            <div className={styles.tableBody}>
            {filter ? tasks.map(task => {
                            if(parseInt(task.volume) == 0 && parseInt(task.surfacearea) == 0){
                                return <div className={styles.tableRow}>
                                <p>{task.taskName}</p>
                                <p>{task.ActualCost?.toFixed(3)}</p>
                                <p style={{color:`${task.PlannedCost >= task.ActualCost ? '#2AC371' : '#D53539'}`}} >{Math.abs(task.PlannedCost - task.ActualCost)?.toFixed(3)}</p>
                                <p>{task.Taskunit === 'cum' ? `${task.volume?.toFixed(3)} ${task.Taskunit}` : task.Taskunit === 'sqm' ? `${task.surfacearea?.toFixed(3)} ${task.Taskunit}` : task.Taskunit === 'km' ? `${task.surfacearea?.toFixed(3)/1000} ${task.Taskunit}` : !task.surfacearea || !task.volume ? "0" : '' }</p>
                            </div>
                            }
                        }) : tasks.map(task => {
                            if(task.volume !== 0  || task.surfacearea !== 0){
                                return <div className={styles.tableRow}>
                                <p>{task.taskName}</p>
                                <p>{task.ActualCost?.toFixed(3)}</p>
                                <p style={{color:`${task.PlannedCost >= task.ActualCost ? '#2AC371' : '#D53539'}`}} >{Math.abs(task.PlannedCost - task.ActualCost)?.toFixed(3)}</p>
                                    <p>{ task.Taskunit == 'cum' ||
                                    task.Taskunit == 'cum' || task.Taskunit == "MT" || task.Taskunit == "Hectare" ||
                                    task.Taskunit == "Kg" || task.Taskunit == "Kiloliter" || task.Taskunit == "Liter" || task.Taskunit == "Meter" == task.Taskunit == "Number"
                                    || task.Taskunit == "RMT" ? `${task.volume?.toFixed(3)} ${task.Taskunit}` :
                                    task.Taskunit === 'sqm' || task.Taskunit == "RMT" || task.Taskunit == "Meter"
                                    || task.Taskunit == "CubicFeet" || task.Taskunit == "Feet"
                                    || task.Taskunit == "SquareFeet" || task.Taskunit == "Tonne" ? `${task.surfacearea?.toFixed(3)} ${task.Taskunit}` : task.Taskunit === 'km' ? `${task.surfacearea?.toFixed(3) / 1000} ${task.Taskunit}` : !task.surfacearea || !task.volume ? "0" : '' }</p>
                              </div>
                            }
                        })}
                    </div>
                 </div>
             </div>

//     return <div className={styles.dpr}>
//         <div className={styles.heading}>
//            <div>
//            <h1>{projectName}</h1>
//             <p>Daily Progress Report</p>
//            </div>
//            <div style={{display:'flex',flexDirection:'row',justifyContent:'space-between',alignItems:'center'}}>
//                 <div style={ {
//                     marginRight: 10, paddingLeft: 10, backgroundColor: '#fff', display: 'flex',
//                     justifyContent: 'center', alignItems: 'center', flexDirection: 'row', borderRadius: 10,
//                 } }>
//                     <p style={ { marginRight: 6, textAlign: 'center' } }>{ selectedDate }</p>
//                     <CustomCalander onChange={ changeHandler } />
//                     <a title="Select Date and download pdf" onClick={ () => window.print() } className={ styles.downloadBtn }><i className="fas fa-download"></i>Download PDF</a>
//                 </div>
//                 <div style={ {
//                     padding: 0, backgroundColor: '#fff', display: 'flex',
//                     justifyContent: 'center', alignItems: 'center', flexDirection: 'row', borderRadius: 10,
//                 } }>
//                     <div style={ { display: 'flex', flexDirection: 'row', borderRadius: 10 } }>
//                         <div style={ { marginLeft: 10, marginRight: 10 } }>
//                             <p>{ `From -${startDate}` }</p>
//                             <p>To  &nbsp;&nbsp;&nbsp;&nbsp; -{ `${endDate}` }</p>
//                         </div>
//                         <CustomCalander onChange={ excelHandler } />
//                     </div>
//                     <a href={ `${process.env.REACT_APP_DURL}downloadexcel/${SelectedProject}/${startDate}/${endDate}` }
//                         title="Select two Date and download Excel"
//                         download onClick={ excelDownloadHandler } className={ styles.downloadBtn }><i className="fas fa-download">
//                         </i>Download Excel</a>
//                 </div>
//                 {/* <div className="tooltip">Hover over me
//                     <span className="tooltiptext">Tooltip text</span>

//                 </div> */}
//            </div>
//         </div>
//         <div className={styles.main}>
//             <div className={styles.sideDetails}>
//                 <Container className={styles.plannedCost}>
//                     <h1 className={styles[`${plannedCost >= actualCost ? 'success' : 'fail'}`]}>₹</h1>
//                     <h2>{actualCost?.toFixed(3)}</h2>
//                     <h4 style={{color:`${plannedCost >= actualCost ? '#5AB044' : 'red'}`}}>
//                         {isNaN(Math.abs(plannedCost - actualCost)) ? '0' : Math.abs(plannedCost - actualCost)?.toFixed(3)}</h4>
//                     <h5>Planned Cost - {plannedCost?.toFixed(3)} </h5>
//                 </Container>
//                 {/* <Container className={styles.workProgress}>
//                     <h1>Work Progress</h1>
//                     <div className={styles.workProgressArea}>
//                         <h2>Area</h2>
//                         <h2>{surfaceArea?.toFixed(3)}</h2>
//                     </div>
//                     <hr/>
//                     <div className={styles.workProgressVolume}>
//                         <h2>Volume</h2>
//                         <h2>{volume}</h2>
//                     </div>
//                 </Container> */}
//                 <Container className={styles.expenses}>
//                     <h1>Expenses</h1>
//                     {expenses.length !== 0 ? expenses.map(expense => {
//                         return <div>
//                         <h3><p>₹ {expense.amount}</p></h3>
//                         <h4 className={styles[expense.status]}></h4>
//                     </div>
//                     }) : <div style={{display:'block',textAlign:'center',color:'darkorange',fontSize:'1.6rem'}}>No expenses!</div>}
//                 </Container>
//             </div>
//             <div className={styles.mainDetails}>
//                 <div style={{display:'flex',alignItems:'center',justifyContent:'space-between'}}>
//                 <div style={{display:'flex',alignItems:'center'}}>
//                 <h1 style={{padding:'0',margin:'0',marginRight:'12px'}}>Task Overview</h1>
//                 <button onClick={()=>setFilter(!filter)} className={styles.filterBtn}>{filter ? "Updated tasks" : 'Non-Updated tasks'}</button>
//                 </div>
                
//                 <button onClick={askForUpdates} className={styles.askForUpdatesBtn}>Ask for Updates</button>
//                 </div>
//                 <table cellSpacing='0' className={styles.taskTable}>
//                     <thead>
//                         <tr>
//                         <th>Task Name</th>
//                         <th>Actual Cost</th>
//                         <th>Profit / Loss</th>
//                         <th>Work Done</th>
//                         </tr>
//                     </thead>
//                     <tbody>
//                      {filter ? tasks.map(task => {
//                             console.log(task);
//                             if(parseInt(task.volume) == 0 && parseInt(task.surfacearea) == 0){
//                                 return <tr>
//                                 <td>{task.taskName}</td>
//                                 <td>{task.ActualCost?.toFixed(3)}</td>
//                                 <td style={{color:`${task.PlannedCost >= task.ActualCost ? 'green' : 'red'}`}} >{Math.abs(task.PlannedCost - task.ActualCost)?.toFixed(3)}</td>
//                                 <td>{task.Taskunit === 'cum' ? `${task.volume?.toFixed(3)} ${task.Taskunit}` : task.Taskunit === 'sqm' ? `${task.surfacearea?.toFixed(3)} ${task.Taskunit}` : task.Taskunit === 'km' ? `${task.surfacearea?.toFixed(3)/1000} ${task.Taskunit}` : !task.surfacearea || !task.volume ? "0" : '' }</td>
//                             </tr>
//                             }
//                         }) : tasks.map(task => {
//                             console.log("qwertyuiopiuytre", task.Taskunit, task.surfacearea, 'wertyuioiuytre');
//                             if(task.volume !== 0  || task.surfacearea !== 0){
//                                 return <tr>
//                                 <td>{task.taskName}</td>
//                                 <td>{task.ActualCost?.toFixed(3)}</td>
//                                 <td style={{color:`${task.PlannedCost >= task.ActualCost ? 'green' : 'red'}`}} >{Math.abs(task.PlannedCost - task.ActualCost)?.toFixed(3)}</td>
//                                     <td>{ task.Taskunit == 'cum' ||
//                                         task.Taskunit == 'cum' || task.Taskunit == "MT" || task.Taskunit == "Hectare" ||
//                                         task.Taskunit == "Kg" || task.Taskunit == "Kiloliter" || task.Taskunit == "Liter" || task.Taskunit == "Meter" == task.Taskunit == "Number"
//                                         || task.Taskunit == "RMT" ? `${task.volume?.toFixed(3)} ${task.Taskunit}` :
//                                         task.Taskunit === 'sqm' || task.Taskunit == "RMT" || task.Taskunit == "Meter"
//                                             || task.Taskunit == "CubicFeet" || task.Taskunit == "Feet"
//                                             || task.Taskunit == "SquareFeet" || task.Taskunit == "Tonne" ? `${task.surfacearea?.toFixed(3)} ${task.Taskunit}` : task.Taskunit === 'km' ? `${task.surfacearea?.toFixed(3) / 1000} ${task.Taskunit}` : !task.surfacearea || !task.volume ? "0" : '' }</td>
//                             </tr>
//                             }
//                         })}
//                       </tbody>
//                 </table>
//                 <div style={{display:'flex',justifyContent:'space-around'}}>
//                         <Container onClick={manHandler} className={styles.man}>
//                         <i className="fas fa-male"></i>
//                         <h1>Man</h1>
//                         <div style={{display:'flex',alignItems:'center',justifyContent:'center',width:'70%'}}><h4>₹ {manCost}</h4>
                        
//                         </div>
//                     </Container>
//                     <Container onClick={machineHandler} className={styles.machine}>
//                         <i className="fas fa-truck"></i>
//                         <h1>Machine</h1>
//                         <div style={{display:'flex',alignItems:'center',justifyContent:'center',width:'70%'}}><h4>₹ {machineCost}</h4>
                  
//                         </div>
//                     </Container>
//                     <Container onClick={materialHandler} className={styles.material}>
//                         <i className="fas fa-wrench"></i>
//                         <h1>Material</h1>
//                         <div style={{display:'flex',alignItems:'center',justifyContent:'center',width:'70%'}}><h4>₹ {materialCost}</h4>
                      
//                         </div>
                        
//                     </Container>
//                 </div>
//                 <table cellSpacing='0' className={`${styles.manTable} manTable`}>
//                     <thead>
//                         <tr>
//                             <th>Man</th>
//                             <th>Quantity</th>
//                             <th>Rate</th>
//                         </tr>
//                     </thead>
//                     <tbody>
//                         {man.map(each => {
//                             return <tr>
//                             <td>{each.name}</td>
//                             <td>{each.Planned}</td>
//                             <td>{each.actual}</td>
//                         </tr>
//                         })}
                        
//                     </tbody>
//                 </table>
//                 <table cellSpacing='0' className={`${styles.machineTable} machineTable`}>
//                     <thead>
//                         <tr>
//                             <th>Machine</th>
//                             <th>Quantity</th>
//                             <th>Rate</th>
//                         </tr>
//                     </thead>
//                     <tbody>
//                         {machine.map(each => {
//                             return  <tr>
//                             <td>{each.name}</td>
//                             <td>{each.Planned}</td>
//                             <td>{each.actual}</td>
//                         </tr>
//                         })}
                       
                       
//                     </tbody>
//                 </table>
//                 <table cellSpacing='0' className={`${styles.materialTable} materialTable`}>
//                     <thead>
//                         <tr>
//                             <th>Material</th>
//                             <th>Quantity</th>
//                             <th>Rate</th>
//                         </tr>
//                     </thead>
//                     <tbody>
//                         {material.map(each => {
//                             return  <tr>
//                             <td>{each.name}</td>
//                             <td>{each.Planned}</td>
//                             <td>{each.actual}</td>
//                         </tr>
//                         })}
//                     </tbody>
//                 </table>
                
//             </div>
            
//         </div>
//     </div>
}

export default DailyProgressReport