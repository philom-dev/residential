import styled from "styled-components";

export let RefreshBtn = styled.button`
font-size: 1.1rem;
display:flex;
justify-content: space-between;
align-items:center;
font-weight: 600;
margin:0;
background: none;
color: white;
background-color: #44444F;
cursor: pointer;
border: 2px solid #44444F; 
height:fit-content;
width:fit-content;
transition: all .3s;
padding:6px 18px;
border-radius: 8px;
&:hover {
color: #44444F;
background-color: white
}
`