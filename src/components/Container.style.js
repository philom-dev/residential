import styled from 'styled-components';

export let Container = styled.div`
padding: 10px;
border-radius: 6px;
background-color: white;
box-shadow: 0px 2px 5px rgba(0,0,0,0.1);
margin: 20px 0;
`

