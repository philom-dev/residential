import axios from "axios";
import { useEffect, useState, useContext} from "react"
import styles from "./Settings.module.css"
import switchstyles from "./ToggleSwitch.module.css";
import loaderlogo from "../logo/loader.gif";
import SelectedProjectContext from "../context/selectdProjectContext";
import AllprojectsContext from '../context/AllprojectsContext';
import moment from "moment";
import { analytics } from "./Analytics"
import { logEvent } from "@firebase/analytics";
import Dropdown from 'react-dropdown';
import { Button } from "./Button.style"

function Settings(){

    const [cost, setCost] = useState('No');
    const [actual, setActual] = useState('No');
    const [planned, setPlanned] = useState('No');
    const [rate, setRate] = useState('No')
    const [projects, setProjects] = useState([])
    const [users, setUsers] = useState([]);
    const [usersList, setUsersList] = useState([])
    const [tasksList, setTasksList] = useState([]);
    const [tasksArr, setTasksArr] = useState([])
    const [id, setId] = useState(localStorage.getItem('pid'))
    const [isChecked, setIsChecked] = useState(false)
    const [check, setCheck] = useState(false)
    const [message, setMessage] = useState(false)
    const [usersMessage, setUsersMessage] = useState(false)
    const [loader, setLoader] = useState(false)
    const [number, setNumber] = useState('')
    const [numbers, setNumbers] = useState([])
    const [viewRole, setViewRole] = useState(true)
    const {SelectedProject, setSelectedProject} = useContext(SelectedProjectContext)
    const { AllProjects } = useContext(AllprojectsContext)

    let _getProjectsList = async () => {
        let userid = localStorage.getItem('userid')
        const options = {
            headers: {
                'Content-type': 'application/json',
                "Authorization": 'Bearer ' + localStorage.getItem('token')
            }
        };
        let res = await axios.get(process.env.REACT_APP_DURL + 'getUserProject/' + userid, options)
        if(res.status == 200){
            setProjects(res?.data.ProjectId);
        }
    }

    let _getProjectTask = async () => {
        const options = {
            headers: {
                'Content-type': 'application/json',
                "Authorization": 'Bearer ' + localStorage.getItem('token')
            }
        };
        let res = await axios.get(process.env.REACT_APP_DURL + 'getprojecttask/' + id ,options)
        if(res.status == 200){
            setTasksArr([...res.data.Delayeddoc,...res.data.INProgressDoc,...res.data.Startdoc,...res.data.Upcomingdoc,...res.data.compeleteddoc,...res.data.currentdoc,...res.data.pausedoc,...res.data.rejecteddoc]);
        }
    }

    let _getUserList = async () => {
        const options = {
            headers: {
                'Content-type': 'application/json',
                "Authorization": 'Bearer ' + localStorage.getItem('token')
            }
        };
        let res = await axios.get(process.env.REACT_APP_DURL + "getProjectuserlist/" + id ,options)
        if(res.status == 200){
            setUsers(res?.data)
        }
    }

    const _getUserRole = async () => {
        
        const options = {
            headers: {
                'Content-type': 'application/json',
                "Authorization": 'Bearer ' + localStorage.getItem('token')
            }
        };
        let res = await axios.get(process.env.REACT_APP_DURL + 'getrole/' + id, options)
        if(res.status == 200){
            res?.data[res.data.length - 1]?.access[0].PlannedPlanned ? setPlanned(res?.data[res.data.length - 1]?.access[0].PlannedPlanned) : setPlanned('No')
            res?.data[res.data.length - 1]?.access[0].ActualPlanned ? setActual(res?.data[res.data.length - 1]?.access[0].ActualPlanned) : setActual('No')
            res?.data[res.data.length - 1]?.access[0].CostDashboard ? setCost(res?.data[res.data.length - 1]?.access[0].CostDashboard) : setCost('No')
            res?.data[res.data.length - 1]?.access[0].Workdonerate ? setRate(res?.data[res.data.length - 1]?.access[0].Workdonerate) : setRate('No')
            res?.data[res.data.length - 1]?.UserId ? setUsersList(res?.data[res.data.length - 1]?.UserId) : setUsersList([])
        }
    }

    let addUsersHandler = async () => {
        setLoader(true)
        console.log(tasksList);
        const data = {
            numbers: numbers,
            tasksList: tasksList
        }
        const options = {
            headers: {
                "Access-Control-Allow-Origin": "*",
                'Content-type': 'application/json',
                "Authorization": 'Bearer ' + localStorage.getItem('token')
            }
        };
        try {

            let res = await axios.put(process.env.REACT_APP_DURL + 'adduserstotasks', data, options)
            if (res.status == 200) {
                // for analytics -----
                const params = {
                    Project_wise: localStorage.getItem('pid'),
                    User_wise: localStorage.getItem('userid'),
                    projectName: SelectedProject !== undefined ? AllProjects.find(i => i._id == SelectedProject).Name : '',
                    userName: JSON.parse(localStorage.getItem('user')).Name,
                    TodaysDate: moment().format('DD-MM-YY hh:mm:ss A'),
                    SlectedDate: '',
                    taskid: localStorage.getItem('tid') || '',
                    taskname: ''
                }
                logEvent(analytics, "add_users_save",params)
                // ---------------
                setLoader(false)
                setUsersMessage(true)
                setNumbers([])
                document.querySelectorAll('.userscheck').forEach(i => {
                    i.checked = false
                })
                setTimeout(() => {
                    setUsersMessage(false)
                }, 5000)
            }
        }
        catch (err) {
            console.log(err)
        }
    }


    let saveHandler = async () => {
        setLoader(true)
        const options = {
            headers: {
                'Content-type': 'application/json',
                "Authorization": 'Bearer ' + localStorage.getItem('token')
            }
        };
        let data = {
            ProjectId: id,
            role: [{
             CostDashboard: cost,
             ActualPlanned: actual,
             PlannedPlanned: planned,
             EditTask: "",
             CreateTask: "",
             CreateProject: "",
             DeleteTask: "",
             UploadMediaToFolder: "",
             CreateFolder: "",
             Workdonerate: rate,
             AddTaskProgress: "",
             ShowTaskProgress: "",
             UpdateMediaInWorkDone: ""
              }],
            userId : usersList
        }
        let res = await axios.post(process.env.REACT_APP_DURL + 'createrole', data, options)
        if(res.status == 200) {
            // for analytics ------
            const params = {
                Project_wise: localStorage.getItem('pid'),
                User_wise: localStorage.getItem('userid'),
                projectName: SelectedProject !== undefined ? AllProjects.find(i => i._id == SelectedProject).Name : '',
                userName: JSON.parse(localStorage.getItem('user')).Name,
                TodaysDate: moment().format('DD-MM-YY hh:mm:ss A'),
                SlectedDate: '',
                taskid: localStorage.getItem('tid') || '',
                taskname: ''
            }
            logEvent(analytics, "set_role_dashboard",params)
            // --------------
            setLoader(false)
            setMessage(true)
            setTimeout(()=>{
                setMessage(false)
            },3000)
        }

    }

    let projectsList = () => {
        let arr = []
        projects.map(i => {
            arr.push({
                label: i.Name,
                value: i._id
            })
        })
        return arr
    }

    useEffect(()=>{
        _getProjectsList()
    },[])

    useEffect(()=>{
        _getUserList()
        _getUserRole()
        _getProjectTask()
    },[id])


    return <div className={styles.settings}>
     <div className={styles.header}>
       <h1 className={styles.pageHeading}>Project Settings</h1>
       <div id='toggle' style={{ backgroundColor: '#fff', borderRadius: '8px',width:'fit-content' }} >
        <button style={{backgroundColor: viewRole ? '#22242E' : '#fff',color: viewRole ? '#fff' : '#22242E'}}
         onClick={ () => setViewRole(true) }>Set Role</button>
         <button style={{backgroundColor: !viewRole ? '#22242E' : '#fff',color: !viewRole ? '#fff' : '#22242E'}}
         onClick={ () => setViewRole(false) }>Add Users</button>
         </div>
       <div className={styles.line}></div>
       </div>

       <div className={styles.selectProject}>
       <div>
       <h2>Project</h2>
        <p>Select the project of which you want the details.</p>
       </div>

       <div style={{minWidth:'300px'}}>
       <Dropdown arrowClassName={styles.dropdownArrow} className={styles.dropdownContainer} menuClassName={styles.dropdownMenu} controlClassName={styles.dropdown}
        onChange={(e)=>{setId(e.value)}} options={projectsList()} value={projectsList().find(i => {
         if(i.value == id){
            return i.label }})}/>
       </div>
      </div>
       <div className={styles.line}></div>
       
       {viewRole && <div>
       <div className={styles.roleOptions}>
           <div>
               <h2>Role Options</h2>
               <p>Toggle options to show/hide</p>
           </div>
           <div className={styles.roles}>
               
        <div className={switchstyles.container}>
      <div className={switchstyles.labelText}>Cost Dashboard</div>
      <div className={switchstyles.toggleSwitch}>
        <input checked={cost === 'No' ? false : true} onChange={()=>{
                       if(cost === 'No'){
                           setCost('Yes')
                       } else {
                           setCost('No')
                       }
                   }} type="checkbox" className={switchstyles.checkbox} 
               name='Cost Dashboard' id='Cost Dashboard' />
        <label className={switchstyles.label} htmlFor='Cost Dashboard'>
          <span className={switchstyles.inner} />
          <span className={switchstyles.switch} />
        </label>
      </div>
        </div>

        <div className={switchstyles.container}>
      <div className={switchstyles.labelText}>Actual Cost</div>
      <div className={switchstyles.toggleSwitch}>
        <input checked={actual === 'No' ? false : true} onChange={()=>{
                       if(actual === 'No'){
                           setActual('Yes')
                       } else {
                           setActual('No')
                       }
                   }} type="checkbox" className={switchstyles.checkbox} 
               name='Actual Cost' id='Actual Cost' />
        <label className={switchstyles.label} htmlFor='Actual Cost'>
          <span className={switchstyles.inner} />
          <span className={switchstyles.switch} />
        </label>
      </div>
        </div>

        <div className={switchstyles.container}>
      <div className={switchstyles.labelText}>Planned Cost</div>
      <div className={switchstyles.toggleSwitch}>
        <input checked={planned === 'No' ? false : true} onChange={()=>{
                       if(planned === 'No'){
                           setPlanned('Yes')
                       } else {
                           setPlanned('No')
                       }
                   }} type="checkbox" className={switchstyles.checkbox} 
               name='Planned Cost' id='Planned Cost' />
        <label className={switchstyles.label} htmlFor='Planned Cost'>
          <span className={switchstyles.inner} />
          <span className={switchstyles.switch} />
        </label>
      </div>
        </div>


        <div className={switchstyles.container}>
      <div className={switchstyles.labelText}>Rate</div>
      <div className={switchstyles.toggleSwitch}>
        <input checked={rate === 'No' ? false : true} onChange={()=>{
                       if(rate === 'No'){
                           setRate('Yes')
                       } else {
                           setRate('No')
                       }
                   }} type="checkbox" className={switchstyles.checkbox} 
               name='Rate' id='Rate' />
        <label className={switchstyles.label} htmlFor='Rate'>
          <span className={switchstyles.inner} />
          <span className={switchstyles.switch} />
        </label>
      </div>
        </div>


           </div>
        </div>
        <div className={styles.line}></div>
            <div className={styles.users}>
               <div>
               <h2>Users</h2>
                <p>Choose multiple users to apply above settings</p>
               </div>
               <div>
                   <table cellSpacing='0' className={styles.table}>
                       <thead>
                           <tr>
                           <th>
                            <div style={{display:'flex',alignItems:'center'}}>
                                   <input onChange={(e)=>{
                                       if(e.target.checked == true){
                                            let list = users.map(i => i._id)
                                            setUsersList(list)
                                            document.querySelectorAll('#checkbox').forEach(i => {
                                                i.checked = true
                                            })
                                       } else if(e.target.checked == false){
                                           setUsersList([])
                                           document.querySelectorAll('#checkbox').forEach(i => {
                                            i.checked = false
                                        })
                                       }
                                   }} type="checkbox"/> <p>Full Name</p>
                                   </div>
                               </th>
                               <th>Mobile Number</th>
                           </tr>
                       </thead>
                       <tbody>
                          
                        {users.map((user,i) => {
                            return <tr key={i}>
                            <td>
                                <div style={{display:'flex',alignItems:'center'}}>
                                <input checked={usersList.includes(user._id) ? true : false} id='checkbox' onChange={(e)=>{
                                    if(e.target.checked == true){
                                        if(!usersList.includes(user._id))
                                        setUsersList((n)=>[...n,user._id])
                                      
                                    } else {
                                        setUsersList(usersList.filter(i => i !== user._id))
                                    }
                                }} type="checkbox"/> <p>{user.Name ? user.Name : 'No name specified'}</p>
                                </div>
                            </td>
                            <td>{user.mobileNumber}</td>
                        </tr>
                        })}
                       </tbody>
                   </table>
               </div>
               
           </div>
           <Button color="#0A60BD" margin="10px 0" onClick={()=>saveHandler()}>Save Changes</Button>
           {loader ? <img className={styles.loader} src={loaderlogo}/> : message ? <div className={styles.successMessage}>Updated Successfully!</div> : ''}
           
       </div>}
       
        {!viewRole && <div className={styles.addUsers}>
            <h2>Add Users to Tasks</h2>
            <div style={{display:'flex',justifyContent:'space-between',alignContent:'flex-start'}}>
            <div>
            {tasksArr.length !== 0 && <div style={{display:'flex',alignItems:'center',marginBottom:'20px'}}>
            <input onChange={(e)=>{
                if(e.target.checked == true){
                    let list = tasksArr.map(i => i._id)
                    setTasksList(list)
                       e.target.parentElement.parentElement.querySelectorAll('.userscheck').forEach(i => {
                           i.checked = true
                       });
                 } else {
                    setTasksList([])
                    e.target.parentElement.parentElement.querySelectorAll('.userscheck').forEach(i => {
                        i.checked = false
                    });
                }
            }} id='checkbox' type="checkbox"/><p>Select All</p>
            </div>}
            {tasksArr.length !== 0 ? tasksArr.map(task => {
             return  <div key={task._id} style={{display:'flex',alignItems:'center',marginBottom:'16px'}}>
             <input className='userscheck' id='checkbox' onChange={(e)=>{
                if(e.target.checked == true){
                    if(!tasksList.includes(task._id))
                    setTasksList((n)=>[...n,task._id])
                 } else {
                    setTasksList(tasksList.filter(i => i !== task._id))
                }
            }} type="checkbox"/> <p>{task.name}</p>
            </div> 
          } ) : <h3 style={{fontSize:'1.4rem',color:'#848EA0',fontWeight:'500'}}>No Tasks in this Project</h3>}
            </div>
            
            <form onSubmit = {(e)=>{
                e.preventDefault();
                if(number.length !== 10){
                    console.log('enter valid number');
                } else {
                    setNumbers([...numbers,number])
                    setNumber('')
                }
                // for analytics -----
                const params = {
                    Project_wise: localStorage.getItem('pid'),
                    User_wise: localStorage.getItem('userid'),
                    projectName: SelectedProject !== undefined ? AllProjects.find(i => i._id == SelectedProject).Name : '',
                    userName: JSON.parse(localStorage.getItem('user')).Name,
                    TodaysDate: moment().format('DD-MM-YY hh:mm:ss A'),
                    SlectedDate: '',
                    taskid: localStorage.getItem('tid') || '',
                    taskname: ''
                }
                logEvent(analytics, "add_users_add",params)
                // -----------
                }}>
                <input value={number} onChange={(e)=>setNumber(e.target.value)} maxLength='10' type='tel'/>
                <Button color="#0A60BD" type="submit">Add</Button>


                <div>
                    {numbers.map((number,i) => {
                        return <p key={i}>{number}</p>
                    })}
                </div>
            </form>

            </div>
            <Button margin="10px 0" onClick={()=>addUsersHandler()} color="#0A60BD">Save Changes</Button>
            {loader ? <img className={styles.loader} src={loaderlogo}/> : usersMessage ? <div className={styles.successMessage}>Users Added Successfully!</div> : ''}
          </div>}
          <div className={styles.line}></div>
    </div>
}

export default Settings