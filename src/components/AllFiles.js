import styles from "./AllFiles.module.css"
import React, {useEffect, useState, useContext} from 'react';
import SelectedProjectContext from "../context/selectdProjectContext";
import axios from "axios";
import pdficon from "../resources/icons/pdficon.svg"
import excelicon from "../resources/icons/excelicon.svg"
import wordicon from "../resources/icons/wordicon.png"
import moment from "moment"
import CustomCalander from "./CustomCalendar";


function AllFiles(){

    const [mediaArr, setMediaArr] = useState([])
    const { SelectedProject, setSelectedProject } = useContext(SelectedProjectContext)
    const [filterId, setFilterId] = useState('')
    const [selectedDate, setSelectedDate] = useState('')
    const [tasksArr, setTasksArr] = useState([])

    const _getAllMedia = async () => {
        const options = { 
            headers: {
                'Content-type': 'application/json',
                "Authorization": 'Bearer ' + localStorage.getItem('token')
            }
        };
        let res = await axios.get(process.env.REACT_APP_DURL + 'allmedia/' + SelectedProject,options)
        console.log(res.data[0].task);
        let arr = []
        res.data[0].task.map(i => {
            i.Media.map(j => {
                if(i => i.mediaType == "xlsx" || i.mediaType == 'pdf' || i.mediaType == 'docx'){
                    arr.push({
                        MediaSize: j.MediaSize,
                        MediaUrl: j.MediaUrl,
                        UploadBy: j.UploadBy,
                        createdate: j.createdate,
                        mediaID: j.mediaID,
                        mediaType: j.mediaType,
                        taskID: i.taskID
                    })
                }
             })
        })
        setTasksArr(res.data[0].task)
        setMediaArr(arr.sort(function(a,b){
            return new Date(moment(b.createdate,"DD-MM-YYYY").format('YYYY,M,DD')) - new Date(moment(a.createdate,"DD-MM-YYYY").format('YYYY,M,DD'))
            }))
       
    }

    let changeHandler = (start) => {
        setSelectedDate(moment(start).format('DD-MM-YYYY'));
   }
    
    useEffect(() => {
        _getAllMedia()
    },[SelectedProject])


    return <React.Fragment>
     <div className={styles.main}>
        <div className={styles.filter}>
            <p style={{backgroundColor: filterId == '' && '#0A60BD', color: filterId == '' && '#fff'}} onClick={()=>setFilterId('')}>All Tasks</p>
            {tasksArr.map(task => {
                if(task.Media.filter(i => i.mediaType == "xlsx" || i.mediaType == 'pdf' || i.mediaType == 'docx').length !== 0){
                return <p style={{backgroundColor: filterId == task.taskID && '#0A60BD', color: filterId == task.taskID && '#fff'}} onClick={()=>{
                    setSelectedDate('')
                    setFilterId(task.taskID)
                }}>{task.name}</p>
                }
            })}
        </div>
        <div style={{marginLeft:'30%'}}>
        <div className={styles.calendar}>
         <CustomCalander onChange={changeHandler}/>
           <h5>{selectedDate ? selectedDate : 'Select Date'}</h5>
         </div>
        <div className={styles.docs}>
            {mediaArr.map(j => {
                if(filterId == '' && selectedDate == ''){
                if(j.mediaType === 'xlsx' || j.mediaType == 'pdf' || j.mediaType == 'docx') return <div className={styles.doc}>
                <a href={j.MediaUrl} download>
                <img src={j.mediaType == 'xlsx' ? excelicon : j.mediaType == 'pdf' ? pdficon : j.mediaType == 'docx' ? wordicon : ''}/>
                 
                 </a>
                 <p>{j.MediaName} {j.createdate} by {j.UploadBy}</p>
                </div> 
                } else if(j.taskID == filterId || j.createdate == selectedDate){
                if(j.mediaType === 'xlsx' || j.mediaType == 'pdf' || j.mediaType == 'docx') return <div className={styles.doc}>
                <a href={j.MediaUrl} download>
                <img src={j.mediaType == 'xlsx' ? excelicon : j.mediaType == 'pdf' ? pdficon : j.mediaType == 'docx' ? wordicon : ''}/>
                 <p>{j.MediaName} {j.createdate} by {j.UploadBy}</p>
                 </a>
                </div>
                } 
            })}
            </div>
        </div>
    </div>
   </React.Fragment>
}

export default AllFiles;