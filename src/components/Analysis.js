import styles from "./Analysis.module.css"
import axios from "axios";
import { useEffect,useContext, useState } from "react";
import roadImage from "../resources/road.png"
import { Link } from "react-router-dom";
import SelectedProjectContext from "../context/selectdProjectContext";
import { Steps, Hints } from 'intro.js-react';
import { analytics } from "./Analytics"
import { logEvent } from "@firebase/analytics";

function Analysis() {
    
    let steps = [
        {
            element: '#chainageFilter',
            intro: '<h1>Select a chainage!</h1><p>Enter a range to check the tasks which are done in that chainage.</p>',
            tooltipClass: 'tooltip'
        },
        {
            element: '#chainageItem',
            intro: '<p>See the tasks which are completed in the selected range. Click here to see further details.</p>',
            tooltipClass: 'tooltip'
        }
    ]

    let renderSteps = () => {
        return <Steps
      showProgress={true}
      enabled={true}
      steps={steps}
      initialStep={0}
      onComplete={()=>console.log("completed")}
      onExit={()=>{
          localStorage.setItem('tutorial3','finished')
        //   window.location.href = 'analysis'
      }}
      options={{
        skipLabel:'Skip',
        showProgress: true,
        showButtons:true,
        showStepNumbers: true,
        disableInteraction: false,
        exitOnOverlayClick:false,
        hideNext: true
      }}
      />
    }
    

    const { SelectedProject, setSelectedProject } = useContext(SelectedProjectContext)
    const [workDoneList, setWorkDoneList] = useState([])
    const [range1, setRange1] = useState('')
    const [range2, setRange2] = useState('')
    // const [min, setMin] = useState('');
    // const [max, setMax] = useState('')
    const [numbers, setNumbers] = useState([]);
    
    // let [ranges, setRanges] = useState([])
    const [tutorial, setTutorial] = useState(localStorage.getItem('tutorial3') ? false : true)

    let [ranges, setRanges] = useState(tutorial ? [{from: 0, to:100}] : [{from: 0, to:100},{from: 100, to:200},{from: 200, to:300},{from: 300, to:400},{from: 400, to:500},{from: 500, to:600},{from: 600, to:700},{from: 700, to:800},{from: 800, to:900},{from: 900, to:1000}])

    let _getAllTasksWorkDone = async () => {
        const options = {
            headers: {
                'Content-type': 'application/json',
                "Authorization": 'Bearer ' + localStorage.getItem('token')
            }
        };
        let res = await axios.get(process.env.REACT_APP_DURL + 'getAllTasksWorkDone/'+ localStorage.getItem('pid'), options)
        if(res.status == 200){
            setWorkDoneList(res.data)
        }
    }

    let _searchHandler = async () => {
        if(range1 == '' || range2 == '') return
        const options = {
            headers: {
                'Content-type': 'application/json',
                "Authorization": 'Bearer ' + localStorage.getItem('token')
            }
        };
        let data = {
            range1: +range1,
            range2: +range2
        }
        let res = await axios.post(process.env.REACT_APP_DURL + 'getRangeWorkDone/'+ SelectedProject,data,options)
        if(res.status == 200){
            setWorkDoneList(res.data.filter(i => i.taskProgress.length !== 0));
          }
    }

let tasks = []
ranges.map(i => {
    workDoneList.map(j=> {
        j.taskProgress.map((k,index) => {
          if((k.From >= i.from && k.To <= i.to) || (k.From <= i.from && k.To >= i.from) || (k.From<=i.to && k.To >= i.to)){
               tasks.push({task: j.name,range: i,actualRange: {
                   from: k.From,
                   to: k.To
               }});
           }
        })
    })
})


  


    const getIntervals = (interval, num) => {
        const size = Math.floor(interval / num);
        const res = [];
        for (let i = 0; i <= interval;
        i += size) {
           const a = i == 0 ? i : i += 1;
           const b = i + size > interval ? interval : i + size;
           if (a < interval){
              res.push({from:a,to: b});
           };
        };
        // setRanges(res)
        return res
     };

   useEffect(()=>{
     _getAllTasksWorkDone()
   },[SelectedProject])

   useEffect(()=>{
     let arr = []
     workDoneList.map(i => {
         i.taskProgress.map(k => {
             numbers.push(+k.From);
             numbers.push(+k.To)
         })
         setNumbers(arr)
     })
     
     let min = Math.min.apply(Math, numbers);
     let max = Math.max.apply(Math, numbers);
     getIntervals(max, 10);
    
   },[])


    return <div className={styles.analysis}>
        {tutorial && renderSteps()}
        <div style={{width:'100%'}}>
            <img style={{height:'100%',width:'100%',opacity:'1'}} src={roadImage} />
        </div>
       
        <div>
            <div id='chainageFilter' className={styles.chainagefilter}>
                <p>Select Chainage</p>
                <input value={tutorial && 0} onInput={(e)=>setRange1(e.target.value)} placeholder='From' type='number' />
                <i className="fas fa-arrows-alt-h"></i>
                <input value={tutorial && 100} onInput={(e)=>setRange2(e.target.value)} placeholder='To' type='number' />
                <button onClick={_searchHandler} ><i style={{fontSize:'1rem'}} className="fas fa-search"></i> View</button>
            </div>

          <div className={styles.chainage}>

          {ranges.map((i,ind) => {
              if(tutorial) return <Link to={{pathname: "/analysisDetails", state: {from:0,to:100,nextstep: true}}} className={styles.chainageItem}>
              <div className={styles.progressBar}>
             <div className={styles.a}>
             <i className="fas fa-map-marker-alt"></i>
             </div>
             <div className={styles.b}></div>
              </div>
          <div id='chainageItem' className={styles.details}>
          <h1>0 - 100m</h1>
         <div className={styles.chainageDetails}>
         <p><i className="fas fa-circle"></i> Task Name <span style={{color: 'rgb(133, 133, 133)',fontStyle:'italic',marginLeft:'6px',fontWeight:'500',fontSize:'80%'}}>0 - 100m</span></p>
          </div>
        </div>
          </Link>

                return <Link id='chainageItem' to={{pathname: "/analysisDetails", state: i}} key={ind} className={styles.chainageItem}>
                    <div className={styles.progressBar}>
                   <div className={styles.a}>
                   <i className="fas fa-map-marker-alt"></i>
                   </div>
                   <div className={styles.b}></div>
                    </div>
                <div className={styles.details}>
                <h1>{i.from} - {i.to}m</h1>
               <div className={styles.chainageDetails}>
               {tasks.map((j,index) => {
                        if(j.range.from == i.from && j.range.to == i.to){
                        return <p key={index}><i className="fas fa-circle"></i> {j.task} <span style={{color: 'rgb(133, 133, 133)',fontStyle:'italic',marginLeft:'6px',fontWeight:'500',fontSize:'80%'}}>{j.actualRange.from} - {j.actualRange.to}m</span></p>
                        }
                    })}
                </div>
              </div>
                </Link>
            })}

           {/* {workDoneList.map((i,index) => {
                return <div className={styles.chainageItem}>
               <div className={styles.progressBar}>
                   <div className={styles.a}>
                   <i className="fas fa-map-marker-alt"></i>
                   </div>
                   <div className={styles.b}></div>
               </div>
              <Link to='/analysisDetails'>
              <div className={styles.details}>
              <h1>{i.From} - {i.To}</h1>
               <p>End Date: DD/MM/YYYY</p>
               <div className={styles.chainageDetails}>
                  <p><i className="fas fa-circle"></i> Evacuation</p>
                  <p><i className="fas fa-circle"></i> Road Marking</p>
                  <p><i className="fas fa-circle"></i> Evacuation</p>
                  <p><i className="fas fa-circle"></i> DBM</p>
                  <p><i className="fas fa-circle"></i> Evacuation</p>
                  <p><i className="fas fa-circle"></i> Tack Coat</p>
               </div>
               <h2 className={styles.profit}>Cost</h2>
              </div></Link>
           </div>
            })} */}

          </div>
        </div>
     </div>
}

export default Analysis;