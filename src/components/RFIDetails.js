import styles from "./RFIDetails.module.css"

function RFIDetails(){
    return <div className={styles.details}>
        <div className={styles.container}>

            <div className={styles.containerHeader}>
                <div className={styles.calendar}><input type='date'/></div>
                <div className={styles.search}>
                <div style={{position:'relative'}}>
               <input placeholder='Search' type='text'/>
               <i style={{color:'grey'}} className="fas fa-search"></i>
               </div>
                </div>
            </div>

            <table cellPadding='0' cellSpacing='0' className={styles.table}>
                <thead>
                    <tr>
                        <th>S.No</th>
                        <th>Activity</th>
                        <th>Side</th>
                        <th>Chainage RFI</th>
                        <th>Length RFI</th>
                        <th>Chainage DPR</th>
                        <th>Length DPR</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>1</td>
                        <td>activity name</td>
                        <td>chainage side</td>
                        <td>73612873</td>
                        <td>278367812</td>
                        <td>2173612783</td>
                        <td>1231232</td>
                        <td>Approved</td>
                     </tr>
                </tbody>
            </table>
            <button>Download Excel</button>
        </div>
    </div>
}

export default RFIDetails