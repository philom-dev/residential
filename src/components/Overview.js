import "./Overview.css";
import React, { useContext, useEffect, useLayoutEffect, useState } from 'react';
import moment from "moment";
import styles from "./Overview.module.css"
import axios from "axios";
import ProjectTaskContext from "../context/ProjectTaskContext";
import SelectedProjectContext from "../context/selectdProjectContext";
import AllprojectsContext from '../context/AllprojectsContext';
import TasksContext from '../context/TasksContext';
import { Link } from "react-router-dom";
import IssuesAndExpensesContext from "../context/IssuesAndExpensesContext";
import changeNumberFormat from "./CostFormat";
import {CostFormatCommas} from "./CostFormatCommas"
import dprlogo from "../resources/icons/dprlogo.svg"
import { parse } from "dotenv";
import updatesContext from '../context/UpdatesContext';
import 'intro.js/introjs.css';
import introJs from "intro.js"
import { Steps, Hints } from 'intro.js-react';
import CountContext from "../context/CountContext";
import { analytics } from "./Analytics"
import { logEvent } from "@firebase/analytics";
import { Card } from "./Card.style";
import {
    CircularProgressbar,
    CircularProgressbarWithChildren,
    buildStyles
  } from "react-circular-progressbar";
import "react-circular-progressbar/dist/styles.css";
import plannedicon from "../resources/icons/plannedcost.svg"
import actualicon from "../resources/icons/actualcost.svg"
import { Button } from "./Button.style";
import costleakageicon from "../resources/icons/costleakageicon.png"
import security from "../resources/icons/security.png"
import commenticon from "../resources/icons/commenticon.png"
import downloadicon from "../resources/icons/downloadicon.svg"


const COLORS = [ '#57cc99', '#38a3a5', '#c70000', '#22577a' ];

function Overview(props) {

    const {ProjectTasks, setProjectTasks} = useContext(ProjectTaskContext)
    const {SelectedProject, setSelectedProject} = useContext(SelectedProjectContext)
    const { AllProjects } = useContext(AllprojectsContext)
    const {isIssue, setIssue} = useContext(IssuesAndExpensesContext)
    const [ProjectCost, setProjectCost] = useState()
    const [issueList, setissueList] = useState([])
    const [expenseList, setExpenseList] = useState([])
    const [AllMedia, setAllMedia] = useState([])
    const [plannedCost, setPlannedCost] = useState('');
    const [actualCost, setActualCost] = useState('')
    const [allTasks, setAllTasks] = useState([])
    const [ totalFinishedtask, settotalFinishedtask ] = useState(0)
    const [ totaltask, settotaltask ] = useState(0)
    const [loading, isLoading] = useState(false)
    const {updates, setUpdates} = useContext(updatesContext)
    const [tutorial0, setTutorial0] = useState(localStorage.getItem('tutorial0') ? false : true)
    const [tutorial, setTutorial] = useState(localStorage.getItem('tutorial') ? false : true)
    const {count, setCount} = useContext(CountContext)
    const [costSaved, setCostSaved] = useState('')
    const [ costLeakages, setCostLeakages ] = useState('34594380')
    const [totalLeakages, setTotalLeakages] = useState([])
    const [ progress, setProgress ] = useState(0)
    const [commentModal, setCommentModal] = useState(false)
    const [notificationFilter, setNotificationFilter] = useState('')
    const [showIssue, setShowIssue] = useState(false)
    const [showExpense, setShowExpense] = useState(false)
    const [showWork, setShowWork] = useState(false)
    const [showAll, setShowAll] = useState(true)
    const [selectedIssue, setSelectedIssue] = useState('')
    const [commentText, setCommentText] = useState('')


    let steps1 = [
        {
            intro: `<h1>Welcome to Temlin!</h1><p>Please follow through the steps to get an overview of this Dashboard.</p>
            `,
            tooltipClass: 'welcomemsg'
        },
        {
            intro: "<h1>Creating a new Task!</h1><p>Click on done to see how to create a task.</p>",
            tooltipClass: 'tooltip'
        }
    ]

    let steps2 = [
        {
            element: '#liveupdates',
            intro: "<h1>Live Updates</h1><p>Get live updates right on your dashboard!</p>",
            tooltipClass:'tooltip',
            position:'left'
         },
        {
            element: '#chart',
            intro: "<h1>Progress</h1><p>Monitor your overall project progress directly from the dashboard!</p>",
            tooltipClass: 'tooltip',
            position: 'right'
        },
        {
            element: '#cost',
            intro: "<h1>Project Cost</h1><p>Monitor your overall project cost progress here!</p>",
            tooltipClass: 'tooltip'
        },
        {
            element: '#pendingissues',
            intro: "<h1>Pending Isuues</h1><p>See your pending issues right through here!</p>",
            tooltipClass:'tooltip',
            position:'right'
         },
         
         {
            element: '#lossmakingtasks',
            intro: "<h1>Loss Making Tasks</h1><p>Have a look at the tasks which are making you poor!</p>",
            tooltipClass:'tooltip'
         }
    ]

    let steps3 = [
        {
            element: '#profitreport',
            intro: "<h1>Profitability Report</h1><p>Click here to download profitablity report of all your tasks in excel format.</p>",
            tooltipClass:'tooltip',
            position:'left'
         }
    ]
    
    let renderSteps1 = () => {
      return <Steps
      showProgress={true}
      enabled={true}
      steps={steps1}
      initialStep={0}
      onComplete={()=>window.location.href = 'createTask'}
      onExit={()=>localStorage.setItem('tutorial0','finished')}
      options={{
        skipLabel:'Skip',
        showProgress: true,
        showButtons:true,
        showStepNumbers: true,
        disableInteraction:false,
        exitOnOverlayClick:false
      }}
      />
    }
    let renderSteps2 = () => {
        return <Steps
        showProgress={true}
        enabled={true}
        steps={steps2}
        initialStep={0}
        onComplete={()=>{
            localStorage.setItem('tutorialdate',new Date())
            localStorage.setItem('tutorial','finished')
        }}
        onExit={()=>{console.log("exit tutorial")}}
        options={{
            skipLabel:'Skip',
            showProgress: true,
            showButtons:true,
            showStepNumbers: true,
            disableInteraction:false,
            exitOnOverlayClick:false
        }}
        />
      }

      let renderSteps3 = () => {
        return <Steps
        showProgress={true}
        enabled={true}
        steps={steps3}
        initialStep={0}
        onComplete={()=>{
            localStorage.setItem("secondvisit","finished")
            
        }}
        onExit={()=>{console.log("exit tutorial")}}
        options={{
            skipLabel:'Skip',
            showProgress: true,
            showButtons:true,
            showStepNumbers: true,
            disableInteraction:false,
            exitOnOverlayClick:false
        }}
        />
      }

   let _getCostDetails = async () => {
        const options = {
           headers: {
               'Content-type': 'application/json',
               "Authorization": 'Bearer ' + localStorage.getItem('token')
           }
        };
        let res = await axios.get(process.env.REACT_APP_DURL + "getLeakage/" + SelectedProject,options)
        if(res.status == 200){
            setTotalLeakages([...res.data.Macdata,...res.data.Matdata])
            setCostSaved(parseInt(res.data.MachineSavedCost || 0 + res.data.MaterialSavedCost || 0))
            setCostLeakages(parseInt(res.data.MatCost || 0 + res.data.machineCost || 0))
        }
   } 

   async function _getProjectTasks() {
        const options = {
            headers: {
                'Content-type': 'application/json',
                "Authorization": 'Bearer ' + localStorage.getItem('token')
            }
        };
        const res = await axios.get(process.env.REACT_APP_DURL + 'getprojecttask/' + SelectedProject,options)
        localStorage.setItem('pid', SelectedProject)
        setProjectTasks(res.data)
    }

    async function _getProjectCost() {
        let pid = localStorage.getItem('pid')
        const options = {
            headers: {
                'Content-type': 'application/json',
                "Authorization": 'Bearer ' + localStorage.getItem('token')
            }
        };
        const res = await axios.get(process.env.REACT_APP_DURL + 'getprojectCost/' + SelectedProject,options)
        if(res.status == 200){
            setProjectCost(res?.data)
            setPlannedCost(parseInt(res.data[0].PlannedCost));
            setActualCost(parseInt(res.data[0].ActualCost))
        }
    }

    async function _getIssuelist() {
        try {
            let pid = localStorage.getItem('pid')
            const options = {
                headers: {
                    'Content-type': 'application/json',
                    "Authorization": 'Bearer ' + localStorage.getItem('token')
                }
            };
            isLoading(true)
            const res = await axios.get(process.env.REACT_APP_DURL + 'issuelist/' + SelectedProject,options)
            setissueList(res?.data)
            isLoading(false)
        }
        catch (err) {
            console.log(err)
        }
    }

    async function _getExpenselist() {
        try {
            let pid = localStorage.getItem('pid')
            const options = {
                headers: {
                    'Content-type': 'application/json',
                    "Authorization": 'Bearer ' + localStorage.getItem('token')
                }
            };
            isLoading(true)
            const res = await axios.get(process.env.REACT_APP_DURL + 'expenselist/' + SelectedProject,options)
            setExpenseList(res?.data)
            isLoading(false)
        }
        catch (err) {
            console.log(err)
        }
    }

    async function _getallMedia() {
        try {
            let pid = localStorage.getItem('pid')
            const options = {
                headers: {
                    'Content-type': 'application/json',
                    "Authorization": 'Bearer ' + localStorage.getItem('token')
                }
            };
            const res = await axios.get(process.env.REACT_APP_DURL + 'allmedia/' + SelectedProject,options)
            setAllMedia(res?.data)
        }
        catch (err) {
            console.error(err)
        }
    }

    let _getAllTasks = async () => {
        console.log("🚀 ~ file: Overview.js ~ line 288 ~ let_getAllTasks= ~ _getAllTasks")
        const options = {
            headers: {
                'Content-type': 'application/json',
                "Authorization": 'Bearer ' + localStorage.getItem('token')
            }
        };
        let res = await axios.get(process.env.REACT_APP_DURL + 'getAllTasks/' + SelectedProject,options)
        // setAllTasks(res.data);
        if (res.data[ 0 ] && ProjectTasks.length < res.data.length) {
            setProjectTasks(res.data)
            // calcProgress(res.data)
        }
    }
    let Projectprogress = async () => {
        console.log("🚀 ~ file: Overview.js ~ line 288 ~ let_getAllTasks= ~ _getAllTasks")
        const options = {
            headers: {
                'Content-type': 'application/json',
                "Authorization": 'Bearer ' + localStorage.getItem('token')
            }
        };
        let res = await axios.get(process.env.REACT_APP_DURL + 'ProjectProgress/' + SelectedProject, options)
        // setAllTasks(res.data);
        let data = res.data
        if (localStorage.getItem('totaltask') == null || localStorage.getItem('Finishedtask') == null || localStorage.getItem('Finishedtask') != data.finished || localStorage.getItem('totaltask') != data.all) {
            setProgress(((data.finished / data.all) * 100))
            settotalFinishedtask(data.finished)
            settotaltask(data.all)
            localStorage.setItem('totaltask', data.all)
            localStorage.setItem('Finishedtask', data.finished)
            // setProgress(47)
        } else {
            setProgress(parseInt(localStorage.getItem('Finishedtask')) / (parseInt(localStorage.getItem('totaltask'))) * 100)
            settotalFinishedtask(localStorage.getItem('Finishedtask'))
            settotaltask(parseInt(localStorage.getItem('totaltask')))
        }
    }

    let _getAllWorkDone = async () => {
        const options = {
            headers: {
                'Content-type': 'application/json',
                "Authorization": 'Bearer ' + localStorage.getItem('token')
            }
        };
        let res = await axios.get(process.env.REACT_APP_DURL + 'getAllTasksWorkDone/' + SelectedProject,options)
    }

    useLayoutEffect(() => {

            // _getProjectCost()
            _getIssuelist()
            // _getExpenselist()
            //_getallMedia()
        // _getProjectTasks()
        // _getAllTasks()
        Projectprogress()
            // _getAllWorkDone()
            // _getCostDetails();
    }, [SelectedProject])


    let refreshHandler = ()=>{
        // _getProjectTasks();
        _getProjectCost();
        _getIssuelist();
        // _getallMedia();
        _getExpenselist();
    }

    let downloadHandler = async () => {
        const options = {
            headers: {
                'Content-type': 'application/json',
                "Authorization": 'Bearer ' + localStorage.getItem('token')
            }
        };
        let res = await axios.get(process.env.REACT_APP_DURL + 'downloadprojectleveldpr/' + SelectedProject)
    }
    
    let sortedIssues = issueList.sort(function(a,b){
        return new Date(b.createdate) - new Date(a.createdate);
      });

    

    useEffect(()=>{
        if(SelectedProject && AllProjects.length !== 0){
            const params = {
                Project_wise: localStorage.getItem('pid'),
                User_wise: localStorage.getItem('userid'),
                projectName: SelectedProject !== undefined ? AllProjects.find(i => i._id == SelectedProject).Name : '',
                userName: JSON.parse(localStorage.getItem('user')).Name,
                TodaysDate: moment().format('DD-MM-YY hh:mm:ss A'),
                SlectedDate: '',
                taskid: localStorage.getItem('tid') || '',
                taskname: ''
            }
            logEvent(analytics, "dashboard_visited",params)
        }
    },[SelectedProject])



    let addCommentIssue = async () => {
        const options = {
            headers: {
                'Content-type': 'application/json',
                "Authorization": 'Bearer ' + localStorage.getItem('token')
            }
        };
        const data = {
            approverId: selectedIssue.approverId,
            createrId: selectedIssue.creatorId,
            issueId: selectedIssue.Issueid,
            taskId: '',
            commentText: commentText,
            Media: []
        }
        let res = await axios.put(process.env.REACT_APP_DURL + 'UpdateIssue',data,options)
        if(res.status == 200){
            setCommentModal(false)
        }
    }



    return <React.Fragment>
            {commentModal && <Card border='2px solid #22242E' shadow='none' className={styles.modal}>
            <h3 className={styles.modalHeader}>Add Comment</h3>
            <div className={styles.modalBody}><textarea onChange={(e)=>setCommentText(e.target.value)} placeholder='Write a comment...'/></div>
            <div className={styles.modalFooter}>
                <Button onClick={()=>{
                    document.getElementById('overview').style.filter = 'none'
                    setCommentModal(false)
                }} color="#22242E" bordered>Cancel</Button>
                <Button onClick={()=>{
                    addCommentIssue()
                    setCommentModal(false)
                    document.getElementById('overview').style.filter = 'none'
                }} color="#0A60BD" >Add Comment</Button>
            </div>
            </Card>}

        <div id="overview" style={ { background: '#f2f2f2' } } className={ styles.overview }>

            {tutorial0 && count == 0 && renderSteps1()}  
            {tutorial && count == 1 && renderSteps2()}  
            {!localStorage.getItem("secondvisit") && moment(new Date()).diff(moment(localStorage.getItem("tutorialdate")),"seconds") > 20 && renderSteps3()}        

            <h1 className={styles.pageHeading}>Dashboard Overview</h1>
            <div style={{display:'grid',gridTemplateColumns:'36% 60%',alignItems:'stretch',justifyContent:'space-between'}}>
            
                <Card style={{display:'flex',flexDirection:'column',alignItems:'center',justifyContent:'space-between'}} color='#fff'>
                <div className={styles.progressBar}>
                <CircularProgressbar
                         value={(progress) > 100 || isNaN(progress) ? '0' : parseInt(progress)}
                         text={`${(progress) > 100 || isNaN(progress) ? '0' : parseInt(progress)}%`}
                         circleRatio={0.75}
                         styles={buildStyles({
                             borderRadius: 20,
                         textColor:'#22242E',
                             pathColor: '#34B5CE',
                         rotation: 1 / 2 + 1 / 8,
                             strokeLinecap: "round",
                         trailColor: "#eee"
                        })}
                     />
                    <div className={styles.progressBackground}></div>
                </div>

                    <div style={ { display: 'flex', justifyContent: 'space-evenly', width: '100%' } }>
                        <div style={ { display: 'flex', justifyContent: 'center', alignItems: 'center', justifyItems: 'center' } }>
                            <div style={ { background: '#E2EDF9', fontSize: 20, paddingLeft: 12, paddingRight: 12, padding: 6, borderRadius: '10px', fontWeight: '600' } }>{ totaltask }</div>
                            <p style={ { width: '100%', color: '#000', marginLeft: 6, fontSize: 16 } }>Total Task</p>
                        </div>
                        <div style={ { display: 'flex', justifyContent: 'center', alignItems: 'center', justifyItems: 'center' } }>
                            <div style={ { background: '#DFECE0', fontSize: 20, paddingLeft: 12, paddingRight: 12, padding: 6, borderRadius: '10px', fontWeight: '600' } }>{ totalFinishedtask }</div>
                            <p style={ { width: '100%', color: '#000', marginLeft: 6, fontSize: 16 } }>Task Completed</p>
                        </div>
                   </div>

                    <div style={ { display: 'grid', gridTemplateColumns: '100%', justifyContent: 'space-around', width: '100%' } }>
                         <Link to="/visualProgress"><Button width="100%" color="#0A60BD">Work Progress</Button></Link>
                        {/* <Link to="/dpr"><Button width='100%' bordered color='#0A60BD'><i class="fas fa-download"></i> DPR</Button></Link> */ }
                     </div>
                 </Card>

                 <div>
                     <Card className={styles.costLeakageCard} shadow='none' gradient='linear-gradient(to right, #0A60BD , #34B5CE)'>
                         <div><img src={costleakageicon} /></div>
                         <div>
                            <p style={ { fontSize: 16 } }>All Project Insights</p>
                            <h3 style={ { color: '#fff' } } className={ styles.leakageMeter }>
                                {/* {costLeakages && costLeakages.toString().split('').map(i => {
                                 return <div>{i}
                                 <p className={styles.prevNum}>{i == '.' ? '' : parseInt(i)-1 >= 0 ? parseInt(i)-1 : ''}</p>
                                 <p className={styles.nextNum}>{i == '.' ? '' : parseInt(i)+1}</p>
                                 </div>
                             })} */}
                                13% less work progress this week
                            </h3>
                         </div>
                         <div style={{position:'relative'}}>
                            {/* <div className={styles.leakageBadge}>{totalLeakages.length}</div> */ }
                            {/* <div className={ styles.leakageBadge }>{ 28 }</div> */ }
                            <Link className={ styles.leakageLink } to='/costLeakages'><Card color='white' padding='10px'>Time Insights</Card></Link>
                         </div>
                     </Card>

                    <div style={{display:'grid',gridTemplateColumns:'48% 48%',justifyContent:'space-between',alignItems:'stretch'}}>
                        {/* <Card border='1px solid #848EA0' shadow='none'>
                             <h1 className={styles.cardHeading}>Project Profit</h1>
                             <div className={styles.overallProfit}>
                             <h2>Overall Profit</h2>
                             <h2><span className='rupee'>₹</span>
                             {isNaN(Math.abs(parseInt(actualCost) - parseInt(plannedCost))) ? '0' : Math.abs(parseInt(actualCost) - parseInt(plannedCost)) <= 0 ? CostFormatCommas(Math.abs(parseInt(actualCost) - parseInt(plannedCost))) : '0'}
                             </h2>
                             </div>
                             <div className={styles.line}></div>
                             <div className={styles.costSaved}>
                                 <h2>Cost Saved by Temlin</h2>
                                 <h2><span className="rupee">₹</span>{CostFormatCommas(costSaved)}</h2>
                             </div>
                             <div style={{display:'grid',gridTemplateColumns:'44% 44%',justifyContent:'space-around'}}>
                            <Link to="/costSaved"><Button width='100%' color='#0A60BD'>View</Button></Link>
                            <Link  to="/dpr"><Button width='100%' bordered color='#0A60BD'><i class="fas fa-download"></i> DPR</Button></Link>
                            </div>
                        </Card> */}
                        <div>
                            <Card style={ { backgroundColor: '#848EA0', height: '150px' } } shadow='none' gradient='linear-gradient(to right, #848EA0 , #848EA0)'>

                                <p style={ { width: '100%', color: '#fff', fontWeight: 'bold', fontSize: 18 } }>Project Safety Meter</p>
                                <div style={ { display: 'flex', justifyItems: 'center', marginTop: 8 } }>
                                    <img style={ { width: '68px', height: '68px', marginRight: 20 } } src={ security } />
                                    <div>
                                        <p style={ { color: '#fff' } }>Safe Man hours</p>
                                        <p className={ styles.leakageMeter }>
                                            { '052953'.toString().split('').map(i => {
                                                return <div>{ i }
                                                    <p className={ styles.prevNum }>{ i == '.' ? '' : parseInt(i) - 1 >= 0 ? parseInt(i) - 1 : '' }</p>
                                                    <p className={ styles.nextNum }>{ i == '.' ? '' : parseInt(i) + 1 }</p>
                                                </div>
                                            }) }
                                        </p>
                                    </div>
                                </div>
                            </Card>
                            <Card style={ { backgroundColor: '#848EA0', height: '150px' } } shadow='none' gradient='linear-gradient(to right, #848EA0 , #848EA0)'>

                                <p style={ { width: '100%', color: '#fff', fontWeight: 'bold', fontSize: 18 } }>Quality Inspection Raised Today</p>
                                <div style={ { display: 'flex', justifyContent: 'space-between', justifyItems: 'center', marginTop: 8 } }>
                                    <div>
                                        <div style={ { background: '#fff', width: '80px', padding: 6, borderRadius: '10px', fontWeight: '600' } }>50</div>
                                        <p style={ { width: '100%', color: '#fff', marginLeft: 6 } }>Total</p>
                                    </div>
                                    <div>
                                        <div style={ { background: '#fff', width: '80px', color: '#2AC371', padding: 6, borderRadius: '10px', fontWeight: '600' } }>25</div>
                                        <p style={ { width: '100%', color: '#fff', marginLeft: 6 } }>Approved</p>
                                    </div>
                                    <div>
                                        <div style={ { background: '#fff', width: '80px', color: '#D53539', padding: 6, borderRadius: '10px', fontWeight: '600' } }>25</div>
                                        <p style={ { width: '100%', color: '#fff', marginLeft: 6 } }>Pending</p>
                                    </div>
                                </div>
                            </Card>
                        </div>

                        <Card shadow='none' border="1px solid #848EA0" className={styles.pendingIssues}>
                            <h1 className={ styles.cardHeading }>Pending Issues</h1>
                            { sortedIssues.slice(0, 3).map((issue, index) => {
                                return <div style={ { padding: 10 } }>
                                <h3>{issue.issue}</h3>
                                <img className={styles.issueImage} onClick={()=>{
                                    setCommentModal(true)
                                    document.getElementById('overview').style.filter ='blur(3px)'
                                    setSelectedIssue(issue)
                                    }} src={commenticon} />
                                </div>
                            })}
                            <div style={{display:'grid',gridTemplateColumns:'50%',justifyContent:'center',marginTop:'10px',backgroundColor:'white'}}>
                            <Link to="/issuesAndExpenses" ><Button width="100%" bordered color='#0A60BD'>View</Button></Link>
                            </div>
                        </Card>
                    </div>
                 </div>
             </div>


            <div style={ { display: 'grid', gridTemplateColumns: '100% 42%', justifyContent: 'space-between' } }>
                 <Card color="#fff">
                     <h1 className={styles.cardHeading}>Live Updates</h1>
                     <div className={styles.notificationGroups}>
                         <button onClick={(e)=>{
                             e.preventDefault()
                             setNotificationFilter('')
                             setShowAll(true)
                             setShowExpense(false)
                             setShowIssue(false)
                             setShowWork(false)
                         }} className={showAll && styles.activeGroup}>All</button>
                         <button className={showIssue && styles.activeGroup} onClick={(e)=>{
                             e.preventDefault()
                             setNotificationFilter('issue')
                             setShowAll(false)
                             setShowExpense(false)
                             setShowIssue(true)
                             setShowWork(false)
                         }}>Issues</button>
                         <button className={showExpense && styles.activeGroup} onClick={(e)=>{
                             e.preventDefault()
                             setNotificationFilter('expense')
                             setShowAll(false)
                             setShowExpense(true)
                             setShowIssue(false)
                             setShowWork(false)
                            }}>Expenses</button>
                         <button className={showWork && styles.activeGroup} onClick={(e)=>{
                             e.preventDefault()
                             setNotificationFilter('workdone')
                             setShowAll(false)
                             setShowExpense(false)
                             setShowIssue(false)
                             setShowWork(true)
                             }}>Work</button>
                     </div>
                     {updates.length == 0 ? <p className={styles.noNotifications}>No new notifications.</p> 
                      : updates.map(update => {
                         if(notificationFilter == ''){
                         return <div className={styles.notification}>
                         <p className={styles.notificationTime}>{moment(update.time).format('hh:mm')}</p>
                         <p className={styles.notificationIcon}><i class="fas fa-exclamation"></i></p>
                         <p className={styles.notificationContent}>
                             <h4>{update.title}</h4>
                             <h5>{update.body}</h5></p>
                        </div>
                         } else if(notificationFilter == update.key){
                            return <div className={styles.notification}>
                                <p className={styles.notificationTime}>{moment(update.time).format('hh:mm')}</p>
                                <p className={styles.notificationIcon}><i class="fas fa-exclamation"></i></p>
                                <p className={styles.notificationContent}>
                                <h4>{update.title}</h4>
                                <h5>{update.body}</h5></p>
                         </div> 
                     }})}
                     
                 </Card>
                {/* <Card className={styles.bids} color="#fff">
                     <h1 className={styles.cardHeading}>Recent Bids</h1>
                     <p>Coming Soon!</p> */}
                     {/* <div className={styles.bid}>
                         <div><h2>Company Name</h2><h2><span className='rupee'>₹</span>00,000</h2></div>
                         <div><h3>Work order placed for</h3><Button color="#0A60BD">View Details</Button></div>
                     </div> */}
                {/* </Card> */ }
             </div>
             

        {/*
        <div style={{display:'grid',gridTemplateColumns:'72% 26%',justifyContent:'space-between',alignItems:'flex-start'}}>

             <div>
             <CustomContainer style={{position:'relative'}} className={`${styles.mainCard} ${styles.cost}`}>
                <h2 className={styles.containerHeading}>Project Cost</h2>
                <div style={{display:'grid',gridTemplateColumns:'55% 37%',justifyContent:'space-between'}}>
                <div>
                <div style={{display:'grid',gridTemplateColumns:'40% 50%',justifyContent:'space-between'}}>
                    <div className={styles.costCards}>
                    <div>
                        <h1>Actual Cost</h1>
                        <h3>₹ {actualCost.toString().length <= 5 ? actualCost : changeNumberFormat(actualCost,2)}</h3>
                    </div>
                    <div>
                        <h1>Planned Cost</h1>
                        <h3>₹ {plannedCost.toString().length <= 5 ? plannedCost : changeNumberFormat(plannedCost, 2)}</h3>
                    </div>
                    <div>
                        <h1>Profit/Loss</h1>
                        <h3 style={{backgroundColor: parseInt(actualCost) <= parseInt(plannedCost) ? '#15D500' : '#EA5253'}}>
                            ₹ {isNaN(Math.abs(parseInt(actualCost) - parseInt(plannedCost))) ? '0' : Math.abs(parseInt(actualCost) - parseInt(plannedCost)).toString().length <= 5 ? Math.abs(parseInt(actualCost) - parseInt(plannedCost)) : changeNumberFormat(Math.abs(parseInt(actualCost) - parseInt(plannedCost)),2)}</h3>
                    </div>
                    </div>

                    <div style={{position:'relative'}}>
                        <div className={styles.colorCode}>
                        <p>In-Progress</p>
                        <p>Completed</p>
                        </div>
                        <Chart 
                        progress={parseInt((allTasks?.filter(i => i.status ==  "INPROGRESS" || i.status == "CURRENT" || i.status == "DElAYED" || i.status == "PAUSE" || i.status == "START").length / allTasks?.length)*100)} 
                        completed={parseInt((allTasks?.filter(i => i.status ==  "END").length / allTasks?.length)*100)} />
                    </div>

                 </div>
                </div>

                <img className={styles.slantedImage} src={slanted} />
                <div className={styles.links}>
                <Link to='/costSaved'>
                    <CustomContainer className={styles.link} border-radius='5px' padding="20px" color='#FFF' >
                    <h1>Cost saved</h1>
                    <h3>₹ {costSaved}</h3>
                    </CustomContainer>
                    </Link>
                   <Link to='/costLeakages'>
                   <CustomContainer className={styles.link}  border-radius='5px' padding="20px" color='#FFF'>
                   <h1>Cost Leakages</h1>
                   <h3>₹ {costLeakages}</h3>
                   </CustomContainer>
                   </Link>
                </div>
              </div>
             </CustomContainer>

             <div style={{display:'grid',gridTemplateColumns:'60% 37%',justifyContent:'space-between'}}>

                 <CustomContainer className={`${styles.mainCard} ${styles.prediction}`}>
                 <h2 className={styles.containerHeading}>Project Prediction</h2>
                 <div style={{display:'grid',gridTemplateColumns:'48% 48%',justifyContent:'space-between'}}>
                    <CustomContainer color='#E3E3E3'>
                        <h1>Cost Prodiction:</h1>
                        <h3>predicted value</h3>
                    </CustomContainer>
                    <CustomContainer color='#E3E3E3'>
                        <h1>Time Prediction:</h1>
                        <h3>predicted value</h3>
                    </CustomContainer>
                 </div>
                 </CustomContainer>

                 <CustomContainer id='pendingissues' className={`${styles.mainCard} ${styles.pendingIssues}`} >
                    <h2 className={styles.containerHeading}>Pending Issues</h2>
                  <div className={styles.count}><img src={pendingissues}/> {issueList.filter(issue => issue.status === "Pending").length} Pending Issues</div>
                     {sortedIssues.slice(0,3).map((issue,index) => {
                            if(issue.status === "Pending") {
                                return (
                                    <div key={index} className={styles.issuelist}>
                                    <div>
                                        <h2 style={{fontSize:'1rem',color:'#363565'}}>{issue.issue}</h2>
                                        <h4 style={{color:'#535272',fontSize:'1rem'}}>Task: {issue.name}</h4>
                                    </div>
                                    <div style={{display:'flex',alignItems:'center',justifyContent:'space-between'}}>
                                        <div className={styles.date}>{moment(issue.createdate).format('DD MMM YYYY')}</div>
                                    </div>
                                </div>
                          )}})}
                        <Link onClick={()=>{
                            const params = {
                                project_id: localStorage.getItem('pid'),
                                user_id: localStorage.getItem('userid'),
                                project_name: SelectedProject !== undefined ? AllProjects.find(i => i._id == SelectedProject).Name : '',
                                user_name: JSON.parse(localStorage.getItem('user')).Name,
                                time: moment().format('DD-MM-YY hh:mm:ss A'),
                            }
                            logEvent(analytics, "pendingissues_viewall",params)
                            setIssue(true)
                        }} to='/issuesAndExpenses'><p className={styles.viewAll}>Tap to view more</p></Link>
                    </CustomContainer>
                 </div>
             </div>  

             <div className={styles.notifications}>
                 
            </div> 
            </div> */}

           

             {/* <div style={{display:'grid',gridTemplateColumns:'30% 68%',justifyContent:'space-between'}}>
             <CustomContainer id='chart' border-radius='6px' border='1px solid #707070' padding="14px" color='#FFF'>
                 <h2 className={styles.containerHeading}>Project Progress</h2>
               <Chart i={ ProjectTasks?.INPROGRESS } e={ ProjectTasks?.COMPLETED } d={ ProjectTasks?.Delayed } p={ ProjectTasks?.PAUSE } />
               </CustomContainer>


               <CustomContainer id='cost' border-radius='6px' border='1px solid #707070' padding="14px" color='#FFF'>
                   <div style={{display:'flex',alignItems:'center',justifyContent:'space-between'}}>
                   <h2 className={styles.containerHeading}>Project Cost</h2>
                   <a id='profitreport' className={styles.projectDprDownload} href={`${process.env.REACT_APP_DURL}downloadprojectleveldpr/${SelectedProject}`} download
                   onClick={downloadHandler}>Profitability Report</a>
                   </div>
                    
                     <div style={{display:'grid',gridTemplateColumns:'40% 40%',justifyContent:'space-around',alignItems:'center',margin:'30px 0'}}>
                    <div style={{textAlign:'center', fontSize:'1.7rem'}} className='planned-cost'>Planned Cost<h3 style={{fontSize:'1.6rem',marginTop:'15px'}}>
                     ₹ { plannedCost.toString().length <= 5 ? plannedCost : changeNumberFormat(plannedCost, 2) }</h3></div>
                 
                <div style={{textAlign:'center',fontSize:'1.7rem'}} className='actual-cost'>Actual Cost<h3 style={{fontSize:'1.6rem',marginTop:'15px'}}>
                 ₹ {actualCost.toString().length <= 5 ? actualCost : changeNumberFormat(actualCost,2)}</h3></div>
                
                 </div>
                   <div style={{display:'grid',gridTemplateColumns:'40%',justifyContent:'center'}}>
                   <div style={{textAlign:'center'}} className={`${parseInt(actualCost) <= parseInt(plannedCost) ? 'profit-class-total' : 'loss-class-total'}`}>
                    <h3 style={{fontSize:'1.7rem'}}>
                       {parseInt(actualCost) <= parseInt(plannedCost) ? 'Profit' : 'Loss'}</h3>
                     <h3 style={{fontSize:'1.6rem',marginTop:'15px'}}>&nbsp;₹ {isNaN(Math.abs(parseInt(actualCost) - parseInt(plannedCost))) ? '0' : Math.abs(parseInt(actualCost) - parseInt(plannedCost)).toString().length <= 5 ? Math.abs(parseInt(actualCost) - parseInt(plannedCost)) : changeNumberFormat(Math.abs(parseInt(actualCost) - parseInt(plannedCost)),2)}</h3>
                    </div>
                   </div>
                 

             </CustomContainer>
             </div>


             <div style={{display:'grid',gridTemplateColumns:'36% 62%',alignItems:'flex-start',justifyContent:'space-between'}}>
             <CustomContainer id='pendingissues' border-radius='6px' border='1px solid #707070' padding="14px" className={styles.pendingIssues} color='#FFF'>
                    <h2 className={styles.containerHeading}>Pending Issues</h2>
                    
                 <div className={styles.count}><img src={pendingissues}/> {issueList.filter(issue => issue.status === "Pending").length} Pending Issues</div>
                     {sortedIssues.slice(0,3).map((issue,index) => {
                            if(issue.status === "Pending") {
                                return (
                                    <div key={index} className={styles.issuelist}>
                                    <div>
                                        <h2 style={{fontSize:'1.2rem',color:'#363565'}}>{issue.issue}</h2>
                                        <h4 style={{color:'#535272',fontSize:'1.1rem'}}>Task: {issue.name}</h4>
                                    </div>
                                    <div style={{display:'flex',alignItems:'center',justifyContent:'space-between'}}>
                                        <div className={styles.date}>{moment(issue.createdate).format('DD MMM YYYY')}</div>
                                    </div>
                                </div>
                                )
                            }
                        })}
                        <Link onClick={()=>{
                            const params = {
                                project_id: localStorage.getItem('pid'),
                                user_id: localStorage.getItem('userid'),
                                project_name: SelectedProject !== undefined ? AllProjects.find(i => i._id == SelectedProject).Name : '',
                                user_name: JSON.parse(localStorage.getItem('user')).Name,
                                time: moment().format('DD-MM-YY hh:mm:ss A'),
                            }
                            logEvent(analytics, "pendingissues_viewall",params)
                            setIssue(true)
                        }} to='/issuesAndExpenses'><p className={styles.viewAll}>Tap to view more</p></Link>
                    </CustomContainer>

                    
                    </div> */}

                    {/* <div>
                      <CustomContainer id='lossmakingtasks' border-radius='6px' border='1px solid #707070' padding="14px" color='#FFF'>
                        <h2 className={styles.containerHeading}>Loss Making Tasks</h2>
                    {allTasks.map(task => {
                        if(task.ActualCost > task.PlannedCost){
                        return <CustomContainer border-radius='4px' color='#FF00001A' className={styles.lossTasks}>
                        <div>
                        <h3>{task.name}</h3>
                        <p>Actual Cost - {task.ActualCost}</p>
                        <p>Planned Cost - {task.PlannedCost}</p>
                        <h4>{task.issues.length == 0 ? 'No Pending Issues' : `${task.issues.length} Pending Issues`}</h4>
                        </div>
                        <div className={styles.progress}>
                            <div style={{display:'flex',alignItems:'center',justifyContent:'space-between'}}>
                                <p>{moment(task.date.end).format('DD/MM/YY')}</p>
                                <p>{parseInt(((task.ActualQuantity / task.TotalQuantity)*100))}%</p>
                            </div>
                            <div className={styles.progressBar}>
                                <div style={{width:`${(task.ActualQuantity / task.TotalQuantity)*100}%`}}></div>
                            </div>
                        </div>
                      </CustomContainer>
                 }})}</CustomContainer>
              </div> */}
            </div>
        </React.Fragment>
}

export default Overview