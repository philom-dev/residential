import styled from 'styled-components';

export let Button = styled.button`
padding: 8px 20px;
font-weight: 600;
font-size: 1.2rem;
color: ${(props) => props.bordered ? props.color : '#fff'};
padding: ${(props) => props.padding};
border-radius: 8px;
text-align: center !important;
cursor: pointer;
transition: all .3s;
border-radius: ${(props) => props.borderRadius};
background-color: ${(props) => props.bordered ? 'white' : props.color};
margin: ${(props)=>props.margin};
border: ${(props) => props.bordered ? `2px solid ${props.color}` : `2px solid ${props.color}`};
width: ${(props)=>props.width};
`