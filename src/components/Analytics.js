import { initializeApp } from "firebase/app";
import { getAnalytics, logEvent} from "firebase/analytics";

const firebaseConfig = {
    apiKey: "AIzaSyAGLut_qi52Y5eZcx8imtphVOu3j6V2bSo",
    authDomain: "temlin-b29c9.firebaseapp.com",
    projectId: "temlin-b29c9",
    storageBucket: "temlin-b29c9.appspot.com",
    messagingSenderId: "1048537949060",
    appId: "1:1048537949060:web:4ec45cad8c547c5641f3d1",
    measurementId: "G-6R9JMEM3MV"
  };

const app = initializeApp(firebaseConfig);
export const analytics = getAnalytics(app)
