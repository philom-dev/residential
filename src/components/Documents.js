import { Container } from "./Container.style";
import styles from "./Documents.module.css";
import pdfLogo from "../logo/pdf.png";
import React, {useContext, useEffect, useRef, useState} from "react";
import { FileDrop } from 'react-file-drop';
import DragDrop from "./DrapDrop";
import axios from "axios";
import { Link } from "react-router-dom";
import SelectedProjectContext from "../context/selectdProjectContext";
import AllprojectsContext from '../context/AllprojectsContext';
import { Steps, Hints } from 'intro.js-react';
import { CustomContainer } from "./CustomContainer.style";
import { Card } from "./Card.style"
import { Button } from "./Button.style"
import foldericon from "../resources/icons/foldericon.png"
import pdficon from "../resources/icons/pdficon.svg"
import excelicon from "../resources/icons/excelicon.svg"
import wordicon from "../resources/icons/wordicon.png"
import { analytics } from "./Analytics"
import { logEvent } from "@firebase/analytics";
import moment from "moment";
import newfolder from "../resources/icons/newfolder.png"
import ImageModal from "./ImageModal";


function Documents(){
 
    const { SelectedProject, setSelectedProject } = useContext(SelectedProjectContext)
    const { AllProjects } = useContext(AllprojectsContext)
    const [folderModal, setFoldermodal] = useState(false);
    const [folders, setFolders] = useState([])
    const [allMedia, setAllMedia] = useState([])
    const folderName = useRef();
    const [tutorial, setTutorial] = useState(localStorage.getItem('tutorial2') ? false : true)
    const [recents, setRecents] = useState([])
    const [media, setMedia] = useState([])
    const [documents, setDocuments] = useState([])
    const [imageModal, setImageModal] = useState(false);
    const [displayImage, setDisplayImage]= useState('')

    let steps = [
        {
            intro: "<h1>Welcome to the Documents section!</h1><p>Click on next to have an overview of the section.</p>",
            tooltipClass: 'welcomemsg',
        },
        {
            element: '#folders',
            intro: '<h1>Folders</h1><p>Here are the folders of your project!</p>',
            tooltipClass:'tooltip',
            position: 'top',
            disableInteraction:true
        },
        {
            element: '#documents',
            intro: '<h1>Documents</h1><p>Here are your project documents. Click view all to see all the project documents.</p>',
            tooltipClass:'tooltip',
            position: 'right',
            disableInteraction:true
        },
        {
            element: '#media',
            intro: '<h1>Media</h1><p>Here is the project media. Click view all to see all the project media (photos).</p>',
            tooltipClass:'tooltip',
            position: 'left',
            disableInteraction:true
        },
        {
            intro: '<h1>Great Going.</h1><p>You are doing great.</p>',
            tooltipClass:'tooltip',
        }
    ]
    
    
    let folderNameHandler = (e) => {
        folderName.current = e.target.value;
    }

    const _createFolder = async () => {
    let data = {
        name: folderName.current,
        projectId: localStorage.getItem('pid'),
    }
    const options = {
        headers: {
            'Content-type': 'application/json',
            "Access-Control-Allow-Origin": "*",
            "Authorization": 'Bearer ' + localStorage.getItem('token')
        }
    }

    const res = await axios.post(process.env.REACT_APP_DURL + 'createFolder',data,options)
    if(res.status == 200){
        // for analytics ------
        const params = {
            Project_wise: localStorage.getItem('pid'),
            User_wise: localStorage.getItem('userid'),
            projectName: SelectedProject !== undefined ? AllProjects.find(i => i._id == SelectedProject).Name : '',
            userName: JSON.parse(localStorage.getItem('user')).Name,
            TodaysDate: moment().format('DD-MM-YY hh:mm:ss A'),
            SlectedDate: '',
            taskid: localStorage.getItem('tid') || '',
            taskname: ''
        }
        logEvent(analytics, "create_folder_dashboard",params)
        // -------------
    }
   }

    const _getProjectFolders = async () => {
        let options = {
            headers: {
                'Content-type': 'application/json',
                "Access-Control-Allow-Origin": "*",
                "Authorization": 'Bearer ' + localStorage.getItem('token')
            }

        }
        let res = await axios.get(process.env.REACT_APP_DURL + 'allmedia/'+localStorage.getItem('pid'),options)
        if(res.status == 200){
            let arr = res.data[0].folder.sort(function(a,b){
                return new Date(b.createdAt) - new Date(a.createdAt);
              });
            setFolders(arr.reverse());
        }

       
    }

    const _getAllMedia = async () => {
        const options = { 
            headers: {
                'Content-type': 'application/json',
                "Authorization": 'Bearer ' + localStorage.getItem('token')
            }
        };
        let res = await axios.get(process.env.REACT_APP_DURL + 'allmedia/' + SelectedProject,options)
       let mediaArr = []
       let documentsArr = []
       let recentsArr = []
       res.data[0].task.map(i => {
           if(i.Media.length !== 0){
           i.Media.map(m => {
               if(m.mediaType == 'pdf' || m.mediaType == 'xlsx' || m.mediaType == 'docx'){
                   documentsArr.push(m)
                   recentsArr.push({
                       task: i.name,...m})
               } else if(m.mediaType == 'jpg' || m.mediaType == 'jpeg' || m.mediaType == 'png'){
                   mediaArr.push(m)
                   recentsArr.push({
                      task:i.name,...m})
               }
           });
           }
       })
       setDocuments(documentsArr.sort(function(a,b){
        return new Date(moment(b.createdate,"DD-MM-YYYY").format('YYYY,M,DD')) - new Date(moment(a.createdate,"DD-MM-YYYY").format('YYYY,M,DD'))
        }))
       setMedia(mediaArr.sort(function(a,b){
        return new Date(moment(b.createdate,"DD-MM-YYYY").format('YYYY,M,DD')) - new Date(moment(a.createdate,"DD-MM-YYYY").format('YYYY,M,DD'))
        }))
       setRecents(recentsArr.sort(function(a,b){
        return new Date(moment(b.createdate,"DD-MM-YYYY").format('YYYY,M,DD')) - new Date(moment(a.createdate,"DD-MM-YYYY").format('YYYY,M,DD'))
        }))
    }

    let renderSteps = () => {
        return  <Steps
        enabled={true}
        steps={steps}
        initialStep={0}
        // onComplete={()=>{
        //     setCount(1);
        //     console.log('tutorial finished')
        // }}
        onExit={(e)=>{
            localStorage.setItem('tutorial2','finished')
            setTutorial(false)
            console.log(e)}}
        options={{
            skipLabel:'Skip',
            // hideNext:true,
            doneLabel: "Done",
            showProgress: true,
            showButtons:true,
            showStepNumbers: true,
            disableInteraction:false,
            exitOnOverlayClick:false,
            overlayOpacity: 0.6
        }} 
        />
    }

    useEffect(()=>{
        _getAllMedia()
         _getProjectFolders();
        logEvent(analytics, "documents_visited")
    },[SelectedProject])


    return <React.Fragment>

        {imageModal && <ImageModal image={displayImage} close={()=>{
            setImageModal(false)
            document.getElementById('documents').style.filter ='none';
         }} />}

        {tutorial && renderSteps()}
      
        {/* Create folder modal : */}
        {folderModal && <Card border='2px solid #22242E' shadow='none' className={styles.modal}>
            <h3 className={styles.modalHeader}>Create Folder</h3>
            <div className={styles.modalBody}><input onChange={folderNameHandler} type='text' placeholder='Folder Name'/></div>
            <div className={styles.modalFooter}>
                <Button onClick={()=>{
                    setFoldermodal(false)
                    document.getElementById('documents').style.filter = 'none'
                }} color="#22242E" bordered>Cancel</Button>
                <Button onClick={()=>{
                    _createFolder()
                    setFoldermodal(false)
                    _getProjectFolders()
                    document.getElementById('documents').style.filter = 'none'
                }} color="#0A60BD" >Create</Button>
            </div>
            </Card>}
        {/* ----------------------------- */}
            <div id='documents' className={styles.documents}>
               <Card className={styles.card}>
                    <div className={styles.heading}><h1>Recently Uploaded</h1></div>
                    <div className={styles.recent}>
                    {recents.slice(0,6).map(j => {
                    if(j.mediaType === 'xlsx' || j.mediaType == 'pdf' || j.mediaType == 'docx') {
                    return <div className={styles.doc}>
                    <a className={styles.link} href={j.MediaUrl} download>
                    <div className={styles.image}><img src={j.mediaType == 'xlsx' ? excelicon : j.mediaType == 'pdf' ? pdficon : j.mediaType == 'docx' ? wordicon :''}/></div>
                     <p>{j.createdate}<p className={styles.moreDetails}>{j.task}</p></p>
                     
                     </a>
                    </div>
                    } else if(j.mediaType == "jpg" || j.mediaType == 'png' || j.mediaType == 'jpeg') {
                      return <div className={styles.doc}>
                       <div className={styles.link}><div className={styles.image}> <img onClick={()=>{
                             setDisplayImage(j.MediaUrl)
                             setImageModal(true)
                             document.getElementById('documents').style.filter = 'blur(3px)'
                             }} className={styles.mediaImage} src={j.MediaUrl}/></div>
                        <p>{j.createdate}<p className={styles.moreDetails}>{j.task}</p></p></div>
                     </div> }})}
                  </div>
                </Card>
            <div style={{display:'grid',gridTemplateColumns:'31% 31% 31%',justifyContent:'space-between',alignItems:'stretch'}}>

            <Card id='folders' style={{position:'relative'}} className={styles.card}>
                <h1>Project Folders</h1>
                <div className={styles.folders}>
                {tutorial && <div className={styles.folder}>
                <div><img src={foldericon} /></div>
                <p>New Folder</p>
                </div>}
                <div onClick={(e) => {
                    setFoldermodal(true)
                    document.getElementById('documents').style.filter = 'blur(3px)'
                    }}
                    className={styles.newfolder}>
                    <i className="fas fa-plus"></i>
                    <img src={newfolder}/>
                </div>
                {folders.slice(0,8).map(folder => {
                return <Link to={`/folderDetails/${folder.folder_id}`} className={styles.folder} >
                <div><img src={foldericon} /></div>
                <p>{folder.folder_name}</p>
                </Link>
                })}
                 </div>
             </Card>

            <Card style={{position:'relative'}} id='documents' className={styles.card}>
               <div className={styles.heading}>
               <h1>Project Documents</h1>
                <Link to='/documents/allFiles' className={styles.viewAll} href='#'>View All</Link>
               </div>

                <div className={styles.docs}>
                {tutorial && <div className={styles.doc}>
                <a href='#' download>
                <div className={styles.image}><img src={excelicon}/></div>
                 <p>01-01-2021</p>
                 </a>
                </div>}

                {documents.slice(0,4).map(j => {
                return <div className={styles.doc}>
                <a href={j.MediaUrl} download>
                <div className={styles.image}><img src={j.mediaType == 'xlsx' ? excelicon : j.mediaType == 'pdf' ? pdficon : j.mediaType == 'docx' ? wordicon : ''}/></div>
                 <p>{j.createdate}</p>
                 </a>
                </div>
                })}
                </div>
            </Card>

            <Card style={{position:'relative'}} id='media' className={styles.card}>
                <div className={styles.heading}>
                <h1>Project Media</h1>
                <Link to='/documents/allMedia' className={styles.viewAll} href='#'>View All</Link>
                </div>

                <div className={styles.docs}>

               {tutorial &&  <div className={styles.doc}>
                <div className={styles.image}><img className={styles.mediaImage} src='https://cdn.pixabay.com/photo/2017/08/03/21/37/construction-2578410__340.jpg'/></div>
                <p>01-01-2021</p>
                </div>}

                {media.slice(0,4).map(j => {
                return <div className={styles.doc}>
               <div className={styles.image}><img onClick={()=>{
                             setDisplayImage(j.MediaUrl)
                             setImageModal(true)
                             document.getElementById('documents').style.filter = 'blur(3px)'
                             }} className={styles.mediaImage} src={j.MediaUrl}/></div>
                <p>{j.createdate}</p>
                </div>})}
                </div>
             </Card>
            </div>
        </div>
    </React.Fragment>
}

export default Documents;