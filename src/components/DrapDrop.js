import React, {useCallback, useState} from 'react'
import {useDropzone} from 'react-dropzone'
import axios from 'axios';

function DragDrop() {
 
  let [files, setFiles] = useState();
  const onDrop = (acceptedFiles) => {
    setFiles(acceptedFiles.map(file => Object.assign(file, {
      preview: URL.createObjectURL(file)
      
    })));

  }
  const {getRootProps, getInputProps} = useDropzone({onDrop})
  console.log(files);


  const upload = () => {
    const uploadURL = process.env.REACT_APP_Cloudinary_Url;
    const uploadPreset = process.env.REACT_APP_upload_preset;
 
    files.forEach(file => {
      const formData = new FormData();
      formData.append('file', file);
      formData.append('upload_preset', uploadPreset);
      formData.append("api_key", process.env.REACT_APP_API_KEY);
      formData.append("cloud_name",process.env.REACT_APP_cloud_name);

      axios({
        url: uploadURL,
        method: "POST",
        headers: {
          "Content-Type": "application/x-www-form-urlencoded"
        },
        data: formData
      })
      .then(res => console.log(res))
      .catch(err => console.log(err))
    })
  }

  return (
    <div {...getRootProps()}>
      <input {...getInputProps()} />
      <p>Drag 'n' drop some files here, or click to select files</p>
    </div>
  )

  
 
}
export default DragDrop