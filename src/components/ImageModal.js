import styles from "./ImageModal.module.css"
import {Card} from "./Card.style"
import {Button} from "./Button.style"

function ImageModal(props) {
    return <Card className={styles.imageModal}>
      <img className={styles.modalImage} src={props.image}/>
    <Button color="#22242E" bordered onClick={props.close} className={styles.closeImageModal}><i className="fas fa-times"></i></Button>
    <a href={props.image} download className={styles.downloadBtn}><Button color="#22242E">Download</Button></a>
    </Card>
}

export default ImageModal