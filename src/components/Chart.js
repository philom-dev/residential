import { PieChart, Pie, Sector, Cell, ResponsiveContainer, Tooltip } from 'recharts';
import React, { PureComponent, useState } from 'react';

const COLORS = [ '#65cc99', '#2a6678'];

const Chart = (props) => {

  const data = [
    { name: 'Completed', value: props.completed },
    { name: 'In-Progress', value: props.progress }
  ];

  const CustomTooltip = ({ active, payload, label }) => {
    if (active && payload && payload.length) {
      return (
        <div className="custom-tooltip" style={ {
          backgroundColor: '#ffffff',
          padding: 10, borderRadius: 100
        } }>
          <p className="label">{`${payload[0].name} ${payload[0].value}%`}</p>
        </div>
      );
    }

    return null;
  };

  const onPieEnter = () => {

  }
  return (
    <div style={ { display: 'flex', flexDirection: 'column', alignItems: 'center' } }>
      <PieChart width={ 200 } height={ 200 } onMouseMove={ onPieEnter }>
        <Pie data={ data } innerRadius={ 45 } outerRadius={ 80 }
          fill="#8884d8" paddingAngle={ 5 } dataKey="value"
        >
          { data.map((entry, index) => (
            <Cell key={ `cell-${index}` } fill={ COLORS[ index % COLORS.length ] }
              values='hbhbjh' />
          )) }
        </Pie>
        <Tooltip content={ <CustomTooltip /> } />
      </PieChart>
    </div>
  );
}

export default Chart