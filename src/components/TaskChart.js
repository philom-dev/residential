import axios from 'axios';
import React, { PureComponent, useEffect, useLayoutEffect, useState } from 'react';
import {
  BarChart,
  Bar,
  Brush,
  ReferenceLine,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend,
  ResponsiveContainer,
} from 'recharts';


export default function TaskChart(props) {

    const [tasks,setTasks] = useState([]);
    const [list, setList] = useState([]);
    console.log("🚀 ~ file: TaskChart.js ~ line 21 ~ TaskChart ~ list", list)

    let _getTasks = async () => {
        const options = {
            headers: {
                'Content-type': 'application/json',
                "Authorization": 'Bearer ' + localStorage.getItem('token')
            }
        };
        let res = await axios.get(process.env.REACT_APP_DURL + 'getprojecttask/' + localStorage.getItem('pid'),options)
        // console.log(res.data);
        setTasks([...res.data.INProgressDoc,...res.data.compeleteddoc,...res.data.Upcomingdoc,...res.data.Delayeddoc,...res.data.Startdoc,...res.data.currentdoc,...res.data.pausedoc,...res.data.rejecteddoc]);
    }

    let arr = [];
   const _getTaskDetails = () =>{
      tasks.slice(0,10).map(i => {
        let data = {
          name: i.name,
          Planned: i.PlanedCost,
          Actual: i.rate
        }
        arr.push(data)
      })

      return arr
    }


    // console.log(arr);
  
     const data = [
         { name: 'task one',  Planned: 100 ,Actual: 70}, 
         { name: 'task two',  Planned: 240 ,Actual: 200}, 
         { name: 'task three',  Planned: 120 ,Actual: 110}, 
         { name: 'task four',  Planned: 440 ,Actual: 220}, 
       ];

      useLayoutEffect(()=>{
          _getTasks();
      },[])

    return (
        <BarChart
          width={1200}
          height={500}
          data={list}
          margin={{
            top: 5,
            right: 30,
            left: 20,
            bottom: 5,
          }}
        >
          <CartesianGrid strokeDasharray="3 3" />
          <XAxis dataKey="name" />
          <YAxis />
          <Tooltip />
          <Legend verticalAlign="top" wrapperStyle={{ lineHeight: '40px' }} />
          <ReferenceLine y={0} stroke="#000" />
          <Brush dataKey="name" height={30} stroke="#8884d8" />
          <Bar dataKey="Planned" fill="#8884d8" />
          <Bar dataKey="Actual" fill="#82ca9d" />
        </BarChart>
    );
}