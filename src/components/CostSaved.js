import styles from "./CostSaved.module.css"
import { CustomContainer } from "./CustomContainer.style";
import { Card } from "./Card.style";
import manicon from "../resources/icons/manicon.png"
import materialicon from "../resources/icons/materialicon.png"
import machineicon from "../resources/icons/machineicon.png"
import { useEffect, useState } from "react";
import {CostFormatCommas} from "./CostFormatCommas"
import axios from "axios";

function CostSaved() {

    const [showDetails, setShowDetails] = useState(false)
    const [selectedData, setSelectedData] = useState('')
    const [list, setList] = useState([])
    const [dataList, setDataList] = useState([])
    

    let _getCostDetails = async () => {
        const options = {
           headers: {
               'Content-type': 'application/json',
               "Authorization": 'Bearer ' + localStorage.getItem('token')
           }
        };
        let res = await axios.get(process.env.REACT_APP_DURL + "getLeakage/" + localStorage.getItem('pid'),options)
        if(res.status == 200){
            setList(res.data)
            setDataList([...res.data.Macdata, ...res.data.Matdata])
        }
   }

   useEffect(()=>{
       _getCostDetails()
   },[localStorage.getItem('pid')])


    return <div className={styles.container}>

        <div className={styles.header}>
            <Card border='2px solid #22242E' className={styles.card} color='#22242E'>
                <h1>Total Cost Saved</h1>
                <h2><span className="rupee">₹</span>{CostFormatCommas(parseInt(list.MachineSavedCost || 0 + list.MaterialSavedCost || 0))}</h2>
            </Card>

            {/* <Card shadow='none' className={styles.card}>
                <div className={styles.headerIcon}><img src={manicon} /></div>
                <h3>Man</h3>
                <h4><span className="rupee">₹</span>00,00,000</h4>
            </Card> */}

            <Card border='2px solid #0A60BD' shadow='none' className={styles.card}>
                <div className={styles.headerIcon}><img src={machineicon} /></div>
                <h3>Machine</h3>
                <h4><span className="rupee">₹</span>{CostFormatCommas(parseInt(list.MachineSavedCost || 0))}</h4>
            </Card>
            <Card border='2px solid #0A60BD' shadow='none' className={styles.card}>
                <div className={styles.headerIcon}><img src={materialicon} /></div>
                <h3>Material</h3>
                <h4><span className="rupee">₹</span>{CostFormatCommas(parseInt(list.MaterialSavedCost || 0))}</h4>
            </Card>
        </div>

        <div className={styles.table}>
             <div className={styles.tableheader}>
                <p>Resource Name</p>
                <p>Amount</p>
            </div>

            <div className={styles.tablebody}>

            {dataList.map(i => {
                return <div onClick={()=>{
                    setShowDetails(true)
                    setSelectedData(i._id)
                    if(selectedData == i._id){
                        setShowDetails(false)
                        setSelectedData('')
                    }
                    }} className={styles.tablerow}>
                <p>{i.MachineReadingId ? i.MachineReadingId.MachineName : i.MaterialProducedId ? i.MaterialProducedId.ProductName : 'No name specified'} <span>{i.taskId && `(${i.taskId.name})`}</span></p>
                <p style={{display:'grid',gridTemplateColumns:'50% 50%',justifyContent:'space-between',alignItems:'center'}}>
                    <p>{i.SavedCost ? `${CostFormatCommas(parseInt(i.SavedCost))}` : 'Not specified'}</p>
                    <p className={styles.tableicons}>
                        <div style={{backgroundColor: i.MachineReadingId ? '#15D500' : '#979797'}} className={styles.tableicon}><img src={machineicon} /></div>
                        <div style={{backgroundColor: i.MaterialProducedId ? '#15D500' : '#979797'}} className={styles.tableicon}><img src={materialicon} /></div>
                    </p>
                </p>
                {showDetails && selectedData == i._id && <p className={styles.moreDetails}>{i.Expression ? i.Expression : 'No Expression'}</p>}
                </div>
            })}
            </div>
        </div>
    </div>
}
export default CostSaved