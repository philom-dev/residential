import { Container } from "./Container.style";
import React, { useContext, useEffect, useState } from 'react';
import axios from "axios";
import moment from "moment";
import styles from "./Task.module.css"
import { RefreshBtn } from "./RefreshBtn.style";
import SelectedProjectContext from "../context/selectdProjectContext";
import { Link } from "react-router-dom";
import { CustomContainer } from "./CustomContainer.style";
import {Card} from "./Card.style";
import manicon from "../resources/icons/manicon.png"
import materialicon from "../resources/icons/materialicon.png"
import machineicon from "../resources/icons/machineicon.png"
import CustomCalander from "./CustomCalendar";
import io from "socket.io-client";
import {nanoid} from "nanoid";
// const socket = io(process.env.REACT_APP_DURL);
const username = nanoid(4)

const bgcolor = [ '#f7d5d8', '#d7eded' ]
const Textcolor = [
    '#d52d3e', '#38a3a5'

]


function Task() {

    const { SelectedProject, setSelectedProject } = useContext(SelectedProjectContext)
    const [ TaskDetails, setTaskDetails ] = useState([])
    const [ labour, setlabour ] = useState([])
    const [ machine, setmachine ] = useState([])
    const [ material, setmaterial ] = useState([])
    const [ taskprogres, settaskprogres ] = useState([])
    const [ Deduction, setdeductions ] = useState([])
    const [ MediaArr, setMediaArr ] = useState([])
    const Todaydate = moment().format('YYYY-MM-DD')
    const [ selecteddate, setselecteddate ] = useState(Todaydate)
    const [ issueList, setissueList ] = useState([])
    const [ PlannedCost, setPlannedCost ] = useState([])
    const [ ActualCost, setActualCost ] = useState([])
    const [ unit, setunit ] = useState('')
    const [ ExpenseList, setExpenseList ] = useState([])
    const [ tid, settid ] = useState('')
    const [ Taskissue, setTaskissue ] = useState([])
    const [ TaskExpense, setTaskExpense ] = useState([])
    const [TaskProgressQty, setTaskProgressQty] = useState(0);
    const [totalQTY, settotalQTY] = useState(0);
    const [imageModal, setImageModal] = useState(false);
    const [displayImage, setDisplayImage]= useState('')
    const [comments, setComments] = useState([]);
    const [comment, setComment] = useState('');
    const [manRate, setManRate] = useState('')
    const [materialRate, setMaterialRate] = useState('')
    const [machineRate, setMachineRate] = useState('')
    const [message, setMessage] = useState('')
    const [chat, setChat] = useState([]);

    // const sendMessage = (e) => {
    //     e.preventDefault();
    //     socket.emit('chat',{message, username: JSON.parse(localStorage.getItem('user')).Name})
    //     setMessage('')
    // }

    // useEffect(()=>{
    //     socket.on('chat',(payload)=>{
    //         setChat([...chat, payload])
    //     })
    // },[])

    useEffect(()=>{
        if (!("Notification" in window)) {
            console.log("This browser does not support desktop notification");
          } else {
            Notification.requestPermission();
          }
    },[])

    let count = 0;
    let counter = 0;

    function _updateTaskQty(){

        taskprogres.map(res =>{
            if(moment(selecteddate).format('DD-MM-YYYY') == res?.Date ){
            counter += parseFloat(res.TaskProgressQuantity)
        }})
        return counter
    }
//     let changeHandler = (start) => {
//         setSelectedDate(moment(start).format('DD-MM-YYYY'));
//    }

useEffect(()=>{
    settotalQTY(_updateTaskQty())

},[SelectedProject,taskprogres, selecteddate])


    function todaysdate(e) {
        let f = moment(e.target.value).format('YYYY-MM-DD')
        setselecteddate(f)
    }

    async function _getIssuelist() {
        let pid = localStorage.getItem('pid')
        const options = {
            headers: {
                'Content-type': 'application/json',
                "Authorization": 'Bearer ' + localStorage.getItem('token')
            }
        };
        const res = await axios.get(process.env.REACT_APP_DURL + 'issuelist/' + pid,options)
        if(res.status == 200){
            if(tid !== ''){
                setissueList(res?.data.filter(i => i.taskid == tid && moment(i.createdate).format('YYYY-MM-DD') == selecteddate))
            }
        }
    } 
    async function _getExpenseList() {
        const options = {
            headers: {
                'Content-type': 'application/json',
                "Authorization": 'Bearer ' + localStorage.getItem('token')
            }
        };
        let pid = localStorage.getItem('pid')
        const res = await axios.get(process.env.REACT_APP_DURL + 'expenselist/' + pid,options)
        if(res.status == 200){
            if(tid !== ''){
                setExpenseList(res?.data.filter(i => i.taskid == tid && moment(i.createdate).format('YYYY-MM-DD') == selecteddate))
            }
        }
    }

    useEffect(() => {
        _getIssuelist()
        _getExpenseList()
        let tid = localStorage.getItem('tid')
        settid(tid)
    },[SelectedProject,selecteddate])

    async function _gettaskdetails() {
        try {
            let tid = localStorage.getItem('tid')
            const options = {
                headers: {
                    'Content-type': 'application/json',
                    "Authorization": 'Bearer ' + localStorage.getItem('token')
                }
            };
            const res = await axios.get(process.env.REACT_APP_DURL + 'gettaskdetails/' + tid, options)
            if(res.status == 200){
                setTaskDetails(res.data)
            }
        }
        catch (err) {
            console.log(err)
        }
    }

    async function _gettaskmedia(){
        const options = { 
            headers: {
                'Content-type': 'application/json',
                "Authorization": 'Bearer ' + localStorage.getItem('token')
            }
        };
        let res = await axios.get(process.env.REACT_APP_DURL + 'allmedia/' + SelectedProject,options)
       if(res.status == 200){
           if(tid !== ''){
             setMediaArr(res.data[0].task.filter(i => i.taskID == tid))
           }
        }
    }
    async function _getWorkDoneList() {
        try {
            let tid = localStorage.getItem('tid')
            const options = {
                headers: {
                    'Content-type': 'application/json',
                    "Authorization": 'Bearer ' + localStorage.getItem('token')
                }
            };
            const res = await axios.get(process.env.REACT_APP_DURL + 'workdonelist/' + tid,options)
            // setTaskDetails(res?.data)
            console.log(res.data?.[0]);
            setlabour(res?.data?.[ 0 ]?.labour.filter(i => moment(i.Date,'DD-MM-YYYY').format('YYYY-MM-DD') == selecteddate))
            setComments(res?.data?.[0]?.Comment[0].comment)
            setmachine(res?.data?.[ 0 ]?.machine.filter(i => moment(i.Date,'DD-MM-YYYY').format('YYYY-MM-DD') == selecteddate))
            setmaterial(res?.data?.[ 0 ]?.material.filter(i => moment(i.Date,'DD-MM-YYYY').format('YYYY-MM-DD') == selecteddate))
            settaskprogres(res?.data?.[ 0 ]?.TaskProgress)
            setunit(res?.data?.[ 0 ]?.unit)
            
            res?.data?.[ 0 ]?.PlannedCost.filter(ele=>{
                if(moment(selecteddate).format('DD-MM-YYYY') == ele?.Date){
                    setPlannedCost(ele)
                }
            })
            res?.data[0].ActualCost.filter(ele=>{
                if((moment(selecteddate).format('DD-MM-YYYY') == ele?.Date)){
                    setActualCost(ele)
                }
            })
            setdeductions(res?.data?.[ 0 ]?.deduction)
        }
        catch (err) {
            console.log(err)
        }
    }

    useEffect(() => {
        _gettaskdetails()
        _getWorkDoneList()
        _gettaskmedia()
    }, [SelectedProject,selecteddate])

    // useEffect(()=>{
    //     _getWorkDoneList()
    // },[selecteddate, comments])

    let refreshHandler = () => {
        _getWorkDoneList();
        _getExpenseList();
        _getIssuelist();
        _gettaskmedia();
        _gettaskdetails();
    }

    let addCommentHandler = async () => {
        setComment('')
        let data = {
            taskId: localStorage.getItem('tid'),
            date: selecteddate,
            labour: [],
            machine: [],
            material: [],
            Media: [],
            commentText: comment,
            TaskProgress: [],
            deduction: []
        }

        const options = {
            headers: {
                'Content-type': 'application/json',
                "Authorization": 'Bearer ' + localStorage.getItem('token')
            }
        };

        let res = await axios.put(process.env.REACT_APP_DURL + 'updateworkdone', data, options)
        if(res.status == 200){
            
            const data = await _getWorkDoneList();
          }
     }

//      let changeHandler = (start, end) => {
//         if(start && !end) {
//             setSelectedDate([start])
//        } else if(start && end) {
//            setSelectedDate(getDates(start,end))
//        }
//    }

let updateHandler = async (e) => {
    e.preventDefault();
    let filteredMan = labour.filter(i => i.Date === moment(selecteddate).format('DD-MM-YYYY'));
    let filteredMachine = machine.filter(i => i.Date === moment(selecteddate).format('DD-MM-YYYY'));
    let filteredMaterial = material.filter(i => i.Date === moment(selecteddate).format('DD-MM-YYYY'));

    let data = {
        taskId: tid,
        date: moment(selecteddate).format('DD-MM-YYYY'),
        labour: filteredMan,
        machine: filteredMachine,
        material: filteredMaterial,
        Media: [],
        commentText: comment,
        TaskProgress: [],
        deduction: []
    }
    const options = {
        headers: {
            "Content-type": 'application/json',
            "Authorization": 'Bearer ' + localStorage.getItem('token')
        }
    };
     let res = await axios.put(process.env.REACT_APP_DURL +  "updateworkdone",data,options)
   
}

    const getNotification = async () => {
        await window.Notification.requestPermission();
        await new Notification('hello')
    }

    const check = () => {
        if (!('serviceWorker' in navigator)) {
          throw new Error('No Service Worker support!')
        } else if (!('PushManager' in window)) {
          throw new Error('No Push API Support!')
        } else {
            console.log('all good');
        }
      }

      const showLocalNotification = (title, body, swRegistration) => {
        const options = {
            body,
        };
        swRegistration.showNotification(title, options);
    }

    return <div className={styles.container}>
         <div className={styles.header}>
         <h2>{TaskDetails.length > 0 && TaskDetails[0]?.name}</h2>
            <div>
                <div>
                    <p>{TaskDetails.length > 0 && moment(TaskDetails[0].startDate).format('DD/MM/YYYY')}</p>
                    <p>-</p>
                    <p>{TaskDetails.length > 0 && moment(TaskDetails[0].endDate).format('DD/MM/YYYY')}</p>
                </div>
                <input onChangeCapture={(e) => {setselecteddate(e.target.value)}}
                type='date' value={selecteddate}/>
            </div>
            {/* <div className={styles.calendar}>
                <CustomCalander onChange={changeHandler}/>
                <h5>{selectedDate ? selectedDate : 'Select Date'}</h5>
            </div> */}
          </div>

          <div className={styles.headerCards}>
              <Card className={styles.plannedCost}>
                  <h1 style={{backgroundColor: PlannedCost?.TotalCost >= ActualCost?.TotalCost ? '#2AC371' : '#D53539'}}><span className="rupee">₹</span></h1>
                  <h2>{ActualCost?.TotalCost}</h2>
                    <h4 style={{color:`${PlannedCost?.TotalCost >= ActualCost?.TotalCost ? '#2AC371' : '#D53539'}`}}>
                        {isNaN(Math.abs(PlannedCost?.TotalCost - ActualCost?.TotalCost)) ? '0' : unit == 'km' ?
                            Math.abs(PlannedCost?.TotalCost - ActualCost?.TotalCost) / 1000 : Math.abs(PlannedCost?.TotalCost - ActualCost?.TotalCost)
                        }</h4>
                    <h5>Planned Cost - { unit == 'km' ? parseInt(PlannedCost?.TotalCost) / 1000 : PlannedCost?.TotalCost } </h5>
                  
              </Card>
              <Card className={styles.headerCard} border="2px solid #0A60BD" shadow='none'>
                <div style={{display:'flex',alignItems:'center'}}>
                <div className={styles.headerIcon}><img src={manicon} /></div>
                <h3>Man</h3>
                </div>
                <h4><span style={{fontWeight:'400',fontSize:'1.1rem'}}>Planned: </span><span className="rupee">₹</span>
                {labour.reduce((acc,obj)=>{
                   return (acc + (+obj.Planned));
                },0)}
                </h4>
                <h4><span style={{fontWeight:'400',fontSize:'1.1rem'}}>Actual: </span><span className="rupee">₹</span>
                {labour.reduce((acc,obj)=>{
                   return (acc + (+obj.actual));
                },0)}
                </h4>
              </Card>
              <Card className={styles.headerCard} border="2px solid #0A60BD" shadow='none'>
                  <div style={{display:'flex',alignItems:'center'}}>
                <div className={styles.headerIcon}><img src={machineicon} /></div>
                <h3>Machine</h3>
                </div>
                <h4><span style={{fontWeight:'400',fontSize:'1.1rem'}}>Planned: </span><span className="rupee">₹</span>
                {machine.reduce((acc,obj)=>{
                   return (acc + (+obj.Planned));
                },0)}
                </h4>
                <h4><span style={{fontWeight:'400',fontSize:'1.1rem'}}>Actual: </span><span className="rupee">₹</span>
                {machine.reduce((acc,obj)=>{
                   return (acc + (+obj.actual));
                },0)}
                </h4>
              </Card>
              <Card className={styles.headerCard} border="2px solid #0A60BD" shadow='none'>
                  <div style={{display:'flex',alignItems:'center'}}>
                <div className={styles.headerIcon}><img src={materialicon} /></div>
                <h3>Material</h3>
                </div>
                <h4><span style={{fontWeight:'400',fontSize:'1.1rem'}}>Planned: </span><span className="rupee">₹</span>
                {material.reduce((acc,obj)=>{
                   return (acc + (+obj.Planned));
                },0)}
                </h4>
                <h4><span style={{fontWeight:'400',fontSize:'1.1rem'}}>Actual
                : </span><span className="rupee">₹</span>
                {material.reduce((acc,obj)=>{
                   return (acc + (+obj.actual));
                },0)}
                </h4>
              </Card>
          </div>

          <Card className={styles.taskProgress}>
           <h2 >Work Progress - {`${unit == 'km' ? parseInt(totalQTY) / 1000 : totalQTY}`} ({unit})</h2>
           <div className={styles.progress}>
            <div style={{width:`${unit == 'km' ? parseInt(totalQTY) / 1000 : totalQTY/TaskDetails[0]?.quantity*100}` <= 100 ? `${unit == 'km' ? parseInt(totalQTY) / 1000 : totalQTY/TaskDetails[0]?.quantity*100}%` : '0%'}} className={styles.progressInner}>
                <div className={styles.percentage}>{(`${unit == 'km' ? parseInt(totalQTY) / 1000 : totalQTY}`/TaskDetails[0]?.quantity)*100 <= 100 ? (`${unit == 'km' ? parseInt(totalQTY) / 1000 : totalQTY}`/TaskDetails[0]?.quantity)*100 : 0}%</div>
            </div>
           </div>
         </Card>

         <div className={styles.cards}>
             <Card>
                <h3>Task Expenses</h3>
                {ExpenseList.length == 0 ? <p style={{color:'#848EA0'}}>No expenses on this date!</p> : ExpenseList.map(expense => {
                    return <div className={styles.expense}>
                    <div><p><span className="rupee">₹</span>{expense.Amount}</p><p>by {expense.creatorname}</p></div>
                    <div className={styles[expense.status]}></div>
                    </div>
                })}
            </Card>
            <Card>
            <h3>Task Issues</h3>
             {issueList.length == 0 ? <p style={{color:'#848EA0'}}>No issues on this date!</p> : issueList.map(issue => {
                return <div className={styles.issue}>
                <div><p>{issue.issue}</p><p>by {issue.creatorname}</p></div>
                <div className={styles[issue.status]}></div>
                </div>
             })}
            </Card>
            <Card>
                <h3>Task Media</h3>
                <div className={styles.media}>
                    {MediaArr.map(i => i.Media.length) == 0 ? <p style={{color:'#848EA0'}}>No media on this date!</p> : MediaArr.map(media => {
                        media.Media.map(each => {
                            if(each.mediaType === 'png' || each.mediaType === 'jpg' || each.mediaType === 'jpeg') return <div>
                              <img onClick={()=>{
                                setDisplayImage(each.MediaUrl)
                                setImageModal(true)
                              }} src={each.MediaUrl} />
                         </div>
                        })
                    })}
                </div>
            </Card>
         </div>

                {/* <CustomContainer className={`${styles.card} ${styles.comments}`}>
                    <h3>Comments</h3>
                      {comments.map(comment => {
                          return <div className={styles.allComments}>
                          <h4>{comment.commentText}</h4>
                          <h5>by {comment.commentby}</h5>
                      </div>  
                      })}
                     <textarea value={comment} onChange={(e)=>{
                         setComment(e.target.value)
                         setMessage(e.target.value)
                         }} rows='7' placeholder='Add comment'></textarea>
                   <button className={styles.updateBtn} onClick={(e)=>{
                       addCommentHandler()
                      }
                       }>Request Updates</button>
               </CustomContainer> */}
            

            {imageModal && <div className={styles.imageModal}><img className={styles.modalImage} src={displayImage}/>
             <div onClick={()=>setImageModal(false)} className={styles.closeImageModal}><i className="far fa-times-circle fa-2x"></i></div></div>
            }

            {chat.length !== 0 && <div className={styles.notifications}>
            {chat.map(i => {
                return <p>{i.username} added a new comment - {i.message}</p>
            })}
             </div>}
        </div >
}

export default Task;