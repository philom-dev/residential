import styles from "./Ratelist.module.css"
import { CustomContainer } from "./CustomContainer.style";
import {Card} from "./Card.style"
import {Button} from "./Button.style"
import Dropdown from 'react-dropdown';
import 'react-dropdown/style.css';
import { useEffect, useState } from "react";
import deleteicon from "../resources/icons/deleteicon.svg"
import updateicon from "../resources/icons/updateicon.svg"
import plusicon from "../resources/icons/plusicon.svg"
import axios from "axios";
import moment from "moment";
import loaderlogo from "../logo/loader.gif";
import { analytics } from "./Analytics"
import { logEvent } from "@firebase/analytics";
import { Oval } from  'react-loader-spinner'

function Ratelist(){
    const [type, setType] = useState('Select type...')
    const [typeOf, setTypeOf] = useState('')
    const [name, setName] = useState('')
    const [unit, setUnit] = useState('Select unit...')
    const [rate, setRate] = useState('')
    const [idle, setIdle] = useState('')
    const [rateList, setRateList] = useState([])
    const [update, setUpdate] = useState(false)
    const [filter, setFilter] = useState('All')
    const [list, setList] = useState([])
    const [number, setNumber] = useState('')
    const [editRate, setEditRate] = useState(false)
    const [message, setMessage] = useState(false)
    const [loader, setLoader] = useState(true)
    const [isDisabled, setIsDisabled] = useState(true)

    let getAllRates = async () => {
        let man = []
        let machine = []
        let material = []
        const options = {
            headers: {
                'Content-type': 'application/json',
                "Authorization": 'Bearer ' + localStorage.getItem('token')
            }
        };
        let res = await axios.get(process.env.REACT_APP_DURL + 'getAllRates/' + localStorage.getItem('pid'),options)
        if(res.status == 200){
            setLoader(false)
            setIsDisabled(false)
            setRateList([...res.data.man, ...res.data.machine ,...res.data.material]);
         }
    }

    let addHandler = () => {
        if(unit == '' || name == '' || rate == ''){
            console.log("cannot be empty");
        } else {
            let data = {
                type,
                typeOf,
                name,
                unit,
                rate,
                idleHours: idle ? idle : '',
                date: moment().format('DD-MM-YYYY hh:mm:ss A'),
                _id: Date.now() + Math.round((Math.random() * 36 ** 12)).toString(36),
                number: type == 'Machine' || type == "Material" ? number : '',
            }
            setRateList([data,...rateList])
            setType('')
            setUnit('')
            setName('')
            setRate('')
            setIdle('')
            setTypeOf('')
            setNumber('')
            
        }
        
    }

    let updateHander = async () => {
        setLoader(true)
        const options = {
            headers: {
                'Content-type': 'application/json',
                "Authorization": 'Bearer ' + localStorage.getItem('token')
            }
        };
        let data = {
            projectId: localStorage.getItem('pid'),
            rateList: rateList
        }
        let res = await axios.post(process.env.REACT_APP_DURL + 'addRate' , data,options)
        if(res.status == 200) {
            setEditRate(false)
            setLoader(false)
            setMessage(true)
            setTimeout(() => {
                setMessage(false)
            }, 3000);
        }
    }

    useEffect(()=>{
        getAllRates()
    },[localStorage.getItem('pid')])

    return loader ? <Oval wrapperClass="pageLoading" strokeWidth={4} secondaryColor='#EAEDF0' color="#0A60BD" height={50} width={50} /> :
    
         <div className={styles.ratelist}>
          <div className={styles.header}>
            <Card>
                <div style={{width:'30%'}}><Dropdown arrowClassName={styles.dropdownArrow} className={styles.dropdownContainer} menuClassName={styles.dropdownMenu} controlClassName={styles.dropdown} onChange={(e)=>setType(e.value)} options={['Man',"Machine","Material"]} value={type} /></div>
                <div className={styles.headerForm}>
                <div className={styles.formGroup}>
                    <label>
                        Name
                    </label>
                    <input value={name} onChange={(e)=>setName(e.target.value)} type='text' />
                </div>

                <div className={styles.formGroup}>
                    <label>
                        Unit
                    </label>
                    <Dropdown controlClassName={styles.unitDropdown} menuClassName={styles.dropdownMenu} 
                     value={unit} options={type == "Machine" ? [ "Number", "Hours", "Km run" ] : type == 'Man' ? [ "Number", "Hours" ,"cum","sqm","km","rmt","meter","MT"] : type == "Material" ? [
                        "Bags", "Number", "Meter", "Cubic feet", "KG", "Litre",
                        "Sq Feet", "Brass", "Cubic meter", "Tonne", "Quintal",
                        "Box", "Milimeter", "Centimeter", "Feet", "Inch", "Sq meter",
                        "Gram", "Lb", "Kilolitre"
                    ] : ['Select a type first...']} onChange={(e)=>setUnit(e.value)}/>
                </div>

                <div className={styles.formGroup}>
                    <label>
                        Rate
                    </label>
                    <input value={rate} onChange={(e)=>setRate(e.target.value)} type='text' />
                </div>

                {type == 'Machine' && <div className={styles.formGroup}>
                    <label>
                        Ideal Hours/Km run (Per day)
                    </label>
                    <input value={idle} onChange={(e)=>setIdle(e.target.value)} type='text' />
                </div>}

               {/* {type == 'Select type...' || type == 'Man' ? <div className={styles.formGroup}>
                    <label>
                        Number
                    </label>
                    <input disabled  value={number} onChange={(e)=>setNumber(e.target.value)} type='text' />
                </div> : <div className={styles.formGroup}>
                    <label>
                        {type} Number
                    </label>
                    <input  value={number} onChange={(e)=>setNumber(e.target.value)} type='text' />
                </div> } */}

                </div>
                <Button disabled={isDisabled} onClick={addHandler} width='200px' color="#0A60BD">Add</Button>
            </Card>
            {/* <button onClick={updateHander}>Update List</button> */}
        </div>

        <div className={styles.table}>
            <div className={styles.tableHeader}>
                <p>S.No.</p>
                <p style={{display:'flex',alignItems:'center'}}>Resource <div style={{width:'100%'}}>
                    <Dropdown onChange={(e)=>setFilter(e.value)} className={styles.dropdownContainer} menuClassName={styles.dropdownMenu} controlClassName={styles.filterDropdown} options={["All",'Man','Machine','Material']} value="All"/>
                </div></p>
                <p>Name</p>
                <p>Unit</p>
                <p>Ideal Hours</p>
                <p>
                 <div style={{display:'flex', alignItems:'center',}}>
                     <p style={{marginRight:10}}>Rate</p>
                     <Button onClick={()=>setEditRate(!editRate)} color="#22242E" bordered>Edit</Button>
                     </div>
                </p>
            </div>
            <div className={styles.tableBody}>
            {rateList.map((rate,index) => {
                   if(rate.type == filter && filter !== "All"){
                    return <div className={styles.tableRow}>
                   <p>{index+1}</p>
                   <p>{rate.type}</p>
                   <p>{rate.name}</p>
                   <p>{rate.unit}</p>
                   <p>{rate.idleHours}</p>
                   <p>{editRate ? <input type='number' onChange={(e)=>{
                       (rateList.filter(i => i._id == rate._id)[0].rate = e.target.value);
                   }} defaultValue={rate.rate} /> : rate.rate}</p>
                </div>
                 } else if (filter == "All"){
                    return <div className={styles.tableRow}>
                    <p>{index+1}</p>
                    <p>{rate.type}</p>
                    <p>{rate.name}</p>
                    <p>{rate.unit}</p>
                    <p>{rate.idleHours}</p>
                    <p >{editRate ? <input type='number' onChange={(e)=>{
                        (rateList.filter(i => i._id == rate._id)[0].rate = e.target.value);
                    }} defaultValue={rate.rate} /> : rate.rate}</p>
                </div>}})}
            </div>

        </div>
        <Button onClick={updateHander} disabled={isDisabled && rateList.length !== 0} color="#0A60BD">Save Changes</Button>
        {loader ? <img className={styles.loader} src={loaderlogo}/> : message ? <div className={styles.successMessage}>List Updated Successfully!</div> : ''}

    </div>

}

export default Ratelist