import styles from "./CreateProject.module.css";
import {useContext, useState} from "react";
import axios from "axios";
import AllprojectsContext from "../context/AllprojectsContext";
import logo from "../logo/loader.gif";


function CreateProject(){
const [projectName, setProjectName] = useState('');
const [number, setNumber] = useState();
const [numbers, setNumbers] = useState([]);
const {AllProjects, setAllProjects} = useContext(AllprojectsContext)
const [success, setSuccess] = useState(false)
const [loader, setLoader] = useState(false)

let nameHandler = (e) => {
    setProjectName(e.target.value)
}

let numberHandler = (e) => {
    setNumber(e.target.value)
}

let addNumberHandler = (e) => {
    e.preventDefault();
    setNumbers([...numbers, number])
    setNumber('')
}

let submitHandler = async (e) => {
    e.preventDefault();
    setLoader(true)
    setSuccess(false)
    let data = {
        projectname: projectName,
        numbers: numbers
    }
    const options = {
        headers: {
            'Content-type': 'application/json',
            "Authorization": 'Bearer ' + localStorage.getItem('token')
        }
    };
   const res = await axios.post(process.env.REACT_APP_DURL + 'createproject',data,options);
   console.log("🚀 ~ file: CreateProject.js ~ line 40 ~ submitHandler ~ res", res)
    if(res.status == 200){
        _updateAllProjects()
        setSuccess(true)
        setLoader(false)
        setTimeout(()=>{
            setSuccess(false)
        },3000)
        setNumbers([])
        setProjectName('')
        setNumber('');
      }
    }

async function _updateAllProjects(){
    const options = {
        headers: {
            'Content-type': 'application/json',
            "Authorization": 'Bearer ' + localStorage.getItem('token')
        }
    };
    let res = await axios.get(process.env.REACT_APP_DURL + 'getUserProject/' + localStorage.getItem('userid'), options)
    console.log("🚀 ~ file: CreateProject.js ~ line 53 ~ _updateAllProjects ~ res", res)
    if(res.status == 200){
        setAllProjects(res.data.ProjectId);
    }
}

return <div className={styles.main}>
    <div className={styles.container}>
        <form onSubmit={submitHandler}>
        <input value={projectName} onChange={nameHandler} className={styles.formInput} type='text' autoComplete='off' placeholder='Project Name'/>
        <div className={styles.formGroup}>
        <div className={styles.addIcon}><i style={{color:'#808495'}} className="fas fa-user-plus fa-2x"></i></div>
        <input value={number} onChange={numberHandler} style={{paddingLeft:'60px'}} className={styles.formInput} minLength='10' maxLength='10' type='tel' autoComplete='off' placeholder='Phone Number'/>
        <div id='addedNumbers' style={{display:'flex',flexWrap:'wrap'}}>
        {numbers.map(number => {
            return <div className={styles.addedNumber}>{number}</div> 
           })}
              
        </div>
        <button onClick={addNumberHandler} className={styles.addNumber}><i className="fas fa-plus"></i> Add</button>
        </div>
        <button type='submit' className={styles.createBtn}>Create Project</button>
        </form>
        {success ? <div className={styles.success}>Created successfully.</div> : loader ? <img className={styles.loader} src = {logo}/> : ''}
    </div>
</div>
}

export default CreateProject;