import { OrbitControls, Polyhedron, RoundedBox , Text, Html} from "@react-three/drei"
import { Canvas, useLoader } from "@react-three/fiber"
import React, { Suspense, useEffect, useState } from "react";
import { CubicBezierCurve, PolyhedronBufferGeometry } from "three";
import { TextureLoader } from "three/src/loaders/TextureLoader";
import { GLTFLoader } from 'three/examples/jsm/loaders/GLTFLoader'
import { OBJLoader } from 'three/examples/jsm/loaders/OBJLoader'
import styles from "./Modelling.module.css"
import axios from "axios";
import moment from "moment"



function BoxA() {
    return <mesh position={[-8,0,0]}>
        <boxBufferGeometry args={[1,2,5]} />
        <meshLambertMaterial color='#f5d273' />
    </mesh>
}

function BoxB() {
    return <mesh position={[8,0,0]}>
        <boxBufferGeometry args={[1,2,5]} />
        <meshLambertMaterial color='#f5d273' />
    </mesh>
}

function BoxxA(){
    return <mesh position={[-8,1,0]}>
    <boxBufferGeometry args={[1,0.1,5]} />
    <meshLambertMaterial color='#ababab' />
</mesh>
}

function BoxxB(){
    return <mesh position={[8,1,0]}>
    <boxBufferGeometry args={[1,0.1,5]} />
    <meshLambertMaterial color='#ababab' />
</mesh>
}

function BoxxxA(){
    return <mesh position={[8,1.8,0]}>
    <boxBufferGeometry args={[1,1.5,5]} />
    <meshLambertMaterial color='#f5d273' />
</mesh>
}
function BoxxxB(){
    return <mesh position={[-8,1.8,0]}>
    <boxBufferGeometry args={[1,1.5,5]} />
    <meshLambertMaterial color='#f5d273' />
</mesh>
}



function Culvert() {
    return <React.Fragment>
        <mesh position={[-7,0,0]}>
            <boxBufferGeometry args={[1,3,5]} />
            <meshLambertMaterial color='green' />
       </mesh>
       <mesh position={[-6,1,0]}>
            <boxBufferGeometry args={[3,1,5]} />
             <meshLambertMaterial color='green' />
      </mesh>
      <mesh position={[-4,0,0]}>
            <boxBufferGeometry args={[1,3,5]} />
            <meshLambertMaterial color='green' />
     </mesh>
    </React.Fragment>
}

function elevationLeft(i,j) {
   return <mesh position={[i-14,j,0]}>
                <boxBufferGeometry args={[0.1,0.5,6]}  />
                <meshLambertMaterial color='#383838' />
         </mesh>
}

function elevationRight(i,j) {
    return <mesh position={[i+8,-j+2.40,0]}>
                 <boxBufferGeometry args={[0.1,0.5,6]}  />
                 <meshLambertMaterial color='#383838' />
          </mesh>
 }

function SlopeLeft(props) {
    const texture = useLoader(TextureLoader, 'https://images.template.net/wp-content/uploads/2017/01/04005413/Tileable-Road-Texture.jpg')
    const vertices = [
        -1,-1,-1,    1,-1,-1,    1, 0.2,-1,    -1, -1,-1,
        -1,-1, 1,    1,-1, 1,    1, 0.2, 1,    -1, -1, 1,
    ];
    const indices = [
        2,1,0,    0,3,2,
        0,4,7,    7,3,0,
        0,1,5,    5,4,0,
        1,2,6,    6,5,1,
        2,3,7,    7,6,2,
        4,5,6,    6,7,4
    ];
    return <mesh position={[props.x,2.7,0]}>
            <polyhedronBufferGeometry args={[vertices,indices,6]} />
            <meshLambertMaterial color="#808387" />
        </mesh>
}

function SlopeRight(props) {
    const texture = useLoader(TextureLoader, 'https://t4.ftcdn.net/jpg/02/99/37/89/360_F_299378925_uGqvCMwbuOI5nLhhHCRdX6b6sRYKdxIO.jpg')
    const vertices = [
        -1,-1,-1,    1,-1,-1,    1, -1,-1,    -1, 0.2,-1,
        -1,-1, 1,    1,-1, 1,    1, -1, 1,    -1, 0.2, 1,
    ];
    const indices = [
        2,1,0,    0,3,2,
        0,4,7,    7,3,0,
        0,1,5,    5,4,0,
        1,2,6,    6,5,1,
        2,3,7,    7,6,2,
        4,5,6,    6,7,4
    ];
    return <mesh position={[props.x,2.7,0]}>
            <polyhedronBufferGeometry args={[vertices,indices,6]} />
            <meshLambertMaterial color="#808387" />
        </mesh>
}

function Base(props) {
    const [hovered0, setHovered0] = useState(false)
    const hover0 = () => setHovered0(true);
    const unhover0 = () => setHovered0(false)
    const colorMap = useLoader(TextureLoader, 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR4y7nT3YvKi1QlAZ4rja9hWan7RuKhtzgkog&usqp=CAU')
    return <mesh scale={hovered0 ? 1.2 : 1} onPointerOver={hover0} onPointerOut={unhover0} position={[0,3.3,0]}>
        <boxBufferGeometry args={[props.length,0.5,8]} />
        <meshLambertMaterial map={colorMap} />
        {hovered0 && <Html scale={2}>
                 <div className={styles.card}>
                     <p>Material - B40</p>
                     <p>Length - {props.length}km</p>
                     {/* <p>Cahinage From 400+1000</p>
                     <p>Planned Cost - ₹10,52,999</p>
                     <p>Actual Cost - ₹11,45,622</p> */}
                 </div>
             </Html>}
    </mesh>
}



function Pillar(props) {
    const [hovered0, setHovered0] = useState(false)
    const hover0 = () => setHovered0(true);
    const unhover0 = () => setHovered0(false)
    const [hovered1, setHovered1] = useState(false)
    const hover1 = () => setHovered1(true);
    const unhover1 = () => setHovered1(false)
    
    const texture = useLoader(TextureLoader, 'https://media.istockphoto.com/photos/asphalt-texture-picture-id464570512?b=1&k=20&m=464570512&s=170667a&w=0&h=3ctoVSrWSvs3KM0fZ5bQIhUMv3Dv7kWpCXZYegiXC0E=')
    const vertices1 = [
        -1,-1,-1,    1,-1,-1,    1, 1,-1,    -1, 1,-1,
        -1,-1, -1,    1,-1, -1,    1, 1, 1,    -1, 1, 1,
    ];

    const vertices2 = [
        -1,-1,1,    1,-1,1,    1, 1,-1,    -1, 1,-1,
        -1,-1, 1,    1,-1, 1,    1, 1, 1,    -1, 1, 1,
    ];
    
     const indices = [
            2,1,0,    0,3,2,
            0,4,7,    7,3,0,
            0,1,5,    5,4,0,
            1,2,6,    6,5,1,
            2,3,7,    7,6,2,
            4,5,6,    6,7,4
        ];
    return <React.Fragment>
         <mesh scale={hovered0 ? 1.3 : 1} onPointerOver={hover0} onPointerOut={unhover0} position={[props.x, 0, 1.2]}>
             <cylinderBufferGeometry attach="geometry" args={[0.7, 0.7, 5,30]} />
             <meshStandardMaterial map={texture}  />
             {hovered0 && <Html scale={2}>
                 <div className={styles.card}>
                     <p>Material - M40</p>
                     <p>Cahinage From 22+200</p>
                     <p>Planned Cost - ₹52,999</p>
                     <p>Actual Cost - ₹45,622</p>
                 </div>
             </Html>}
            </mesh>

            <mesh scale={hovered1 ? 1.3 : 1} onPointerOver={hover1} onPointerOut={unhover1} position={[props.x, 0, -1.2]}>
             <cylinderBufferGeometry attach="geometry" args={[0.7, 0.7, 5,30]} />
             <meshStandardMaterial map={texture}/>
             {hovered1 && <Html scale={2}>
                 <div className={styles.card}>
                     <p>Material - M43</p>
                     <p>Cahinage From 22+200</p>
                     <p>Planned Cost - ₹52,999</p>
                     <p>Actual Cost - ₹45,622</p>
                 </div>
             </Html>}
            </mesh> 

            <mesh position={[props.x,2,2.8]}>
            <polyhedronBufferGeometry args={[vertices1,indices,2]} />
            <meshStandardMaterial map={texture} /> 
            </mesh>
            <mesh position={[props.x,2,-2.8]}>
            <polyhedronBufferGeometry args={[vertices2,indices,2]} />
            <meshStandardMaterial map={texture} /> 
            </mesh>

            <mesh position={[props.x,2,0]}>
            <boxBufferGeometry args={[2.16,2.16,3.5]} />
            <meshStandardMaterial map={texture} /> 
            </mesh>
    </React.Fragment>
}


 function Dice() {
    let sideone = useLoader(TextureLoader, 'textures/dice_1.jpeg')
    let sidetwo = useLoader(TextureLoader, 'textures/dice_2.jpeg')
    let sidethree = useLoader(TextureLoader, 'textures/dice_3.jpeg')
    let sidefour = useLoader(TextureLoader, 'textures/dice_4.jpeg')
    let sidefive = useLoader(TextureLoader, 'textures/dice_5.jpeg')
    let sidesix = useLoader(TextureLoader, 'textures/dice_6.jpeg')

    return <mesh>
    <boxBufferGeometry args={[1,1,1]} />
    <meshStandardMaterial map={sideone} attachArray='material'  />
    <meshStandardMaterial map={sidetwo} attachArray='material'  />
    <meshStandardMaterial map={sidethree}  attachArray='material' />
    <meshStandardMaterial map={sidefour} attachArray='material'  />
    <meshStandardMaterial map={sidefive} attachArray='material'  />
    <meshStandardMaterial map={sidesix}  attachArray='material' />
</mesh>
 }

 function Wall(props) {
     return <mesh position={[props.x || 0,props.y || 0,props.z || 0]}>
         <boxBufferGeometry args={[props.length,4,props.breadth]} />
         <meshLambertMaterial color='grey' />
     </mesh>
 }

function GltfCube() {
    const model = useLoader(OBJLoader,'/cube.obj')
    return <Suspense fallback={null}>
     <primitive object={model} />
      </Suspense>
}

function Modelling() {
    return <Canvas>
        <Suspense fallback={null}>
         <OrbitControls/>
             <ambientLight position={[-10,-10,-8]} intensity={0.4} />
             <spotLight position={[10,-2,-8]} />
             <pointLight color="white" intensity={1} position={[2, 10, 40]} />
             <GltfCube />
        </Suspense>
    </Canvas>
}


// function Modelling() {

//     const [length, setLength] = useState(30)
//     const [showPillar,setShowPillar] = useState(false)
//     const [rightElevation, setRightElevation] =useState(false)
//     const [leftElevation, setLeftElevation] =useState(false)

//     const [slopeLeftPosition, setSlopeLeftPosition] = useState()
//     const [slopeRightPosition, setSlopeRightPosition] = useState()
//     const [pillarPoint, setPillarPoint] = useState()
//     let [pillars, setPillars] = useState([])

//     let _getAllTasksWorkDone = async () => {
//         const options = {
//             headers: {
//                 'Content-type': 'application/json',
//                 "Authorization": 'Bearer ' + localStorage.getItem('token')
//             }
//         };
//         let highwayTasks = []
//         let pillarTasks = []
//         let res = await axios.get(process.env.REACT_APP_DURL + 'getAllTasksWorkDone/'+ localStorage.getItem('pid'), options)
//         if(res.status == 200){
//             res.data.map(i => {
//                 if(i.type == 'Highway'){
//                     highwayTasks.push(...i.taskProgress)
//                 } else if (i.type == 'Structure'){
//                     pillarTasks.push(...i.taskProgress)
//                 }
//             })
//         }
//       let minFrom = Math.min.apply(Math, highwayTasks.map(function(o) { return o.From; }));
//       let maxTo = Math.max.apply(Math, highwayTasks.map(function(o) { return o.To; }));
//     //   setLength((maxTo - minFrom))
//       setSlopeLeftPosition(-length/2 - 4)
//       setSlopeRightPosition(length/2 + 4)
//     //   console.log(pillarTasks);

//     pillarTasks.map(i => {
//         setPillars((n)=>[...n,<Pillar x={i.From - (length/2)}/>])
//       })
//     }

//     useEffect(()=>{
//         _getAllTasksWorkDone()
//     },[localStorage.getItem('pid')])

//     return <div>
//          {/* <div>
//         <button onClick={()=>{
            
//             setLength(length+1)
//             setSlopeLeftPosition(slopeLeftPosition-0.5)
//             setSlopeRightPosition(slopeRightPosition+0.5)
            
//         }} className={styles.addBtn}>Increase length</button>
//         <button onClick={()=>{
//             if(length > 1){
//                 setLength(length-1)
//                 setSlopeLeftPosition(slopeLeftPosition+0.5)
//                 setSlopeRightPosition(slopeRightPosition-0.5)
//             }
//         }} className={styles.addBtn} >Decrease length</button>
//         <br></br>
//         <button onClick={()=>setRightElevation(true)} className={styles.addBtn}>Set elevation on right side</button>
//         <button onClick={()=>setLeftElevation(true)} className={styles.addBtn}>Set elevation on left side</button>

//         <form onSubmit={(e)=>{
//             e.preventDefault()
//             if(pillarPoint < (slopeRightPosition-4) && pillarPoint > (slopeLeftPosition+4)){
//                 setPillars((n)=>[...n,<Pillar x={pillarPoint}/>])
//             } else if(pillarPoint > (slopeRightPosition-4)){
//                 console.log('greater from right');
//                 setPillars((n)=>[...n,<Pillar x={pillarPoint}/>])
//             }
//         }}>
//         <input type='number' onChange={(e)=>setPillarPoint(e.target.value)} />
//         <input type='submit' />
//         </form>

//         <button className={styles.addBtn} onClick={()=>_getAllTasksWorkDone()}>refresh</button>
        
//         </div> */}

//         {/* <Canvas>
//             <Suspense fallback={null}>
//             <OrbitControls/>
//             <ambientLight position={[-10,-10,-8]} intensity={0.4} />
//             <spotLight position={[10,-2,-8]} />
//             <pointLight color="white" intensity={1} position={[2, 10, 40]} />
//             <GltfCube />
//             </Suspense>
//         </Canvas> */}

//         <Canvas>
//         <Suspense fallback={null}>
//             <OrbitControls/>
//             <ambientLight position={[-10,-10,-8]} intensity={0.4} />
//             <spotLight position={[10,-2,-8]} />
//             <pointLight color="white" intensity={1} position={[2, 10, 40]} />

//             <GltfCube />

//             {/* <BoxA />
//             <BoxB />
//             <BoxxA />
//             <BoxxB />
//             <BoxxxA />
//             <BoxxxB /> */}
            
//             {/* <mesh position={[-9, 0, 1.2]}>
//              <cylinderBufferGeometry attach="geometry" args={[0.7, 0.7, 5,30]} />
//              <meshLambertMaterial color="grey" />
//             </mesh>
//             <mesh position={[-9, 0, -1.2]}>
//              <cylinderBufferGeometry attach="geometry" args={[0.7, 0.7, 5,30]} />
//              <meshLambertMaterial color="grey" />
//             </mesh> */}

//             {/* <mesh position={[-6, 0, 1.2]}>
//              <cylinderBufferGeometry attach="geometry" args={[0.7, 0.7, 5,30]} />
//              <meshLambertMaterial color="#f5d273" />
//             </mesh>
//             <mesh position={[-6, 0, -1.2]}>
//              <cylinderBufferGeometry attach="geometry" args={[0.7, 0.7, 5,30]} />
//              <meshLambertMaterial color="#f5d273" />
//             </mesh>

//             <mesh position={[-3, 0, 1.2]}>
//              <cylinderBufferGeometry attach="geometry" args={[0.7, 0.7, 5,30]} />
//              <meshLambertMaterial color="#f5d273" />
//             </mesh>
//             <mesh position={[-3, 0, -1.2]}>
//              <cylinderBufferGeometry attach="geometry" args={[0.7, 0.7, 5,30]} />
//              <meshLambertMaterial color="#f5d273" />
//             </mesh>

//             <mesh position={[0, 0, 1.2]}>
//              <cylinderBufferGeometry attach="geometry" args={[0.7, 0.7, 5,30]} />
//              <meshLambertMaterial color="#f5d273" />
//             </mesh>
//             <mesh position={[0, 0, -1.2]}>
//              <cylinderBufferGeometry attach="geometry" args={[0.7, 0.7, 5,30]} />
//              <meshLambertMaterial color="#f5d273" />
//             </mesh>

//             <mesh position={[3, 0, 1.2]}>
//              <cylinderBufferGeometry attach="geometry" args={[0.7, 0.7, 5,30]} />
//              <meshLambertMaterial color="#f5d273" />
//             </mesh>
//             <mesh position={[3, 0, -1.2]}>
//              <cylinderBufferGeometry attach="geometry" args={[0.7, 0.7, 5,30]} />
//              <meshLambertMaterial color="#f5d273" />
//             </mesh>

//             <mesh position={[6, 0, 1.2]}>
//              <cylinderBufferGeometry attach="geometry" args={[0.7, 0.7, 5,30]} />
//              <meshLambertMaterial color="#f5d273" />
//             </mesh>
//             <mesh position={[6, 0, -1.2]}>
//              <cylinderBufferGeometry attach="geometry" args={[0.7, 0.7, 5,30]} />
//              <meshLambertMaterial color="#f5d273" />
//             </mesh> */}

//             {/* {elevationLeft(0,0)}
//             {elevationLeft(0.1,0.04)}
//             {elevationLeft(0.2,0.08)}
//             {elevationLeft(0.3,0.12)}
//             {elevationLeft(0.4,0.16)}
//             {elevationLeft(0.5,0.20)}
//             {elevationLeft(0.6,0.24)}
//             {elevationLeft(0.7,0.28)}
//             {elevationLeft(0.8,0.32)}
//             {elevationLeft(0.9,0.36)}
//             {elevationLeft(1.0,0.40)}
//             {elevationLeft(1.1,0.44)}
//             {elevationLeft(1.2,0.48)}
//             {elevationLeft(1.3,0.52)}
//             {elevationLeft(1.4,0.56)}
//             {elevationLeft(1.5,0.60)}
//             {elevationLeft(1.6,0.64)}
//             {elevationLeft(1.7,0.68)}
//             {elevationLeft(1.8,0.72)}
//             {elevationLeft(1.9,0.76)}
//             {elevationLeft(2.0,0.80)}
//             {elevationLeft(2.1,0.84)}
//             {elevationLeft(2.2,0.88)}
//             {elevationLeft(2.3,0.92)}
//             {elevationLeft(2.4,0.96)}
//             {elevationLeft(2.5,1.00)}
//             {elevationLeft(2.6,1.04)}
//             {elevationLeft(2.7,1.08)}
//             {elevationLeft(2.8,1.12)}
//             {elevationLeft(2.9,1.16)}
//             {elevationLeft(3.0,1.20)}
//             {elevationLeft(3.1,1.24)}
//             {elevationLeft(3.2,1.28)}
//             {elevationLeft(3.3,1.32)}
//             {elevationLeft(3.4,1.36)}
//             {elevationLeft(3.5,1.40)}
//             {elevationLeft(3.6,1.44)}
//             {elevationLeft(3.7,1.48)}
//             {elevationLeft(3.8,1.52)}
//             {elevationLeft(3.9,1.56)}
//             {elevationLeft(4.0,1.60)}
//             {elevationLeft(4.1,1.64)}
//             {elevationLeft(4.2,1.68)}
//             {elevationLeft(4.3,1.72)}
//             {elevationLeft(4.4,1.76)}
//             {elevationLeft(4.5,1.80)}
//             {elevationLeft(4.6,1.84)}
//             {elevationLeft(4.7,1.88)}
//             {elevationLeft(4.8,1.92)}
//             {elevationLeft(4.9,1.96)}
//             {elevationLeft(5.0,2.00)}
//             {elevationLeft(5.1,2.04)}
//             {elevationLeft(5.2,2.08)}
//             {elevationLeft(5.3,2.12)}
//             {elevationLeft(5.4,2.16)}
//             {elevationLeft(5.5,2.20)}
//             {elevationLeft(5.6,2.24)}
//             {elevationLeft(5.7,2.28)}
//             {elevationLeft(5.8,2.32)}
//             {elevationLeft(5.9,2.36)}
//             {elevationLeft(6.0,2.40)}

//             {elevationRight(0,0)}
//             {elevationRight(0.1,0.04)}
//             {elevationRight(0.2,0.08)}
//             {elevationRight(0.3,0.12)}
//             {elevationRight(0.4,0.16)}
//             {elevationRight(0.5,0.20)}
//             {elevationRight(0.6,0.24)}
//             {elevationRight(0.7,0.28)}
//             {elevationRight(0.8,0.32)}
//             {elevationRight(0.9,0.36)}
//             {elevationRight(1.0,0.40)}
//             {elevationRight(1.1,0.44)}
//             {elevationRight(1.2,0.48)}
//             {elevationRight(1.3,0.52)}
//             {elevationRight(1.4,0.56)}
//             {elevationRight(1.5,0.60)}
//             {elevationRight(1.6,0.64)}
//             {elevationRight(1.7,0.68)}
//             {elevationRight(1.8,0.72)}
//             {elevationRight(1.9,0.76)}
//             {elevationRight(2.0,0.80)}
//             {elevationRight(2.1,0.84)}
//             {elevationRight(2.2,0.88)}
//             {elevationRight(2.3,0.92)}
//             {elevationRight(2.4,0.96)}
//             {elevationRight(2.5,1.00)}
//             {elevationRight(2.6,1.04)}
//             {elevationRight(2.7,1.08)}
//             {elevationRight(2.8,1.12)}
//             {elevationRight(2.9,1.16)}
//             {elevationRight(3.0,1.20)}
//             {elevationRight(3.1,1.24)}
//             {elevationRight(3.2,1.28)}
//             {elevationRight(3.3,1.32)}
//             {elevationRight(3.4,1.36)}
//             {elevationRight(3.5,1.40)}
//             {elevationRight(3.6,1.44)}
//             {elevationRight(3.7,1.48)}
//             {elevationRight(3.8,1.52)}
//             {elevationRight(3.9,1.56)}
//             {elevationRight(4.0,1.60)}
//             {elevationRight(4.1,1.64)}
//             {elevationRight(4.2,1.68)}
//             {elevationRight(4.3,1.72)}
//             {elevationRight(4.4,1.76)}
//             {elevationRight(4.5,1.80)}
//             {elevationRight(4.6,1.84)}
//             {elevationRight(4.7,1.88)}
//             {elevationRight(4.8,1.92)}
//             {elevationRight(4.9,1.96)}
//             {elevationRight(5.0,2.00)}
//             {elevationRight(5.1,2.04)}
//             {elevationRight(5.2,2.08)}
//             {elevationRight(5.3,2.12)}
//             {elevationRight(5.4,2.16)}
//             {elevationRight(5.5,2.20)}
//             {elevationRight(5.6,2.24)}
//             {elevationRight(5.7,2.28)}
//             {elevationRight(5.8,2.32)}
//             {elevationRight(5.9,2.36)}
//             {elevationRight(6.0,2.40)} */}


            
//             {/* <mesh position={[0,2.40,0]}>
//                 <boxBufferGeometry args={[16,0.5,6]} attach='geometry' />
//                 <meshLambertMaterial color='#383838' />
//             </mesh> */}

//             {/* {length > 0 && <Base length={length} />} */}

//             {/* {leftElevation && <SlopeLeft x={slopeLeftPosition} />} */}
//             {/* {rightElevation && <SlopeRight x={slopeRightPosition} />} */}

//             {/* {pillars.map(pillar => {
//                 return pillar
//             })}
//             <Pillar x={-13} />
//             <Pillar x={-6} /> */}


//            {/* <Pillar x={0} /> */}

//            {/* <Pillar x={-5} /> */}
//            {/* <Pillar x={5} /> */}
//             </Suspense>
//         </Canvas>
//     </div>
// }

export default Modelling