import React, { useEffect, useContext } from "react";
import { Route } from "react-router";
import styles from "./FolderDetails.module.css";
import {useDropzone} from 'react-dropzone'
import excelicon from "../resources/icons/excelicon.svg"
import pdficon from "../resources/icons/pdficon.svg"
import wordicon from "../resources/icons/wordicon.png"
import {useState} from "react";
import axios from "axios";
import moment from "moment";
import { logRoles } from "@testing-library/react";
import SelectedProjectContext from "../context/selectdProjectContext";
import S3 from 'aws-sdk/clients/s3';
// import S3 from "react-aws-s3";
import { decode } from 'base64-arraybuffer'
import {Button} from "./Button.style"
import {Card} from "./Card.style"
import ImageModal from "./ImageModal"
import CustomCalander from "./CustomCalendar"
import Dropdown from 'react-dropdown';

function FolderDetails(props){
    const [showModal, setShowmodal] = useState(false);
    const [Media, setMedia] = useState([]);
    const [files, setFiles] = useState();
    const [image, setImage ] = useState("");
    const [ MediaArr, setMediaArr ] = useState([]);
    const [imageModal, setImageModal] = useState(false);
    const [displayImage, setDisplayImage]= useState('')
    const { SelectedProject, setSelectedProject } = useContext(SelectedProjectContext)
    const [file, setFile] = useState()
    const [selectedDate, setSelectedDate] = useState('')
    const [filter, setFilter] = useState('')
 
    let modalHandler = () => {
        setShowmodal(true)
    }

    function getBase64(input) {
        var reader = new FileReader();
        reader.readAsDataURL(input);
        reader.onload = function () {
            const file = {
                name: input.name,
                type: input.type,
                size: input.size,
                base64: input.type == 'image/jpeg' ? reader.result.substr(23) : input.type == 'image/png' ? reader.result.substr(22) : '',
            };
            setFile(file)
        };
     }
   
     const uploadImageOnS3 = async () => {
        const s3bucket = new S3({
            accessKeyId: 'AKIA4JYUC3EWYFLZA7MU',
            secretAccessKey: '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',
            Bucket: 'temlinprod',
            key: file.name
        })
        let contentType = file.type;
        let contentDeposition = 'inline;filename="' + file.name + '"';
        const arrayBuffer = decode(file.base64);
        s3bucket.createBucket(() => {
            const params = {
                Bucket: "temlinprod",
                Key: file.name,
                Body: arrayBuffer,
                ContentDisposition: contentDeposition,
                ContentType: contentType,
            };
            s3bucket.upload(params, (err, data) => {
                if (err) {
                    console.log('error in callback', err);
                }
                else if (data) {
                    const media = {
                        mediaID: data.key,
                        MediaUrl: data.Location,
                        MediaName: file.name,
                        createdBy: JSON.parse(localStorage.getItem('user')).Name,
                        mediaType: file.type == "image/jpeg" ? 'jpeg' : file.type == 'image/jpg' ? 'jpg' : file.type == 'image/png' ? 'png' : file.type == 'application/pdf' ? 'pdf' : file.type == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' ? 'xlsx' : file.type == 'application/vnd.openxmlformats-officedocument.wordprocessingml.document' ? 'docx' : '',
                        MediaSize: file.size,
                        createdTime: moment().format('DD-MM-YYYY hh:mm:ss A'),
                        createdate : moment().format('DD-MM-YYYY')
                    }
                  setMediaArr([media])
                }
            });
        });
    };
    

    // const upload = () => {
    //   const data = new FormData()
    //   data.append("file", image)
    //   data.append("upload_preset", process.env.REACT_APP_upload_preset)
    //   data.append("api_key", process.env.REACT_APP_API_KEY);
    //   data.append("cloud_name",process.env.REACT_APP_cloud_name);
    //   data.append("cloud_name",process.env.REACT_APP_cloud_name)
    //   fetch("https://api.cloudinary.com/v1_1/dlzo8dt7m/image/upload",{method:"post",
    //   body: data
    //   })
    //   .then(resp => resp.json())
    //   .then(data => {
    //   console.log("🚀 ~ file: FolderDetails.js ~ line 34 ~ upload ~ data", data)
          


    //     const media = {
    //         mediaID: data.asset_id,
    //         MediaUrl: data.secure_url,
    //         mediaType: data?.format == undefined ? 'xlsx' : data?.format,
    //         MediaSize: data.bytes,
    //         Date : moment().format('DD-MM-YYYY')
    //     }
    //   setMediaArr([media])
    //   })
    //   .catch(err => console.log(err))
    //   }

      const _uploadMediaInFolder = async ()=>{

         let data = {
             projectId: localStorage.getItem('pid'),
             folderId: props.match.params.id,
             Media: MediaArr
         }
         const options = {
            headers: {
                'Content-type': 'application/json',
                "Authorization": 'Bearer ' + localStorage.getItem('token')
            }
        };
        const res = await axios.put(process.env.REACT_APP_DURL + 'updatefolder', data,options )
        if(res.status== 200 ){
            _getFolderImages();
        }
     }   
    
    let modalHideHandler = () => {
        setShowmodal(false)
    }

    let _getFolderImages = async () => {
        let options = {
            headers: {
                'Content-type': 'application/json',
                "Access-Control-Allow-Origin": "*",
                "Authorization": 'Bearer ' + localStorage.getItem('token')
            }
        }
        let res = await axios.get(process.env.REACT_APP_DURL +'getfolderbyProject/'+localStorage.getItem('pid'),options)
        
        if(selectedDate == ''){
        setMedia(res.data.folder.filter(i => i._id == props.match.params.id)[0].MediaUrl);
        } else {
        setMedia(res.data.folder.filter(i => i._id == props.match.params.id)[0].MediaUrl.filter(j => j.createdate == selectedDate))
    }
    }

    let changeHandler = (date) => {
        setSelectedDate(moment(date).format('DD-MM-YYYY'));
    }

    useEffect(()=>{
        _getFolderImages();
    },[SelectedProject,selectedDate,filter])
  

    useEffect(()=>{
        _uploadMediaInFolder()
    },[MediaArr])

    let filtered = []
    Media.map(i => {
        if(i.mediaType == filter){
            filtered.push(i)
        }
    })

     return (
        <React.Fragment>
            {showModal &&  <Card className={styles.modal}>
            <div className={styles.modalHeader}><h3>Upload Files</h3></div>
            <div className={styles.modalBody}>
                 <div >
                 <label for="file-upload" >
                        <div className={styles.customFileUpload}><i className="fas fa-upload fa-10x"></i></div>
                    </label>
                <input id='file-upload' className={styles.inputFile} type="file" onChange= {(e) => {
                    setImage(e.target.files[0])
                    getBase64(e.target.files[0])
                  }}></input>
                </div>
            <p style={{color:'rgb(27, 27, 27)',fontWeight:'bold',margin:'10px 0'}}>Drag your files here to upload</p><p style={{color:'grey'}}>Or click to browse</p></div>
            <div className={styles.modalFooter}>
                <Button onClick={()=>modalHideHandler()} color="#22242E" bordered>Cancel</Button>
                <Button onClick={()=>{
                    uploadImageOnS3()
                    modalHideHandler();
                }} color="#0A60BD">Upload</Button>
            </div>
        </Card>}

        {imageModal && <ImageModal image={displayImage} close={()=>{
        setImageModal(false)
        document.getElementById('main').style.filter = 'none';
        }} />}

        <div id='main' className={styles.main}>

        <div style={{display:'flex',alignItems:'center',justifyContent:'space-between'}}>
        <Button color="#0A60BD" onClick={()=>modalHandler()}>Upload</Button>
       <div style={{width:'20%',marginLeft:'40px'}}>
       <Dropdown arrowClassName={styles.dropdownArrow} className={styles.dropdownContainer} menuClassName={styles.dropdownMenu} controlClassName={styles.dropdown}
        onChange={(e)=>setFilter(e.value)} options={
            [{label: 'Images',
            value: 'jpeg'},
            {label: 'Excel Sheets',
            value: 'xlsx'},
            {label: 'Word Documents',
            value: 'docx'},
            {label: 'Pdf Documents',
            value: 'pdf'},
            ]} value="All Media"/>
       </div>
        <div className={styles.calendar}>
         <CustomCalander onChange={changeHandler}/>
           <h5>{selectedDate ? selectedDate : 'Select Date'}</h5>
         </div>
        </div>
        

        <div className={styles.docs}>
           {/* one image ----- */}
           {(filter == '' ? Media : filtered).map(media => {
               return media.mediaType === 'png' || media.mediaType === 'jpg' || media.mediaType === 'jpeg' ?
                   <div className={styles.doc}>
                       <img onClick={()=>{
                        setDisplayImage(media.MediaUrl)
                        setImageModal(true)
                        }} className={styles.folderImage} src={media.MediaUrl}/>
                        <p>{media.createdate} by {media.createdBy}</p>
                   </div>  : 
                   <div className={styles.doc}>
                    <a className={styles.imageLink} href={media.MediaUrl} download
                        ><img src={media.mediaType == 'xlsx' ? excelicon : media.mediaType == 'pdf' ? pdficon : media.mediaType == 'docx' ? wordicon : ''}/>
                     </a>
                     <p>{media.MediaName} <br></br> {media.createdate} by {media.createdBy}</p>
                   </div>
                })}
           {/* ------------ */}
         </div>
        </div>
        </React.Fragment>
    )
}


export default FolderDetails;