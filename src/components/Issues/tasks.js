import { Container } from "../Container.style";
import { PieChart, Pie, Sector, Cell, ResponsiveContainer, Tooltip } from 'recharts';
import React, { PureComponent, useContext, useEffect, useState } from 'react';
import ProjectTaskContext from "../../context/ProjectTaskContext";
import moment from "moment";
import SelectedProjectContext from "../../context/selectdProjectContext";
import AllprojectsContext from '../../context/AllprojectsContext';
import axios from "axios";
import { RefreshBtn } from "../RefreshBtn.style";
import { Route, Switch } from "react-router";
import { Link } from "react-router-dom";
import Task from "../Task";
import CustomCalander from "../CustomCalendar";
import { CustomContainer } from "../CustomContainer.style";
import { Card } from "../Card.style";
import styles from "./tasks.module.css";
import profitlogo from "../../resources/icons/profitlogo.svg"
import losslogo from "../../resources/icons/losslogo.svg"
import { analytics } from "../Analytics"
import { logEvent } from "@firebase/analytics";
import Search from "../Search";
import { CostFormatCommas } from "../CostFormatCommas"
import { Button } from "../Button.style";
import editicon from '../../resources/icons/editicon.svg';
import loadingimg from '../../resources/loading.mp4'
import level from '../../resources/level.png';
import toplevel from '../../resources/toplevel.png';
import Dropdown from 'react-dropdown';
import { FixedSizeList as List } from 'react-window';


const data = [
    { name: 'Completed', value: 20 },
    // { name: 'Ongoing', value: 22 },
    // { name: 'Pending', value: 22 },
    // { name: 'Group C', value: 300 },
    // { name: 'Group D', value: 200 },
    // { name: 'Group E', value: 278 },
    // { name: 'Group F', value: 189 },

];
const COLORS = [ '#0088FE', '#00C49F', '#FFBB28' ];



function AllTasks(props) {
    const routes = props
    const { SelectedProject, setSelectedProject } = useContext(SelectedProjectContext)
    const { AllProjects } = useContext(AllprojectsContext)
    const [ isDelayed, setisDelayed ] = useState(false)
    const [ isCurrent, setisCurrent ] = useState(false)

    const [ taskFloor, setTaskFloor ] = useState()
    const [ selectedtasktype, setselectedtasktype ] = useState()
    const [ selectedBuilding, setselectedBuilding ] = useState()
    const [ selectedpocket, setselectedpocket ] = useState(undefined)
    console.log("🚀 ~ file: VisualProgress.js ~ line 18 ~ VisualProgress ~ selectedBuilding", selectedpocket)

    const [ floors, setfloors ] = useState([])
    const [ isloading, setisloading ] = useState(true)
    const [ selectedcontractor, setselectedcontractor ] = useState()

    const [ pockets, setpockets ] = useState([])
    const [ tasktypes, settasktypes ] = useState([])
    const [ buildingtype, setbuildingtype ] = useState([])

    const [ contractor, setcontractor ] = useState([])
    const [ isUpcoming, setisUpcoming ] = useState(false)
    const [ isInprogres, setisInprogres ] = useState(true)
    const [ islloading, setislloading ] = useState(true)
    const [ isPaused, setisPaused ] = useState(false)
    const [ isCompled, setisCompled ] = useState(false)
    const [ isRejected, setIsRejected ] = useState(false)
    const [ isAllTasks, setIsAllTasks ] = useState(true)
    const [ selecteddate, setselecteddate ] = useState()
    const [ search, setSearch ] = useState('')

    const [ plots, setPlots ] = useState([])
    const [ taskPlot, setTaskPlot ] = useState()


    const { ProjectTasks, setProjectTasks } = useContext(ProjectTaskContext)

    const [ allTasks, setAllTasks ] = useState([]);
    const [ createModule, setCreateModule ] = useState(false)
    const [ allModules, setAllModules ] = useState([])
    const [ moduleName, setModuleName ] = useState('')
    const [ moduleList, setModuleList ] = useState([]);
    const [ showModal, setShowModal ] = useState(false)
    const [ modalMessage, setModalMessage ] = useState(false)

    async function _setTaskid(id) {
        localStorage.setItem('tid', id)
    }

    function diffDates(startDate, endDate) {
        var startDate = moment(startDate, "D-MMM-YY");
        var endDate = moment(endDate, "D-MMM-YY");
        var result = 'Diff: ' + endDate.diff(startDate, 'days');
        return result
    }




    async function _getProjectTasks() {
        const options = {
            headers: {
                'Content-type': 'application/json',
                "Authorization": 'Bearer ' + localStorage.getItem('token')
            }
        };
        const tasksres = await axios.get(process.env.REACT_APP_DURL + 'getprojecttask/' + SelectedProject, options)
        localStorage.setItem('pid', SelectedProject)
    }

    let _getAllTasks = async () => {
        setisloading(true)
        const options = {
            headers: {
                'Content-type': 'application/json',
                "Authorization": 'Bearer ' + localStorage.getItem('token')
            }
        };
        let res = await axios.get(process.env.REACT_APP_DURL + 'getTasksByPlot/' + SelectedProject + '/' + selectedpocket + '/', options)
        if (res.status == 200) {
            setAllTasks(res.data);
            setisloading(false)
        }



    }

    const getAllPlots = async () => {
        const options = {
            headers: {
                'Content-type': 'application/json',
                "Authorization": 'Bearer ' + localStorage.getItem('token')
            }
        };

        let resp = await axios.get(process.env.REACT_APP_DURL + 'getAllFloors/' + localStorage.getItem('pid'), options)
        console.log("🚀 ~ file: VisualProgress.js ~ line 35333 ~ getAllPlots ~ resp", resp.data)
        if (resp.status == 200) {
            setfloors(resp.data.floor)
            setpockets(resp.data.pocket)
            setbuildingtype(resp.data.buildingtype)
            settasktypes(resp.data.typeoftask)
            setcontractor(resp.data.contractor)

            if (taskFloor == "" || taskFloor == undefined || taskFloor == null)
                resp.data.floor[ 0 ] && setTaskFloor(resp.data.floor[ 0 ]._id)
            if (selectedpocket == "" || selectedpocket == undefined || selectedpocket == null)
                resp.data.pocket[ 0 ] && setselectedpocket(resp.data.pocket[ 0 ]._id)
            if (selectedBuilding == "" || selectedBuilding == undefined || selectedBuilding == null)
                resp.data.buildingtype[ 0 ] && setselectedBuilding(resp.data.buildingtype[ 0 ]._id)
            if (selectedtasktype == "" || selectedtasktype == undefined || selectedtasktype == null)
                resp.data.typeoftask[ 0 ] && setselectedtasktype(resp.data.typeoftask[ 0 ]._id)
            if (selectedcontractor == "" || selectedcontractor == undefined || selectedcontractor == null)
                resp.data.contractor[ 0 ] && setselectedcontractor(resp.data.contractor[ 0 ]._id)
        }
        let res = await axios.get(process.env.REACT_APP_DURL + 'getAllPlots/' + localStorage.getItem('pid'), options)
        if (res.status == 200) {
            setPlots(res.data);
            if (taskPlot == "" || taskPlot == undefined || taskPlot == null) {
                setTaskPlot(res.data[ 0 ]._id)
            }
        }
    }

    const editTaskHandler = (e) => {
        // window.location.pathname = '/editTask'
        e.preventDefault()
        console.log("edit clicked");
    }

    useEffect(() => {
        // _getProjectTasks()
        _getAllTasks()
        getAllPlots()
    }, [ SelectedProject, selectedpocket ])
    // }, [ SelectedProject, search, taskPlot, taskFloor ])

    const TaskCard = (props) => {
        return (
            <Card style={ { width: '300px', marginRight: 20, maxWidth: '300px', maxHeight: '290px' } } color="#fff">
                { createModule && <div>
                    <input checked={ moduleList.includes(props.data.taskId) ? true : false } onChange={ (e) => {
                        if (e.target.checked == true) {
                            setModuleList([ ...moduleList, props.data.taskId ])
                        } else if (e.target.checked == false) {
                            setModuleList(moduleList.filter(i => i !== props.data.taskId))
                        }
                    } } type='checkbox' />
                </div> }
                <Link to={ `/tasks/taskDetails` }>
                    <div className={ styles.taskCard } onClickCapture={ () => _setTaskid(props.data.taskId) }>
                        <div className={ styles.taskCardHeading }>
                            <div>
                                <h1>{ props.data.name }</h1>
                                {/* <h4>ARYAN INFRAHEIGHT</h4> */ }
                                {/* <h4>{props.data.type || 'Not specified'}</h4> */ }
                                {/* <div className={styles.creater}><img src={props.data.creater?.[0]?.url} /><p>{props.data.creater[0]?.name}</p></div> */ }
                            </div>
                            <Link to={ `/editTask/${props.data.taskId}` } >
                                <img style={ { width: '20px', height: '20px' } } className={ styles.editIcon } src={ editicon } /></Link>
                        </div>
                        <div className={ styles.taskCardDate }>
                            {/* <div className={styles.result}>
                                {props.data.ActualCost < props.data.PlannedCost ? <p style={{ color: '#2AC371' }}><img src={profitlogo} /> <span className='rupee'>₹</span>{CostFormatCommas(Math.abs(parseInt(props.data.ActualCost) - parseInt(props.data.PlannedCost)))}</p> : props.data.ActualCost == props.data.PlannedCost ? <p style={{ color: '#575757' }}>No Profit/Loss</p> : <p style={{ color: '#D53539' }}><img src={losslogo} /> <span className='rupee'>₹</span>{CostFormatCommas(Math.abs(parseInt(props.data.ActualCost) - parseInt(props.data.PlannedCost)))}</p>}
                            </div> */}

                            <p className={ styles.dateDelay }>
                                { isNaN(Math.abs(moment(props.data.endDate[ 0 ], 'D-MMM-YY').diff(moment(props.data.endDate[ props.data.endDate.length - 1 ], 'D-MMM-YY'), 'days'))) ? '0' : Math.abs(moment(props.data.endDate[ 0 ], 'D-MMM-YY').diff(moment(props.data.endDate[ props.data.endDate.length - 1 ], 'D-MMM-YY'), 'days')) } days delay
                            </p>

                        </div>

                        <div className={ styles.dates }>
                            <div className={ styles.line }></div>
                            <p>Planned Finish: <span>{ props.data.endDate[ 0 ] && props.data.endDate[ 0 ] !== null && props.data.endDate[ 0 ] }</span></p>

                            <p>Actual Finish: <span>{ props.data.endDate[ 5 ] && props.data.endDate[ 5 ] !== null && props.data.endDate[ 5 ] }</span></p>
                        </div>

                    </div>
                </Link>
            </Card>
        )
    }

    const CustomTooltip = ({ active, payload, label }) => {
        if (active && payload && payload.length) {
            return (
                <div className="custom-tooltip" style={ {
                    backgroundColor: '#ffffff',
                    padding: 10, borderRadius: 100
                } }>
                    <p className="label">{ `${payload[ 0 ].name} ${payload[ 0 ].value}%` }</p>
                </div>
            );
        }

        return null;
    };

    let getModuleList = async () => {
        let res = await axios.get(process.env.REACT_APP_DURL + 'getAllModules/' + SelectedProject);
        if (res.status == 200) {
            setAllModules(res.data);
        }
    }

    let addModuleHandler = async () => {
        if (moduleName === '') return

        let data = {
            projectId: SelectedProject,
            moduleName,
            moduleList
        }

        let res = await axios.post(process.env.REACT_APP_DURL + 'addModule', data);
        if (res.status == 200) {

            // for analytics -----
            const params = {
                Project_wise: localStorage.getItem('pid'),
                User_wise: localStorage.getItem('userid'),
                projectName: SelectedProject !== undefined ? AllProjects.find(i => i._id == SelectedProject).Name : '',
                userName: JSON.parse(localStorage.getItem('user')).Name,
                TodaysDate: moment().format('DD-MM-YY hh:mm:ss A'),
                SlectedDate: '',
                taskid: localStorage.getItem('tid') || '',
                taskname: ''
            }
            logEvent(analytics, "create_module_dashboard", params)
            // ---------------

            setModalMessage(true)
            setTimeout(() => {
                setModalMessage(false)
            }, 3000)
            setModuleName('')
            setModuleList([])
        }
    }

    let updateModuleHandler = async (id) => {
        let res = await axios.get(process.env.REACT_APP_DURL + 'getModuleById/' + id);
        let arr = []
        let finalarr = []
        res.data.map(i => {
            arr.push(i.taskId)
        })

        moduleList.map(i => {
            if (!arr.includes(i)) {
                finalarr.push(i)
            }
        })

        let updateData = {
            finalarr,
        }
        let response = await axios.put(process.env.REACT_APP_DURL + 'updateModule/' + id, updateData)
        // for analytics -----
        const params = {
            Project_wise: localStorage.getItem('pid'),
            User_wise: localStorage.getItem('userid'),
            projectName: SelectedProject !== undefined ? AllProjects.find(i => i._id == SelectedProject).Name : '',
            userName: JSON.parse(localStorage.getItem('user')).Name,
            TodaysDate: moment().format('DD-MM-YY hh:mm:ss A'),
            SlectedDate: '',
            taskid: localStorage.getItem('tid') || '',
            taskname: ''
        }
        logEvent(analytics, "add_to_module_dashboard", params)
        // ---------------
    }

    useEffect(() => {
        // getModuleList()
    }, [ moduleList ])

    let taskFormat = () => {
        let arr = [ {
            label: 'All Tasks',
            value: 'All Tasks'
        } ]
        // tasksList.map(i => {
        //     arr.push({
        //         label: `${i.name} ${i.issues.length > 0 ? `- ${i.issues.length} issues` : '- No issues'}`,
        //         value: i.taskId
        //     })
        // })
        return arr
    }


    return <React.Fragment>
        { !isloading ? null :
            <div style={ { position: 'fixed', height: '100vh', width: '100vw', zIndex: 2, backgroundColor: '#fff' } }>
                <video style={ { objectFit: 'contain', width: '300px', left: '50%', transform: 'translate(-50%,-50%)', top: '50%', backgroundColor: '#fff', position: 'fixed' } }
                    src={ loadingimg } autoPlay muted loop />
            </div>
        }
        <div id="tasks" className={ styles.tasks }>

            <Link style={ { marginTop: 10, } } to='/visualProgress'><Button width="100%" color="#0A60BD" >Visual Progress</Button></Link>
            <div style={ { display: 'flex', } }>

                <div style={ { position: 'relative' } }>
                    <div style={ {
                        background: '#fff', boxShadow: "6px 13px 20px #0000003f",
                        display: 'flex', flexDirection: 'column',
                        padding: 10, borderRadius: 10, marginTop: '20px',
                        marginLeft: '20px', marginRight: '50px',
                    } }>
                        <div>
                            <i style={ { color: '#727272' } } class="fa fa-filter"></i>
                            <span>&nbsp; Filters:</span>
                        </div>
                        <div style={ { width: '250px' } }>
                            <p style={ { marginTop: 10 } }>Select Pocket</p>
                            <select style={ { width: '100%', padding: 10, borderWidth: 2, borderRadius: 6, borderColor: '#0A60BD' } } className={ styles.dropdownContainer } onChange={ (e) => {
                                console.log("🚀 ~ file: VisualProgress.js ~ line 151pockt ~ VisualProgress ~ e", e.target.value)
                                setselectedpocket(e.target.value)
                            } } name="priority">
                                { pockets.map(i => (
                                    // i.plotId.includes(taskPlot) ?
                                    <option label={ i.name }
                                        value={ i._id }>{ i.name }
                                    </option>
                                    // : null
                                ))
                                }
                            </select>
                            <p>Select Plot</p>
                            <select style={ { width: '100%', padding: 10, borderWidth: 2, borderRadius: 6, borderColor: '#0A60BD' } } className={ styles.dropdownContainer } onChange={ (e) => {
                                console.log("🚀 ~ file: VisualProgress.js ~ line 151 ~ VisualProgress ~ e", e.target.value)
                                setTaskPlot(e.target.value)
                            } } name="priority">
                                <option value="" >Plots filter... </option>
                                { plots.map(i => (
                                    i.pocketId.includes(selectedpocket) ?
                                        <option label={ i.name + ' (' +
                                            allTasks.filter(res => res.plotId.includes(i._id)
                                            ).length + ')' }
                                            value={ i._id }>{ i.name }</option>
                                        : null
                                ))
                                }
                            </select>
                        </div>


                        <div style={ {} }>
                            <p style={ { marginTop: 10 } }>Select Contractor</p>
                            { contractor.map(i => (
                                i.plotId.includes(taskPlot) ?
                                    <>
                                        <div style={ { display: 'flex', justifyContent: 'space-between' } }>
                                            <div style={ { display: 'flex', alignItems: 'center' } }>
                                                <input id='con' name='con' onClick={ () => setselectedcontractor(i._id) } type='radio' />
                                                <p style={ { marginLeft: 10 } }>{ i.name }</p>
                                            </div>
                                            <p>{ ' (' +
                                                allTasks.filter(res => res.contractorId.includes(i._id) &&
                                                    res.plotId.includes(taskPlot)
                                                ).length + ')' }</p>
                                        </div>
                                    </>
                                    : null
                            ))
                            }
                        </div>
                        <p style={ { marginTop: 10 } }>Select Building</p>
                        <select style={ { width: '100%', maxWidth: '250px', padding: 10, borderWidth: 2, borderRadius: 6, borderColor: '#0A60BD' } } className={ styles.dropdownContainer } onChange={ (e) => {
                            console.log("🚀 ~ file: VisualProgress.js ~ line 151 ~ VisualProgress ~ e", e.target.value)
                            setselectedBuilding(e.target.value)
                        } } name="priority">
                            <option value="" >Building Filter...</option>
                            { buildingtype.map(i => (
                                i.contractorId.includes(selectedcontractor) ?
                                    <option label={ i.name + ' (' + allTasks.filter(
                                        res => res.buildingtypeId.includes(i._id) &&
                                            res.contractorId.includes(selectedcontractor) && 
                                            res.plotId.includes(taskPlot)

                                    ).length + ')' } value={ i._id }>{ i.name }</option>
                                    : null
                            ))
                            }
                        </select>
                        <p style={ { marginTop: 10 } }>Select Floor</p>
                        <select style={ { width: '100%', maxWidth: '250px', padding: 10, borderWidth: 2, borderRadius: 6, borderColor: '#0A60BD' } } className={ styles.dropdownContainer } onChange={ (e) => {
                            console.log("🚀 ~ file: VisualProgress.js ~ line 151 ~ VisualProgress ~ e", e.target.value)
                            setTaskFloor(e.target.value)
                        } } name="priority">
                            <option value="" >Floor Filter...</option>
                            { floors.map(i => (
                                // i.contractorId.includes(selectedcontractor) ?
                                <option label={ i.name + ' (' + allTasks.filter(
                                    res => res.floorId.includes(i._id) &&
                                        res.buildingtypeId.includes(selectedBuilding) &&
                                        res.contractorId.includes(selectedcontractor) &&
                                        res.plotId.includes(taskPlot) &&
                                        res.pocketId.includes(selectedpocket)
                                ).length + ')' } value={ i._id }>{ i.name }</option>
                                // : null
                            ))
                            }
                        </select>
                        <p style={ { marginTop: 10 } }>Select Task type</p>

                        { tasktypes.map(i => (
                            // i.buildingtypeId.includes(selectedBuilding) ?
                            <>
                                <div style={ { display: 'flex', justifyContent: 'space-between' } }>
                                    <div style={ { display: 'flex', alignItems: 'center' } }>
                                        <input id='con' name='con' onClick={ () => setselectedtasktype(i._id) } type='radio' />
                                        <p style={ { marginLeft: 10 } }>{ i.name }</p>
                                    </div>
                                    <p></p>
                                </div>
                            </>
                            // : null
                        ))
                        }



                    </div>
                </div>



                <div style={ {} } className={ styles.taskCards }>
                    { allTasks.map((res, index) => (
                        (selectedcontractor != "" && selectedtasktype != "" && selectedBuilding != "" &&
                            selectedpocket != "") && (res.contractorId.includes(selectedcontractor) &&
                                res.tasktypeId.includes(selectedtasktype) &&
                                res.plotId.includes(taskPlot) &&
                            res.floorId.includes(taskFloor) &&
                                res.buildingtypeId.includes(selectedBuilding) &&
                                res.pocketId.includes(selectedpocket)) ?
                            <TaskCard key={ index } data={ res } />
                            : null
                    )) }
                </div>
            </div>




        </div>
        { showModal && <Card className={ styles.moduleModal }>
            <form onSubmit={ (e) => {
                e.preventDefault()
                addModuleHandler(module._id)
            } }>
                <input value={ moduleName } onChange={ (e) => setModuleName(e.target.value) } placeholder="Create a new module..." type="text" />
                <Button color="#0A60BD" type="submit">Add</Button>
            </form>
            { modalMessage && <h4>Tasks added in new module.</h4> }
            { allModules.map(module => {
                return <Card shadow='none' color='#EAEDF0' margin='10px' padding='8px 20px' onClick={ () => updateModuleHandler(module._id) } key={ module._id }>{ module.name }</Card>
            }) }
            <Button className={ styles.closeModal } color="#22242E" bordered onClick={ () => {
                setShowModal(false)
                document.getElementById('tasks').style.filter = 'none'
            } }>Close</Button>
        </Card> }

    </React.Fragment>
}

export default AllTasks;