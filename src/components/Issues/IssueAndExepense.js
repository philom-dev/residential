import { Container } from "../Container.style";
import { PieChart, Pie, Sector, Cell, ResponsiveContainer, Tooltip } from 'recharts';
import React, { PureComponent, useContext, useEffect, useState } from 'react';
import axios from "axios";
import moment from "moment";
import SelectedProjectContext from "../../context/selectdProjectContext";
import { RefreshBtn } from "../RefreshBtn.style";
import IssuesAndExpensesContext from "../../context/IssuesAndExpensesContext";
import CustomCalander from "../CustomCalendar";
import styles from "./IssueAndExepense.module.css"
import emptyLogo from "../../logo/emptyIssues.svg"
import { Steps, Hints } from 'intro.js-react';
import { analytics } from "../Analytics"
import { logEvent } from "@firebase/analytics";
import { Oval } from  'react-loader-spinner'
import Dropdown from 'react-dropdown';

const COLORS = [ '#0088FE', '#00C49F', '#FFBB28' ];
const bgcolor = [ '#f7d5d8', '#d7eded' ]
const Textcolor = ['#FA9601', '#2AC371']

function IssueAndExpense(props) {
   
     const [ issueList, setissueList ] = useState([])
    // console.log("🚀 ~ file: IssueAndExepense.js ~ line 25 ~ IssueAndExpense ~ issueList", issueList)
    const [ ExpenseList, setExpenseList ] = useState([])
    const {isIssue, setIssue} = useContext(IssuesAndExpensesContext)
    const [ todaysdate, settodaysdate ] = useState(moment().format('YYYY-MM-DD'))
    const [ selectedDate, setSelectedDate ] = useState()
    const { SelectedProject, setSelectedProject } = useContext(SelectedProjectContext)
    const [dateRange, setDateRange] = useState([]);
    const [myarr, setmyarr] = useState([])
    const [mediaArray, setMediaArray] = useState([])
    const [showMore, setShowMore] = useState(false)
    const [tutorial, setTutorial] = useState(localStorage.getItem('tutorial1') ? false : true)
    const [loader, setLoader] = useState(true);
    const [taskId, setTaskId] = useState('All Tasks')
    const [tasksList, setTasksList] = useState([])

    let steps = [
        {
            intro: "<h1>Welcome to the Issues and Expenses Page!</h1><p>Click on next to have an overview of the section.</p>",
            tooltipClass: 'welcomemsg',
        },
        {
            element: '#toggleIssue',
            intro: '<h1>Toggle</h1><p>Click to toggle between issues and expenses.</p>',
            tooltipClass:'tooltip',
            position: 'right'
        },
        {
            element: '#calendar',
            intro: '<h1>Choose a date!</h1><p>Click to see the issues and expenses for a particular date.</p>',
            tooltipClass:'tooltip',
            position: 'right'
        },
        {
            element: '#card',
            intro: '<h1>Your Issue/Expense!</h1><p>Here are the details of your issue/expense.</p>',
            tooltipClass:'tooltip',
            position: 'bottom'
        },
        {
            element: '#askforupdatesbtn',
            intro: '<h1>Ask for updates</h1><p>Click on this button to ask for the updates for this issue/expense and see the updates live on your dashboard.</p>',
            tooltipClass:'tooltip',
            position: 'bottom'
        },
        {
            intro: '<h1>Great Going.</h1><p>You are doing great.</p>',
            tooltipClass:'tooltip',
        }
                
                ]
 

    async function _getIssuelist() {
        setLoader(true)
        let pid = localStorage.getItem('pid')
        const options = {
            headers: {
                'Content-type': 'application/json',
                "Authorization": 'Bearer ' + localStorage.getItem('token')
            }
        };
        const res = await axios.get(process.env.REACT_APP_DURL + 'issuelist/' + SelectedProject,options)
        if(res.status == 200) {
            setLoader(false)
            setissueList(res?.data)
        }
    }


    async function _getExpenseList() {
        setLoader(true)
        let pid = localStorage.getItem('pid')
        const options = {
            headers: {
                'Content-type': 'application/json',
                "Authorization": 'Bearer ' + localStorage.getItem('token')
            }
        };
        const res = await axios.get(process.env.REACT_APP_DURL + 'expenselist/' + SelectedProject,options)
        if(res.data == 200){
            setLoader(false)
            setExpenseList(res?.data)
        }
        
    }

    let taskFormat = () => {
        let arr = [{
            label: 'All Tasks',
            value:'All Tasks'
        }]
        tasksList.map(i => {
            arr.push({
                label: `${i.name} ${i.issues.length > 0 ? `- ${i.issues.length} issues` : '- No issues'}`,
                value: i.taskId
            })
        })
        return arr
    }



    useEffect(() => {
        _getIssuelist()
    }, [ SelectedProject ])


let getDates = (startDate, endDate) => {
    const dates = []
    let currentDate = startDate
    const addDays = function (days) {
      const date = new Date(this.valueOf())
      date.setDate(date.getDate() + days)
      return date
    }
    while (currentDate <= endDate) {
      dates.push(currentDate)
      currentDate = addDays.call(currentDate, 1)
    }
    return dates
  }

let changeHandler = (start, end) => {
     if(start && !end) {
         setSelectedDate([start])
    } else if(start && end) {
        setSelectedDate(getDates(start,end))
    }
}

let renderSteps = () => {
    return  <Steps
    enabled={true}
    steps={steps}
    initialStep={0}
    // onComplete={()=>{
    //     setCount(1);
    //     console.log('tutorial finished')
    // }}
    onExit={(e)=>{
        localStorage.setItem('tutorial1','finished')
        setTutorial(false)
        console.log(e)}}
    options={{
        skipLabel:'Skip',
        // hideNext:true,
        doneLabel: "Done",
        showProgress: true,
        showButtons:true,
        showStepNumbers: true,
        disableInteraction:false,
        exitOnOverlayClick:false,
        overlayOpacity: 0.6
    }} 
    />
}

    let issueArr = [];
    selectedDate ? selectedDate.map(e1 => (issueList.map(e2 => {if(moment(e2.createdate).format('DD-MM-YYYY') == moment(e1).format('DD-MM-YYYY')){issueArr.push(e2)}}))) : taskId !== 'All Tasks' ? issueList.map(i => {
        if(i.taskid == taskId){
        issueArr.push(i)
    }
    }) : issueArr = issueList.sort(function(a,b){
        return new Date(b.createdate) - new Date(a.createdate);
      });

    

    // console.log(issueList);

    let expenseArr = [];
    selectedDate ? selectedDate.map(e1 => (ExpenseList.map(e2 => {if(moment(e2.createdate).format('DD-MM-YYYY') == moment(e1).format('DD-MM-YYYY')){expenseArr.push(e2)}}))) : expenseArr = ExpenseList.sort(function(a,b){
        return new Date(b.createdate) - new Date(a.createdate);
      });

    return loader ? <Oval wrapperClass="pageLoading" strokeWidth={4} secondaryColor='#EAEDF0' color="#0A60BD" height={50} width={50} /> : <div id='issues' className={styles.issuesexpenses}>
        {tutorial && renderSteps()}

            <div style={{display:'flex',justifyContent:'space-between',alignItems:'center',marginBottom:'20px'}} >
                 <div className={styles.heading} id='toggleIssue' style={{ backgroundColor: '#fff', borderRadius: '8px' }} >
                    <button style={ {
                        backgroundColor: isIssue ? '#22242E' : '#fff',
                        color: isIssue ? '#fff' : '#22242E', 
                        } }
                        onClick={ () => setIssue(true) }>Issues</button>
                    <button style={ {
                        backgroundColor: !isIssue ? '#22242E' : '#fff',
                        color: !isIssue ? '#fff' : '#22242E'
                    } }
                        onClick={ () => setIssue(false) }>Expenses</button>
                </div>
                
                <div id='calendar'>
                    {isIssue ? <div className={styles.calendar}>
                       <CustomCalander showCount={true} listName='issuelist' onChange={changeHandler}/>
                       <h5>{selectedDate ? moment(selectedDate[0]).format('DD-MM-YYYY') : 'Select Date'}</h5>
                    </div> : <div className={styles.calendar}>
                        <CustomCalander showCount={true} listName='expenselist' onChange={changeHandler}/>
                       <h5>{selectedDate ? moment(selectedDate[0]).format('DD-MM-YYYY') : 'Select Date'}</h5>
                    </div>}
                    </div>
                 </div>

                 

         <div style={{display:'grid',gridTemplateColumns:'70% 28%',justifyContent:'space-between'}}>
             
             <div className={styles.issuesandexpensesList}>
               
               <div style={{width:'50%'}}>
                    {/* <Dropdown arrowClassName={styles.dropdownArrow} className={styles.dropdownContainer} menuClassName={styles.dropdownMenu} controlClassName={styles.dropdown}
                 onChange={(e)=>{
                   setTaskId(e.value)
                 }} options={taskFormat()} value="All Tasks"/> */}
                 </div>
            

                 {/* for tutorial------ */}
                      {tutorial && <div id='card' className={styles.issuesandexpensesCard}>
                         <h3>{isIssue ? 'Issue Name' : 'Expense Amount'}</h3>
                           <h3>Task Name</h3>
                            <h3>{ moment().format('DD MMMM YY') }</h3>
                            <h3 style={{
                                backgroundColor: bgcolor[0],
                                color: Textcolor[0]
                            }}>Pending</h3>
                            <button id='askforupdatesbtn'
                               className={styles.askForUpdatesBtn}>Ask for updates</button>
                         </div>}
                     {/* ----------- */}

            <div className={styles.tableHeader}>
                <p>{isIssue ? 'Issue' : 'Amount'}</p>
                    {/* <p>Task Name</p> */ }
                <p>Date</p>
                <p>Status</p>
                <p></p>
            </div>

            {isIssue && issueArr.length !== 0 ? issueArr.map((res, i) => {
                return <div onClick={()=>{
                    setMediaArray([ res.comment, res.MediaUrl ])
                    setShowMore(true)
                    }} key={i} className={styles.issuesandexpensesCard}>
                         <h3>{ res?.issue }</h3>
                    {/* <h3>{ res?.taskId[ 0 ] && res?.taskId[ 0 ].name }</h3> */ }
                            <h3>{ moment(res?.createdate).format('DD MMMM YY') }</h3>
                            <h3 style={{
                                fontWeight:'500',
                                color: res?.status == 'Pending' ? Textcolor[0] : Textcolor[1]
                            }}>{res?.status == 'Approve' ? 'Resolved' : res?.status}</h3>
                            {res?.status == "Pending" ? <button onClick={async ()=>{
                             //   const options = {
                                //     headers: {
                                //         'Content-type': 'application/json',
                                //         "Authorization": 'Bearer ' + localStorage.getItem('token')
                                //     }
                                // }
                                let result = await axios.post(process.env.REACT_APP_DURL + "sendPushApprover/" + res.approverId)

                                if(result.status == 200){
                                    let data = {
                                        token: [`${result.data.token}`],
                                        messageTitle: 'Issue',
                                        messageBody: `${res?.creatorname} raised an issue in task - ${res?.name}`,
                                        imageUrl: 'http://res.cloudinary.com/dlzo8dt7m/image/upload/v1626373909/IMG-20210715-WA0010_vt6qjx.jpg',
                                        key: 'issue'
                                      }

                                    let push = await axios.post(process.env.REACT_APP_DURL + "sendPush", data)
                                    if(push.status == 200){
                                        console.log(push.data.message);
                                    }
                                }
                            }} className={styles.askForUpdatesBtn}>Ask for updates</button> : <div style={{minWidth:'150px'}}></div>}
                        </div>
            }) : isIssue && !tutorial && <h3 className={styles.emptyState}>No Issues in this date. Please select another date.</h3>
            }


             {!isIssue && expenseArr.length !== 0 ? expenseArr.map((res, i) => (
                 <div key={i} className={styles.issuesandexpensesCard}>
                    
                                <h3><i className='fas fa-rupee-sign'></i> &nbsp;{res?.Amount}</h3>
                                {/* <div className='flex m-t-20'>
                                    { res?.Media.map((media, i) => (
                                        media?.mediaType == 'jpg' || media?.mediaType == 'png' ?

                                            <img onClickCapture={ () => {
                                                window.open(media?.MediaUrl, '_blank').focus()
                                            } } style={ {
                                                width: 90, height: 60, marginRight: 20, borderRadius: 10, objectFit: 'fill'
                                            } } src={ media?.MediaUrl } />
                                            : null
                                    ))} */}

                            {/* { !isIssue && ExpenseList.map((res, i) => (
                            res?.Media.map((media, i) => (
                                media?.mediaType == 'mp4' ?
                                    <div className='m-t-20'>
                                     <audio controls>
                                            <source src={ media?.MediaUrl } />
                                            Your browser does not support the audio element.
                                        </audio>
                                    </div>
                                    : null
                            ))
                        )) } */}

                        {/* </div> */}

                            <h3>{ res?.name }</h3>
                            <h3>{ moment(res?.createdate).format('DD-MM-YYYY') }</h3>
                            <h3 style={{
                                backgroundColor:'inherit',
                                color: res?.status == 'Reject' ? Textcolor[0] : Textcolor[1]
                            }} >{res?.status == 'Reject' ? 'Rejected' :
                                res?.status == 'Pending' ? 'Pending' :  'Approved'}</h3>
                            {res?.status == "Pending" ? <button onClick={async ()=>{

//   const options = {
//     headers: {
    //         'Content-type': 'application/json',
    //         "Authorization": 'Bearer ' + localStorage.getItem('token')
    //     }
    // }
    let result = await axios.post(process.env.REACT_APP_DURL + "sendPushApprover/" + res.approverId)
    
    if(result.status == 200){
        let data = {
            token: [`${result.data.token}`],
            messageTitle: 'Expense',
            messageBody: `${res?.creatorname} added expense of Rs. ${res?.Amount} in task - ${res?.name}`,
            imageUrl: 'http://res.cloudinary.com/dlzo8dt7m/image/upload/v1626373909/IMG-20210715-WA0010_vt6qjx.jpg',
            key: 'expense'
           }
           let push = await axios.post(process.env.REACT_APP_DURL + "sendPush", data)
           if(push.status == 200){
               console.log(push.data.message);
           }
        }
    }} className={styles.askForUpdatesBtn}>Ask for updates</button> : <div style={{minWidth:'150px'}}></div>}
                         </div>
                )) : !isIssue && !tutorial && <h3 className={styles.emptyState}>No expenses in this date. Please select another date.</h3>}
             </div>

            <div className={styles.issuesandexpensesMedia}>
          <h2>More Info</h2>
           <div className={styles.line}></div>

           {showMore ? <>
             <div className={styles.comments}>
                <h3>Additional comment if any added when creating issue:</h3>
                {mediaArray[0].map(i => {
                    return <p>{i.commentText ? i.commentText : 'Comment not specified'} <br></br> <span>by {i.commentby}</span></p>
                })}
                </div>
            <div className={styles.line}></div>
            <div className={styles.images}>
                <h2>Media</h2>
                <div className={styles.mediaImages}>
                  {mediaArray[1].map(i => {
                      if(i.mediaType == 'jpg' || i.mediaType == 'png'){
                        return <div className={styles.mediaImagesContainer}><img onClickCapture={ () => {
                            window.open(i.MediaUrl, '_blank').focus()
                        } } src={i.MediaUrl} /></div>
                      }
                  })}
                </div>
            </div>
            <div className={styles.line}></div>
            <div className={styles.audio}>
                <h2>Audio</h2>
            </div>
            </> : <h2 style={{fontSize:'1rem',fontWeight:'bold'}}>Select an issue/expense.</h2>}
        </div>


        {/* <RefreshBtn style={{marginLeft:'auto',marginBottom:'12px'}} onClick={()=>refreshHandler()}><i className="fas fa-redo"></i> &nbsp; Refresh</RefreshBtn> */}
         </div>
        </div>
}

export default IssueAndExpense;