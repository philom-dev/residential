import styled from 'styled-components';

export let CustomContainer = styled.div`
padding: 20px;
padding: ${(props) => props.padding};
border-radius: 12px;
border-radius: ${(props) => props.borderRadius};
background-color: ${(props) => props.color};
// box-shadow: 0px 2px 5px rgba(0,0,0,0.1);
margin: 20px 0;
margin: ${(props)=>props.margin}
border: ${(props) => props.border}
`