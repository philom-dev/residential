export default function changeNumberFormat(number, decimals, recursiveCall) {
    const decimalPoints = decimals || 2;
    const noOfLakhs = number / 100000;
    let displayStr;
    let isPlural;

    function roundOf(integer) {
        return +integer.toLocaleString(undefined, {
            minimumFractionDigits: decimalPoints,
            maximumFractionDigits: decimalPoints,
        });
    }

    if (number >= 10000000) {
        return number = (number / 10000000).toFixed(2) + ' Cr';
    } else if (number >= 100000) {
        return number = (number / 100000).toFixed(2) + ' Lac';
    }
    else {

    }

    return displayStr;
}