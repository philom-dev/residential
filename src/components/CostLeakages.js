import axios from "axios";
import { useEffect, useState } from "react";
import styles from "./CostLeakages.module.css"
import { CustomContainer } from "./CustomContainer.style";
import { Card } from "./Card.style";
import { Button } from "./Button.style";
import materialicon from "../resources/cost/time.png"
import machineicon from "../resources/cost/done.svg"
import con from "../resources/cost/con.svg"
import { CostFormatCommas } from "./CostFormatCommas"

function CostLeakages() {

    const [ insights, setInsights ] = useState(true)
    const [ lossTasks, setLossTasks ] = useState(false)

    const [ showDetails, setShowDetails ] = useState(false)
    const [ list, setList ] = useState([])
    const [ filter, setfilter ] = useState('All Insights')
    const [ dataListcontractor, setdataListcontractor ] = useState([
        "Foundation structure of Plot 13 is running late by 158 days",
        "3rd roof structure of 121 is running late by 27 days",
        "Foundation structure of Plot 153A is running late by 153 days",
        "Basement roof structure of Plot 16 is running late by 142 days",
        "2nd roof structure of Plot 121 is running late by 35 days",
        "Stilt roof structure of Plot 101 is running late by 137 days",
        "1st roof structure of Plot 143 is running late by 24 days",
        "Stilt roof structure of Plot 13 is running late by 129 days",
        "Basement roof structure of Plot 13 is running late by 148 days",
        "2nd roof structure of Plot 142 is running late by 16 days",
        "1st roof structure of Plot 121 is running late by 43 days"
    ])
    const [ dataList, setDataList ] = useState([
        "207 tasks of Aryan Infra are delayed by more than 15 days",
        "53 tasks of KCDPL are delayed by more than 15 days",
        "194 tasks of NDCC are delayed by more than 15 days",
        "222 tasks of Perfect are delayed by more than 15 days",
        "Most delayed task of Aryan - Basement roof structure, Plot 13",
        "Most delayed task of KCDPL - Foundation structure, Plot 153 A ",
        "Most delayed task of NDCC - Basement roof structure, Plot 101",
        "Most delayed task of Perfect - Basement roof structure, Plot 11A"
    ])
    const [ selectedData, setSelectedData ] = useState('')

    let _getCostDetails = async () => {
        const options = {
            headers: {
                'Content-type': 'application/json',
                "Authorization": 'Bearer ' + localStorage.getItem('token')
            }
        };
        let res = await axios.get(process.env.REACT_APP_DURL + "getLeakage/" + localStorage.getItem('pid'), options)
        if (res.status == 200) {
            console.log(res.data);
            setList(res.data)
            setDataList([ ...res.data.Macdata, ...res.data.Matdata ])
        }
    }

    useEffect(() => {
        //    _getCostDetails()
    }, [ localStorage.getItem('pid') ])

    return <div className={ styles.container }>

        <div style={ { display: 'grid', gridTemplateColumns: '28% 70%', gridGap: 10, justifyContent: 'space-between' } }>

            <div>
                <Card>
                    <Card margin='0' className={ styles.heading } color="#22242E">
                        <h1>Projected End Date</h1>
                        <h2 style={ { color: '#fff' } }>12 Dec 2022</h2>
                    </Card>

                    <div className={ styles.leakage }>
                        <div className={ styles.leakageIcon }><img src={ materialicon } /></div>
                        <div>
                            <p>Most delayed running task</p>
                            <h3>Foundation Structure,Plot 153A</h3>
                        </div>
                    </div>

                    <div className={ styles.leakage }>
                        <div className={ styles.leakageIcon }><img src={ machineicon } /></div>
                        <div>
                            <p>Most delayed completed task</p>
                            <h3>Basement roof Structure, Plot 101</h3>
                        </div>
                    </div>
                    <div className={ styles.leakage }>
                        <div className={ styles.leakageIcon }><img src={ con } /></div>
                        <div>
                            <p>Most delayed sub-contractor</p>
                            <h3>Perfect</h3>
                        </div>
                    </div>


                </Card>
                {/* <Card className={styles.predicted}>
            <h1><span className="rupee">₹</span>00,00,000</h1>
            <p>3% this week</p>
            <h2>Predicted Cost</h2>
            <Button color="#0A60BD">View Details</Button>
        </Card>  */}
            </div>
            <div>



                <Card style={ { background: '#848EA0' } }>
                    <h2 style={ { color: '#fff' } }>Project Delay Insights</h2>
                </Card>
                <Card className={ styles.insights }>
                    <div>
                        <button onClick={ () => setfilter('All Insights') } style={ { borderRadius: 100, background: filter == 'All Insights' ? '#171717' : '#A0A0A0', color: '#FFF' } }>
                            All Insights
                        </button>
                        <button onClick={ () => setfilter('Contractor Insights') } style={ { borderRadius: 100, background: filter == 'Contractor Insights' ? '#171717' : '#A0A0A0', color: '#FFF' } }>
                            Contractor Insights
                        </button>
                        <button onClick={ () => setfilter('Time Insights') } style={ { borderRadius: 100, background: filter == 'Time Insights' ? '#171717' : '#A0A0A0', color: '#FFF' } }>
                            Time Insights
                        </button>
                    </div>
                    { (filter == 'Contractor Insights' || filter == 'All Insights') && dataListcontractor.map(i => {
                        return <div onClick={ () => {
                            setShowDetails(true)
                            setSelectedData(i?._id)
                            if (selectedData == i?._id) {
                                setShowDetails(false)
                                setSelectedData('')
                            }
                        } } className={ styles.row }>
                            <div className={ styles.insight }>
                                <h2>{ i }</h2>
                                {/* <h3><span className="rupee">₹</span>{i.LeakageCost ? `${CostFormatCommas(parseInt(i.LeakageCost))}` : 'Not specified'}</h3> */ }
                                <button style={ { background: '#0A60BD', color: '#FFF' } }>Request Update</button>
                            </div>
                            {/* {showDetails && selectedData == i._id && <p>More details</p>} */ }
                        </div>
                    }) }

                    { (filter == 'Time Insights' || filter == 'All Insights') && dataList.map(i => {
                        return <div onClick={ () => {
                            setShowDetails(true)
                            setSelectedData(i?._id)
                            if (selectedData == i?._id) {
                                setShowDetails(false)
                                setSelectedData('')
                            }
                        } } className={ styles.row }>
                            <div className={ styles.insight }>
                                <h2>{ i }</h2>
                                {/* <h3><span className="rupee">₹</span>{i.LeakageCost ? `${CostFormatCommas(parseInt(i.LeakageCost))}` : 'Not specified'}</h3> */ }
                                <button style={ { background: '#0A60BD', color: '#FFF' } }>Request Update</button>
                            </div>
                            {/* {showDetails && selectedData == i._id && <p>More details</p>} */ }
                        </div>
                    }) }

                </Card>
            </div>



        </div>

        {/* <div className={styles.toggle}>
        <button onClick={()=>{
            setInsights(true)
            setLossTasks(false)
            }} className={insights ? styles.focused : ''}>Correction Insights</button>
        <button onClick={()=>{
            setInsights(false)
            setLossTasks(true)
            }} on className={lossTasks ? styles.focused : ''}>Loss Making Tasks</button>
    </div>

    {insights && <div className={styles.table}>
            <div className={styles.tableheader}>
                <p>Resource Name</p>
                <p>Amount</p>
            </div>

            <div className={styles.tablebody}>

            {dataList.map(i => {
                return <div onClick={()=>{
                    setShowDetails(true)
                    setSelectedDate(i._id)
                    if(selectedData == i._id){
                        setShowDetails(false)
                        setSelectedDate('')
                    }
                    }} className={styles.tablerow}>
                <p>{i.MachineReadingId ? i.MachineReadingId.MachineName : i.MaterialProducedId ? i.MaterialProducedId.ProductName : 'No name specified'} <span>{i.taskId && `(${i.taskId.name})`}</span></p>

                <p style={{display:'grid',gridTemplateColumns:'50% 50%',justifyContent:'space-between',alignItems:'center'}}>
                    <p>{i.LeakageCost ? `₹ ${parseInt(i.LeakageCost)}` : 'Not specified'}</p>
                    <p className={styles.tableicons}>
                        <div style={{backgroundColor: i.MachineReadingId ? '#EA5253' : '#979797'}} className={styles.tableicon}><img src={machineicon} /></div>
                        <div style={{backgroundColor: i.MaterialProducedId ? '#EA5253' : '#979797'}} className={styles.tableicon}><img src={materialicon} /></div>
                    </p>
                </p>
                {showDetails && selectedData == i._id && <p className={styles.moreDetails}>{i.Expression ? i.Expression : 'No Expression'}</p>}
                </div>
            })}
            </div>

        </div>} */}



        {/* {lossTasks && <div>
    <div className={styles.tablerow}>
        <p>Task Name</p>
        <p>₹ 00,00,000</p>
     </div>
    </div>} */}

    </div>
}
export default CostLeakages