import styles from "./Search.module.css"
import searchicon from "../resources/icons/search.png"
import { PresignedPost } from "aws-sdk/clients/s3"
import { useState } from "react"

function Search(props) {

    const [text, setText] = useState('')

    return <div className={styles.search}>
        <input onKeyUp={(e)=>{
            e.preventDefault()
            props.search(e.target.value)
        }} type='text' placeholder="Search" />
         <img src={searchicon}/>
    </div>
}

export default Search