import styles from "./AllMedia.module.css"
import React, { useState, useContext, useEffect } from 'react';
import SelectedProjectContext from "../context/selectdProjectContext";
import axios from "axios";
import { CustomContainer } from "./CustomContainer.style";
import { Card } from "./Card.style"
import { Button } from "./Button.style"
import CustomCalander from "./CustomCalendar";
import moment from "moment"
import Dropdown from 'react-dropdown';
import ImageModal from "./ImageModal";


function AllMedia(){

    const [mediaArr, setMediaArr] = useState([]);
    const { SelectedProject, setSelectedProject } = useContext(SelectedProjectContext)
    const [imageModal, setImageModal] = useState(false);
    const [displayImage, setDisplayImage]= useState('')
    const [filterId, setFilterId] = useState('')
    const [selectedDate, setSelectedDate] = useState('')
    const [tasksArr, setTasksArr] = useState([])

    const _getAllMedia = async () => {
        const options = { 
            headers: {
                'Content-type': 'application/json',
                "Authorization": 'Bearer ' + localStorage.getItem('token')
            }
        };
        let res = await axios.get(process.env.REACT_APP_DURL + 'allmedia/' + SelectedProject,options)
        let arr = []
        res.data[0].task.map(i => {
            i.Media.map(j => {
                if(j.mediaType == "jpg" || j.mediaType == 'png' || j.mediaType == 'jpeg'){
                    arr.push({
                        MediaSize: j.MediaSize,
                        MediaUrl: j.MediaUrl,
                        UploadBy: j.UploadBy,
                        createdate: j.createdate,
                        mediaID: j.mediaID,
                        mediaType: j.mediaType,
                        taskID: i.taskID
                    })
                }
             })
        })
        setTasksArr(res.data[0].task)
        setMediaArr(arr.sort(function(a,b){
            return new Date(moment(b.createdate,"DD-MM-YYYY").format('YYYY,M,DD')) - new Date(moment(a.createdate,"DD-MM-YYYY").format('YYYY,M,DD'))
            }))
    }

    let changeHandler = (start) => {
        setSelectedDate(moment(start).format('DD-MM-YYYY'));
   }
    
    useEffect(() => {
        _getAllMedia()
    },[SelectedProject])



 return <React.Fragment>
    
    {imageModal && <ImageModal image={displayImage} close={()=>{
        setImageModal(false)
        document.getElementById('main').style.filter = 'none';
    }} />}

   <div id="main" className={styles.main}>
      <div className={styles.filter}>
            <p style={{backgroundColor: filterId == '' && '#0A60BD', color: filterId == '' && '#fff'}} onClick={()=>setFilterId('')}>All Tasks</p>
            {tasksArr.map(task => {
                if(task.Media.filter(i => i.mediaType == "jpg" || i.mediaType == 'png' || i.mediaType == 'jpeg').length !== 0){
                return <p style={{backgroundColor: filterId == task.taskID && '#0A60BD', color: filterId == task.taskID && '#fff'}} onClick={()=>{
                    setSelectedDate('')
                    setFilterId(task.taskID)}
                }>{task.name}</p>
                }
            })}
        </div>

     <div style={{marginLeft:'30%'}}>
     <div className={styles.calendar}>
         <CustomCalander onChange={changeHandler}/>
           <h5>{selectedDate ? selectedDate : 'Select Date'}</h5>
         </div>

        <div className={styles.docs}>
            {mediaArr.map(j => {
                if(filterId == '' && selectedDate == ''){
                    if(j.mediaType == "jpg" || j.mediaType == 'png' || j.mediaType == 'jpeg'){
                    return <div className={styles.doc}>
                    <div className={styles.image}>
                    <img onClick={()=>{
                             setDisplayImage(j.MediaUrl)
                             setImageModal(true)
                             document.getElementById('main').style.filter = 'blur(3px)'
                             }}  src={j.MediaUrl}/>
                    </div>
                     <p>{j.createdate} by {j.UploadBy}</p>
                 </div>  
                }}
                else if(j.taskID == filterId || j.createdate == selectedDate){
                if(j.mediaType == "jpg" || j.mediaType == 'png' || j.mediaType == 'jpeg'){
                return <div className={styles.doc}>
                    <div className={styles.image}>
                    <img onClick={()=>{
                        setDisplayImage(j.MediaUrl)
                        setImageModal(true)
                        document.getElementById('main').style.filter = 'blur(3px)'
                        }} src={j.MediaUrl}/>
                    </div>
                <p>{j.createdate} by {j.UploadBy}</p>
            </div>
            }}})}
        </div> 
        </div> 
     </div>
   </React.Fragment>
}

export default AllMedia;