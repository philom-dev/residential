import styles from "./VisualProgress.module.css"
import Dropdown from 'react-dropdown';
import { useEffect, useState } from "react";
import axios from "axios";
import { Button } from "./Button.style";
import { Link } from "react-router-dom";
import moment from 'moment'
import { Card } from "./Card.style"
import Skeleton, { SkeletonTheme } from 'react-loading-skeleton'
import 'react-loading-skeleton/dist/skeleton.css'
import loadingimg from '../resources/loading.mp4'

let colors = [ '#eab64f', '#0f2e29', '#306B62', '#C1DEAE', '#E8E8A6', '#F2F5C8', '#655A49' ]
function VisualProgress() {

    const [ taskPlot, setTaskPlot ] = useState()
    const [ selectedtasktype, setselectedtasktype ] = useState()
    const [ selectedBuilding, setselectedBuilding ] = useState()
    const [ selectedpocket, setselectedpocket ] = useState()
    // console.log("🚀 ~ file: VisualProgress.js ~ line 18 ~ VisualProgress ~ selectedBuilding", selectedpocket)
    const [ selectedcontractor, setselectedcontractor ] = useState()
    const [ taskFloor, setTaskFloor ] = useState()
    const [ isloading, setisloading ] = useState(true)
    const [ plots, setPlots ] = useState([])
    const [ floors, setfloors ] = useState([])
    const [ pockets, setpockets ] = useState([])
    const [ filter, setfilter ] = useState('MEP')
    const [ tasktypes, settasktypes ] = useState([])
    const [ buildingtype, setbuildingtype ] = useState([])
    const [ contractor, setcontractor ] = useState([])
    const [ filtercontractor, setfiltercontractor ] = useState([])
    const [ allTasks, setAllTasks ] = useState([])

    const getAllPlots = async () => {
        const options = {
            headers: {
                'Content-type': 'application/json',
                "Authorization": 'Bearer ' + localStorage.getItem('token')
            }
        };

        let resp = await axios.get(process.env.REACT_APP_DURL + 'getAllFloors/' + localStorage.getItem('pid'), options)
        // console.log("🚀 ~ file: VisualProgress.js ~ line 35333 ~ getAllPlots ~ resp", resp.data.newPlots[ 0 ])
        if (resp.status == 200) {
            setfloors(resp.data.floor)
            setpockets(resp.data.pocket)
            setbuildingtype(resp.data.buildingtype)
            settasktypes(resp.data.typeoftask)
            setcontractor(resp.data.contractor)

            if (taskFloor == "" || taskFloor == undefined || taskFloor == null)
                resp.data.floor[ 0 ] && setTaskFloor(resp.data.floor[ 1 ]._id)
            if (selectedpocket == "" || selectedpocket == undefined || selectedpocket == null)
                resp.data.pocket[ 0 ] && setselectedpocket(resp.data.pocket[ 0 ]._id)
            if (selectedBuilding == "" || selectedBuilding == undefined || selectedBuilding == null)
                resp.data.buildingtype[ 0 ] && setselectedBuilding(resp.data.buildingtype[ 0 ]._id)
            if (selectedtasktype == "" || selectedtasktype == undefined || selectedtasktype == null)
                resp.data.typeoftask[ 0 ] && setselectedtasktype(resp.data.typeoftask[ 0 ]._id)
            if (selectedcontractor == "" || selectedcontractor == undefined || selectedcontractor == null)
                resp.data.contractor[ 0 ] && setselectedcontractor(resp.data.contractor[ 0 ]._id)
        }
        let res = await axios.get(process.env.REACT_APP_DURL + 'getAllPlots/' + localStorage.getItem('pid'), options)
        if (res.status == 200) {
            setPlots(res.data);
            if (taskPlot == "" || taskPlot == undefined || taskPlot == null) {
                setTaskPlot(res.data[ 0 ]._id)
            }
        }
    }

    let _getAllTasks = async () => {
        setisloading(true)
        const options = {
            headers: {
                'Content-type': 'application/json',
                "Authorization": 'Bearer ' + localStorage.getItem('token')
            }
        };
        // console.log("🚀 ~ file: VisualProgress.js ~ line 45", process.env.REACT_APP_DURL + 'getTasksByPlot/' + localStorage.getItem('pid') + '/' + taskPlot + '/' + taskFloor)
        // let res = await axios.get(process.env.REACT_APP_DURL + 'getTasksByPlot/' + localStorage.getItem('pid') + '/' + selectedpocket + '/' + taskFloor, options)
        let res = await axios.get(process.env.REACT_APP_DURL + 'getTasksByPlot/' + localStorage.getItem('pid') + '/' + selectedpocket + '/', options)
        console.log("🚀 ~ file: VisualProgress.js ~ line 65 ~ let_getAllTasks= ~ res", res.data)
        if (res.status == 200) {
            setAllTasks(res.data);
            setisloading(false)
        }
    }

    useEffect(() => {
        getAllPlots()
        _getAllTasks()

    }, [ localStorage.getItem('pid'), selectedpocket ])


    // console.log(allTasks);

    return (
        <>
            { !isloading ? null :
                <div style={ { position: 'fixed', height: '100vh', width: '100vw', zIndex: 2, backgroundColor: '#fff' } }>
                    <video style={ { objectFit: 'contain', width: '300px', left: '50%', transform: 'translate(-50%,-50%)', top: '50%', backgroundColor: '#fff', position: 'fixed' } }
                        src={ loadingimg } autoPlay muted loop />
                </div>
            }
        <div style={ { background: '#f2f2f2', paddingTop: 50, height: '100vh' } }>

            <div style={ { display: 'flex', justifyItems: 'center', } }>
                <Link className={ styles.allTasksBtn } to="/tasks"><Button color='#0A60BD'>See All Tasks</Button></Link>
                <div style={ {
                    background: '#fff', boxShadow: "6px 13px 20px #0000003f",
                    display: 'flex', flexDirection: 'column',
                    padding: 10, borderRadius: 10, marginTop: '20px',
                    marginRight: '70px', marginLeft: '20px'
                } }>
                    <div>
                        <i style={ { color: '#727272' } } class="fa fa-filter"></i>
                        <span>&nbsp; Filters:</span>
                    </div>
                    <div style={ { width: '250px' } }>
                        <p style={ { marginTop: 10 } }>Select Pocket</p>
                        <select style={ { width: '100%', padding: 10, borderWidth: 2, borderRadius: 6, borderColor: '#0A60BD' } } className={ styles.dropdownContainer } onChange={ (e) => {
                            console.log("🚀 ~ file: VisualProgress.js ~ line 151pockt ~ VisualProgress ~ e", e.target.value)
                            setselectedpocket(e.target.value)
                        } } name="priority">
                            { pockets.map(i => (
                                // i.plotId.includes(taskPlot) ?
                                <option label={ i.name }
                                    value={ i._id }>{ i.name }
                                </option>
                                // : null
                            ))
                            }
                        </select>
                        <p>Select Plot</p>
                        <select style={ { width: '100%', padding: 10, borderWidth: 2, borderRadius: 6, borderColor: '#0A60BD' } } className={ styles.dropdownContainer } onChange={ (e) => {
                            console.log("🚀 ~ file: VisualProgress.js ~ line 151 ~ VisualProgress ~ e", e.target.value)
                            setTaskPlot(e.target.value)
                        } } name="priority">
                            <option value="" >Plots filter... </option>
                            { plots.map(i => (
                                i.pocketId.includes(selectedpocket) ?
                                    <option label={ i.name + ' (' +
                                        allTasks.filter(res => res.plotId.includes(i._id)
                                        ).length + ')' }
                                        value={ i._id }>{ i.name }</option>
                                    : null
                            ))
                            }
                        </select>
                    </div>


                    <div style={ {} }>
                        <p style={ { marginTop: 10 } }>Select Contractor</p>
                        { contractor.map(i => (
                            i.plotId.includes(taskPlot) ?
                                <>
                                    <div style={ { display: 'flex', justifyContent: 'space-between' } }>
                                        <div style={ { display: 'flex', alignItems: 'center' } }>
                                            <input id='con' name='con' onClick={ () => setselectedcontractor(i._id) } type='radio' />
                                            <p style={ { marginLeft: 10 } }>{ i.name }</p>
                                        </div>
                                        <p>{ ' (' +
                                            allTasks.filter(res => res.contractorId.includes(i._id) &&
                                                res.plotId.includes(taskPlot)
                                            ).length + ')' }</p>
                                    </div>
                                </>
                                : null
                        ))
                        }
                    </div>
                    <p style={ { marginTop: 10 } }>Select Building</p>
                    <select style={ { width: '100%', maxWidth: '250px', padding: 10, borderWidth: 2, borderRadius: 6, borderColor: '#0A60BD' } } className={ styles.dropdownContainer } onChange={ (e) => {
                    console.log("🚀 ~ file: VisualProgress.js ~ line 151 ~ VisualProgress ~ e", e.target.value)
                    setselectedBuilding(e.target.value)
                } } name="priority">
                    <option value="" >Building Filter...</option>
                    { buildingtype.map(i => (
                        i.contractorId.includes(selectedcontractor) ?
                            <option label={ i.name + ' (' + allTasks.filter(
                                res => res.buildingtypeId.includes(i._id) &&
                                    res.contractorId.includes(selectedcontractor) && 
                                    res.plotId.includes(taskPlot)

                            ).length + ')' } value={ i._id }>{ i.name }</option>
                            : null
                    ))
                    }
                </select>


                </div>
                <div style={ { display: 'flex', justifyContent: 'center', flexDirection: 'column', alignItems: 'center' } }>
                    <div style={ { display: 'flex', marginTop: 20, flexDirection: 'column-reverse' } } className={ styles.layers }>
                        { floors.map((i, index) => {
                            return <div onClick={ () => {
                                setTaskFloor(i._id)
                                if (taskFloor == i._id) {
                                    setTaskFloor()
                                }
                            } } style={ { backgroundColor: colors[ index ], cursor: 'pointer' } }>
                                <p style={ { color: taskFloor == i._id ? '#0a60bd' : '#000' } }
                                >{ i.name }</p>
                            </div>

                        }) }

                    </div>
                </div>
                <div className={ styles.tasks }>
                    <div className={ styles.tasksHeading }>
                        <h2 style={ { fontWeight: 'bold', minWidth: '300px', width: '300px', } }>Work Progress</h2>
                        <Link to='/tasks'><Button color="#0A60BD">All Tasks</Button></Link>
                    </div>
                    { isloading ? <div >

                        <div className={ styles.skeleton }>
                            <SkeletonTheme height={ 17 } borderRadius={ 3 } baseColor="#EAEDF0" highlightColor="#C7C7C7">
                                <Skeleton count={ 5 } />
                            </SkeletonTheme>
                        </div>
                        <div className={ styles.skeleton }>
                            <SkeletonTheme height={ 17 } borderRadius={ 3 } baseColor="#EAEDF0" highlightColor="#C7C7C7">
                                <Skeleton count={ 5 } />
                            </SkeletonTheme>
                        </div>
                        <div className={ styles.skeleton }>
                            <SkeletonTheme height={ 17 } borderRadius={ 3 } baseColor="#EAEDF0" highlightColor="#C7C7C7">
                                <Skeleton count={ 5 } />
                            </SkeletonTheme>
                        </div>
                    </div> :
                        <div style={ { marginBottom: 70, } }>
                            <div style={ { minWidth: '300px', width: '300px', marginBottom: 10, display: 'flex', paddingRight: 10 } }>
                                { tasktypes.map(i => (
                                    <button style={ { borderRadius: 100, background: filter == i.name ? '#171717' : '#A0A0A0', color: '#FFF' } }
                                        onClick={ () => {
                                            console.log("line 324", i._id);
                                            setselectedtasktype(i._id);
                                            setfilter(i.name)
                                        } }>
                                        { i.name }
                                    </button>
                                )) }
                            </div>
                            { allTasks.map(i => (
                                (selectedcontractor != "" && selectedtasktype != "" && selectedBuilding != "" &&
                                    selectedpocket != "") && (i.contractorId.includes(selectedcontractor) &&
                                        i.tasktypeId.includes(selectedtasktype) &&
                                        i.plotId.includes(taskPlot) &&
                                        i.floorId.includes(taskFloor) &&
                                        i.buildingtypeId.includes(selectedBuilding) &&
                                        i.pocketId.includes(selectedpocket)) ?
                                <div className={ styles.task }>
                                    { console.log("🚀 ~ file: VisualProgress.js ~ line 181", (i.pocketId == selectedpocket || (selectedpocket == null || selectedpocket == undefined || selectedpocket == ""))) }
                                    <h2>{ i.name }</h2>

                                    {/* <h2>tasktype:{ i.tasktypeId }</h2>
                                    <h2>buildingtype :{ i.buildingtypeId }</h2>
                                    <h2>pcoket :{ i.pocketId }</h2> */}

                                    <div>
                                        { isNaN(Math.abs(moment(i.actualEndDate, 'D-MMM-YY'))) ?
                                            <h3 style={ { color: '#D53939', fontWeight: '500' } }>
                                                DELAYED: { isNaN(Math.abs(moment(i.plannedEndDate, 'D-MMM-YY').diff(moment(i.endDate[ 0 ], 'D-MMM-YY'), 'days'))) ? '0' : Math.abs(moment(i.plannedEndDate, 'D-MMM-YY').diff(moment(i.endDate[ 0 ], 'D-MMM-YY'), 'days')) } days
                                            </h3>
                                            : <h3 style={ { color: '#D53939', fontWeight: '500' } }>
                                                DELAYED: { isNaN(Math.abs(moment(i.plannedEndDate, 'D-MMM-YY').diff(moment(i.actualEndDate, 'D-MMM-YY'), 'days'))) ? '0' : Math.abs(moment(i.plannedEndDate, 'D-MMM-YY').diff(moment(i.actualEndDate, 'D-MMM-YY'), 'days')) } days
                                            </h3> }
                                        <p style={ { textTransform: 'lowercase' } }>{ i.status == 'END' ? 'Completed' : i.status }</p>
                                    </div>

                                    <div style={ { display: 'flex', gap: 10, justifyContent: 'space-between' } }>
                                        <div>
                                            <p>Planned Finish:</p><p style={ { fontSize: 16, fontWeight: 'bold' } }>{ i.plannedEndDate && i.plannedEndDate !== null && i.plannedEndDate }</p>
                                        </div>
                                        <div>
                                            <p>Revised Planned Finish:</p><p style={ { fontSize: 16, fontWeight: 'bold' } }>{ i.endDate[ 0 ] && i.endDate[ 0 ] !== null && i.endDate[ 0 ] }</p>
                                        </div>
                                        <div>
                                            <p>Actual Finish: </p><p style={ { fontSize: 16, fontWeight: 'bold' } }>{ i.actualEndDate && i.actualEndDate !== null && i.actualEndDate }</p>
                                        </div>
                                    </div>


                                </div>
                                    : null)) }
                        </div>
                    }

                </div>
            </div>
        </div>
        </>
    )
}

export default VisualProgress