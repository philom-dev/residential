import styles from "./Module.module.css"
import pendinglogo from "../resources/icons/pendingissues.svg"
import { CircularProgressbar, buildStyles } from 'react-circular-progressbar';
import 'react-circular-progressbar/dist/styles.css';
import { useEffect, useState, useContext} from "react";
import changeNumberFormat from "./CostFormat";
import axios from "axios";
import SelectedProjectContext from '../context/selectdProjectContext';
import Dropdown from 'react-dropdown';
import {Card} from "./Card.style"
import {Button} from "./Button.style"
import {CostFormatCommas} from "./CostFormatCommas"
import { BarChart, Bar, Cell, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer } from 'recharts';

function Module() {

    const [allModules, setAllModules] = useState([])
    const [selectedModule, setSelectedModule] = useState('')

    const [plannedCost, setPlannedCost] = useState('');
    const [actualCost, setActualCost] = useState('');

    const { SelectedProject, setSelectedProject } = useContext(SelectedProjectContext)


    let getModuleList = async () => {
        let res = await axios.get(process.env.REACT_APP_DURL + 'getAllModules/' + SelectedProject);
        if(res.status == 200){
            setAllModules(res.data);
        }
    }

    let getModuleById = async (id) =>{
        let res = await axios.get(process.env.REACT_APP_DURL + 'getModuleById/' + id);
        if(res.status == 200){
            setSelectedModule(res.data)
        }
    }

    let sumActualCost = (input) => {
        let total = 0
        input.map(i => {
            total += parseInt(i["TotalCost"])

        })
        return total
    }

    let profit = (input) => {
        let totalactual = 0
        let totalplanned = 0
        input["ActualCost"].map(i => {
            totalactual += parseInt(i["TotalCost"])
           })
        input["PlannedCost"].map(i => {
            totalplanned += parseInt(i["TotalCost"])
         })
        return (totalplanned - totalactual)
    }

    let sumQuantity = (input) => {
        return input.total
    }

    let workProgress = () => {
        let totalQty = 0
        let actualQty = 0
        selectedModule && selectedModule.map(i => {
            totalQty += parseInt(i["quantity"])
        })
        selectedModule && selectedModule.map(i => {
            actualQty += parseInt(i["workdone"][0]["total"])

        })
        return (actualQty/totalQty)*100
    }

    let getIssuesAndExpenses = () => {
        let totalIssues = []
        let totalExpenses = []
        selectedModule && selectedModule.map(i => {
            i.issues.map(j => {
                if(j.status == "Pending"){
                    totalIssues.push(j)
                }
            })
            i.expenses.map(k => {
                if(k.status == 'Pending'){
                    totalExpenses.push(k)
                }
            })
        })
        return totalIssues.length + totalExpenses.length
    }

    let getTotalCost = () => {
        let totalactual = 0
        let totalplanned = 0

        selectedModule && selectedModule.map(i => {
            i.workdone[0].ActualCost.map(actual => {
                if(actual.TotalCost){
                totalactual += parseInt(actual.TotalCost)
                }
            })
            i.workdone[0].PlannedCost.map(planned => {
                if(planned.TotalCost){
                totalplanned += parseInt(planned.TotalCost)
                }
            })
        })
        setPlannedCost(totalplanned)
        setActualCost(totalactual)
    }

    let getChartData = () => {
        let arr = []
        selectedModule && selectedModule.map(i => {
            let planned = 0
            let actual= 0
            i.workdone[0].PlannedCost.map(p => {
                if(p.TotalCost){
                planned += p.TotalCost
                }
            })
            i.workdone[0].ActualCost.map(p => {
                if(p.TotalCost) {
                actual += p.TotalCost
                }
            })

            let m = {}
            m.name = i.taskname
            m.Planned = planned
            m.Actual = actual
            arr.push(m)
        })
        return arr;
    }

    useEffect(()=>{
        getModuleList();
        getIssuesAndExpenses()
        getTotalCost();
       
    },[selectedModule, SelectedProject])

    return <div className={styles.module}>

        <div className={styles.dropdownContainer}>
        <Dropdown arrowClassName={styles.dropdownArrow} menuClassName={styles.dropdownMenu} controlClassName={styles.dropdown} 
           onChange={(e)=>getModuleById(e.value)}
           options={allModules.map(i => {
               return {
                   value: i._id,
                   label: i.name
               }
           })} 
          value='Select a module...' />
        </div>

       {selectedModule && <div>
       <div className={styles.heading}>
            <Card color="#EAEDF0" className={styles.progress}>
                <div>
                <CircularProgressbar value={workProgress()} text={`${workProgress()}%`} styles={buildStyles({pathColor:'#2AC371',textColor:'#2AC371',trailColor:'#EAEDF0'})}/> 
                </div>
                <p>Work Progress</p>
            </Card>
            <Card color="#EAEDF0" className={styles.pending}>
                <p><img src={pendinglogo}/></p>
                <p><span>{getIssuesAndExpenses()}</span> &nbsp; Pending Issues & Expenses</p>
            </Card>
        </div>

        <Card className={styles.main}>  
            <div className={styles.cost}>
                <Card shadow='none' border='2px solid #22242E'>
                    <h3>Planned Cost</h3>
                    <h2><span className="rupee">₹</span>{CostFormatCommas(parseInt(plannedCost))}</h2>
                </Card>
                <Card shadow='none' border='2px solid #0A60BD'>
                    <h3>Actual Cost</h3>
                    <h2><span className="rupee">₹</span>{CostFormatCommas(parseInt(actualCost))}</h2>
                </Card>
                <Card shadow='none' color={Math.abs(plannedCost - actualCost) >= 0 ? '#2AC371' : '#D53539'}>
                    <h3>{Math.abs(plannedCost - actualCost) >= 0 ? 'Profit' : 'Loss'}</h3>
                    <h2><span className="rupee">₹</span>{Math.abs(plannedCost - actualCost)}</h2>
                </Card>
            </div>
            <div className={styles.chart}>
            <BarChart
            width={900}
            height={300}
            data={getChartData()}
            margin={{
            top: 5,
            right: 30,
            left: 20,
            bottom: 5,
          }}
        >
          <CartesianGrid strokeDasharray="3 3" />
          <XAxis dataKey="name" />
          <YAxis />
          <Tooltip />
          <Legend />
          <Bar dataKey="Planned" fill="#8884d8" />
          <Bar dataKey="Actual" fill="#82ca9d" />
        </BarChart>
            </div>
        </Card>

        <div className={styles.table}>
            <div className={styles.tableHeader}>
                <p>Task Name</p>
                <p>Actual Cost</p>
                <p>Profit/Loss</p>
                <p>Work Done</p>
            </div>
            <div>
            {selectedModule && selectedModule.map((i,index) => {
                    return <div className={styles.tableRow} key={index}>
                        <p>{i.taskname}</p>
                        <p>{sumActualCost(i.workdone[0].ActualCost)}</p>
                        <p>{profit(i.workdone[0])}</p>
                        <p>{sumQuantity(i.workdone[0])}</p>
                    </div>
                    })}
            </div>
        </div>
       </div>}
    </div>
}

export default Module