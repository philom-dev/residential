import { useState, useEffect, useContext } from "react";
import styles from "./CreateTask.module.css";
import moment from "moment";
import axios from "axios";
import Dropdown from 'react-dropdown';
import logo from "../logo/loader.gif"
import excellogo from "../resources/icons/excellogo.svg"
import downloadlogo from "../resources/icons/Download.svg";
import ReactTooltip from 'react-tooltip';
import SelectedProjectContext from "../context/selectdProjectContext";
import AllprojectsContext from '../context/AllprojectsContext';
import { analytics } from "./Analytics"
import { logEvent } from "@firebase/analytics";
import XLSX from "xlsx";
import { useTheme } from "styled-components";
import { Steps, Hints } from 'intro.js-react';
import { useHistory } from "react-router";
import { Card } from "./Card.style"
import { Button } from "./Button.style"

function CreateTask(props){
    let [taskname, setTaskname] = useState('');
    let [plannedcost, setPlannedcost] = useState('');
    let [quantity,setQuantity] = useState('');
    let [rate, setRate] = useState('');
    let [unit, setUnit] = useState('sqm')
    let [numbers, setNumbers] = useState([]);
    let [ PrevNumbers, setPrevNumbers ] = useState([]);
    let [startdate, setStartdate] = useState('');
    let [enddate, setEnddate] = useState('');
    let [number, setNumber] = useState('');
    const [success, setSuccess] = useState(false)
    const [loader, setLoader] = useState(false)
    const [items, setItems] = useState([])
    const [bulkCreated, setBulkCreated] = useState(false)
    const [creationMessage, setCreationMessage] = useState(false)
    const [taskType, setTaskType] = useState('')
    const [taskStructure, setTaskStructure] = useState('')
    const [showMore, setShowMore] = useState(false)
    const {SelectedProject, setSelectedProject} = useContext(SelectedProjectContext)
    const { AllProjects } = useContext(AllprojectsContext)
    const [plots,setPlots] = useState([])
    const [plot, setPlot] = useState('')
    const [floor, setFloor] = useState('')
    
    const [tutorial, setTutorial] = useState(localStorage.getItem('tasktutorial') ? false : true)
    let history = useHistory()

    let steps = [
        {
            intro: "<h1>Lets create a task!</h1><p>Please follow through all the steps to see how to create a task.</p>",
            tooltipClass: 'welcomemsg',
        },
        {
            element: "#format",
            intro:"<h1>Download format</h1><p>Click on this button to download the format of the excel through tasks can be created in bulk!</p>",
            tooltipClass: 'tooltip'
        },
        
        {
            element: "#uploadexcel",
            intro:"<h1>Upload Excel</h1><p>Click this button to upload an excel file containing all the details of the tasks in the format shown in the previous step.</p>",
            tooltipClass: 'tooltip',
            position:'left'
        },
        {
            element: "#taskname",
            intro:"<h1>Enter Task Name</h1><p>Enter the name of the task you wish to create. For examlpe, we have set up a task named Test Task for you.</p>",
            tooltipClass: 'tooltip',
            position:'bottom'
        },
        {
            element: "#taskunit",
            intro:"<h1>Enter Task Unit</h1><p>Enter the unit of the task. You can select any unit from this dropdown menu.</p>",
            tooltipClass: 'tooltip',
            position:'bottom'
        },
        {
            element: "#startdate",
            intro:"<h1>Select Start Date</h1><p>Select the date on which the task is to be started.</p>",
            tooltipClass: 'tooltip',
            position:'bottom'
        },
        {
            element: "#enddate",
            intro:"<h1>Select End Date</h1><p>Select the date on which the task is to be ended.</p>",
            tooltipClass: 'tooltip',
            position:'bottom'
        },
        {
            element: "#addMoreDetails",
            intro:"<h1>Show More Details</h1><p>Click on this buttom to add more details to your task.</p>",
            tooltipClass: 'tooltip',
            position:'top'
        },
        
        showMore ? {
            intro:"<p>Enter all the details of the task.</p>",
            tooltipClass: 'tooltip',
            // position: 'top'
        } : {
            element: "#submitbtn",
            intro:"<p>Click on this button to create a task!</p>",
            tooltipClass: 'tooltip',
        } 
        
    ]

    const [UnitArr, setUnitArr] = useState([
        {'label': 'Cubic Feet', 'value': 'CubicFeet'},
        {'label': 'Cubic Meter','value': 'cum'},
        {'label': 'Feet', 'value': 'Feet'},
        {'label': 'Hectare', 'value': 'Hectare'},
        {'label': 'Kilo gram','value': 'Kg'},
        {'label': 'Kilo Meter','value': 'km'},
        { 'label': 'Kiloliter', 'value': 'Kiloliter' },
        { 'label': 'Liter', 'value': 'Liter' },
        {'label': 'Meter', 'value': 'Meter'},
        {'label': 'Metric Tonne', 'value': 'MT'},
        {'label': 'Number','value': 'Number'},
        {'label': 'Running Meter', 'value': 'RMT'},
        {'label': 'Square Meter','value': 'sqm'},
        {'label': 'Square Feet', 'value': 'SquareFeet'},
        {'label': 'Tonne', 'value': 'Tonne'},
    ])

    const Structure = [ "Box Culvert", "Slab Culvert", "Minor Bridge",
        "Major Bridge", "Interchange", "Toe Wall", "VUP", "SVUP", "LVUP"
    ]

    let addNumberHandler = (e) => {
        e.preventDefault();
        if(number.length == 10){
        setNumbers([...numbers, number])
        setNumber('')
        }
    }

    const getAllPlots = async () => {
        const options = {
            headers: {
                'Content-type': 'application/json',
                "Authorization": 'Bearer ' + localStorage.getItem('token')
            }
        };
        let res = await axios.get(process.env.REACT_APP_DURL + 'getAllPlots/' + localStorage.getItem('pid'),options)
        if(res.status == 200) {
            setPlots(res.data);
        }
    }
        
    let costCalc = (e) => {
        if(e.target.value !== ''){
            setPlannedcost((parseFloat(quantity)*parseFloat(rate)).toFixed(2))
        } else {
            setPlannedcost('')
        }
    }

    let submitHandler = async (e)=>{
        e.preventDefault()
        if(taskname == '' || startdate == '' || enddate == ''){
            console.log("field cannot be empty");
        } else {
        setLoader(true)
        let data = {
         taskname: taskname,
         plannedcost: plannedcost,
         projectId: localStorage.getItem('pid'),
         numbers: numbers,
         enddate: moment(enddate).format('YYYY-MM-DD'),
         startdate: moment(startdate).format('YYYY-MM-DD'),
         unit: unit,
         quantity: quantity,
         rate: rate,
         typeoftask: taskType == '' ? 'Miscellaneous' : taskType,
         typeofstructure: taskStructure,
         plot: plot,
         floor: floor

        }
        const options = {
            headers: {
                'Content-type': 'application/json',
                "Authorization": 'Bearer ' + localStorage.getItem('token')
            }
        };
        let res = await axios.post(process.env.REACT_APP_DURL + 'createtask',data,options)
        if(res.status === 200){

            // analytics -----
            const params = {
                Project_wise: localStorage.getItem('pid'),
                User_wise: localStorage.getItem('userid'),
                projectName: SelectedProject !== undefined ? AllProjects.find(i => i._id == SelectedProject).Name : '',
                userName: JSON.parse(localStorage.getItem('user')).Name,
                TodaysDate: moment().format('DD-MM-YY hh:mm:ss A'),
                SlectedDate: '',
                taskid: localStorage.getItem('tid') || '',
                taskname: ''
            }
            logEvent(analytics, "task_created_dashboard",params)
            // ---------------

            setSuccess(true)
            setLoader(false)
            setTimeout(()=>{
                setSuccess(false)
            },5000)
            setTaskname('')
            setNumbers([])
            setNumber('');
            setStartdate('');
            setEnddate('')
            setQuantity('');
            setRate('')
            setPlannedcost('')
            setUnit('sqm')
            // if(tutorial) window.location.href = 'dpr'
        }
      }
      }

      let _getDetails = async () => {
        if(props.match.params.id){
            const options = {
                headers: {
                    'Content-type': 'application/json',
                    "Authorization": 'Bearer ' + localStorage.getItem('token')
                }
            };
            let res = await axios.get(process.env.REACT_APP_DURL + 'getTaskuserlist/' + localStorage.getItem('tid'), options)
            res.data.map(i => setPrevNumbers((n) => [ ...n, i.mobileNumber ]))
             
        } else {
            setPrevNumbers([])
        }
      }

    let editHandler = async (e) => {
        e.preventDefault();
        const data0 = {
            numbers: numbers,
            taskId: props.match.params.id, 
        }
          const data = {
              numbers: numbers,
              taskId: props.match.params.id,
              projectId: localStorage.getItem('pid'),
              taskname: taskname,
              typeoftask: taskType,
              typeofstructure: taskStructure
          }
          const options = {
              headers: {
                  "Content-type": "application/json",
                  "Authorization": 'Bearer ' + localStorage.getItem('token')
              }
          };
        let res0 = await axios.put(process.env.REACT_APP_DURL + 'addusertotask',data0, options)
        let res = await axios.put(process.env.REACT_APP_DURL + 'updatetask',data,options);
        if(res.status == 200){
            setSuccess(true)
            setTimeout(() => {
                setSuccess(false)
            }, 1000);   
          
        }
    }

      let renderSuccessMessage = () => {
          return props.match.params.id ? <div className={ styles.success }>Updated successfully</div>
              : <div className={ styles.success }>Task created successfully. Go to home screen and then task screen to view your tasks</div>
      }

      const _getTaskDetails = async () => {
      if(props.match.params.id){
        const options = {
            headers: {
                'Content-type': 'application/json',
                "Authorization": 'Bearer ' + localStorage.getItem('token')
            }
        };
        const res = await axios.get(process.env.REACT_APP_DURL + 'gettaskdetails/' + props.match.params.id, options)
        if(res.status == 200){
            setTaskname(res.data[0].name)
            setStartdate(moment(res.data[0].startDate).format('YYYY-MM-DD'))
            setEnddate(moment(res.data[0].endDate).format('YYYY-MM-DD'))
            setQuantity(res.data[0].quantity)
            setPlannedcost(res.data[0].PlanedCost)
            setRate(res.data[0].rate)
            setUnit(res.data[0].unit)
        }
      } else{
          setTaskname('')
          setStartdate('')
          setEnddate('')
          setPlannedcost('')
          setQuantity('')
          setRate('')
          setUnit('sqm')
          setNumbers([])
      }
      }

      useEffect(()=>{
          getAllPlots()
      },[localStorage.getItem('pid')])

      useEffect(()=>{
          _getTaskDetails();
      },[props.match.params.id])

      useEffect(()=>{
          _getDetails();
      },[])

      useEffect(()=>{
          if(tutorial){
              setTaskname('Test task')
              setNumbers([JSON.parse(localStorage.getItem('user')).mobileNumber])
              setStartdate()
              setEnddate()
              setTaskType('Highway')
              setTaskStructure()
              setQuantity('10')
              setRate('20')
              setPlannedcost('200')
             }
      },[tutorial])

      let createUsingExcel = async (input) => {
          setCreationMessage(true)
            input.map(async (i,index) => {
                let data = {
                    taskname: i['Task Name'] || '',
                    projectId: localStorage.getItem('pid'),
                    numbers: i['Users contact no. (10 digits - Use comma and space to separate numbers)'] ? i['Users contact no. (10 digits - Use comma and space to separate numbers)'].toString().length == 10 ? i['Users contact no. (10 digits - Use comma and space to separate numbers)'].toString().split() : i['Users contact no. (10 digits - Use comma and space to separate numbers)'].split(', ') : [],
                    enddate: i['End Date'] || '',
                    // startdate: i['Start Date (YYYY-MM-DD)'] || '',
                    unit: i['Unit'] || '',
                    quantity: parseFloat(i['Scope (Qty)']) || parseFloat(i[' Scope (Qty) ']) || '',
                    rate: parseFloat(i['Rate']) || parseFloat(i[' Rate ']) || '',
                    typeoftask: i['Task type'] || 'Miscellaneous',
                    typeofstructure: i['Structure type'] || '',
                    plot: i['Plot No.'] || '',
                    floor: i['Floor'] || '',
                    contractor: i['Contractor'] || 'ARYAN INFRAHEIGHT'
                   }
                   const options = {
                    headers: {
                        'Content-type': 'application/json',
                        "Authorization": 'Bearer ' + localStorage.getItem('token')
                    }
                };
                let res = await axios.post(process.env.REACT_APP_DURL + 'createtask', data, options)
                if (res.status == 200 && index == input?.length - 1) {
                    setBulkCreated(true)
                    setCreationMessage(false)
                    history.push('/tasks')
                    setTimeout(() => {
                        setBulkCreated(false)
                    }, 3000)
                }
            })
        }

         let readExcel = async (file) => {
          const fileReader = new FileReader();
          fileReader.readAsArrayBuffer(file)
          fileReader.onload = async (e) => {
              const bufferArray = e.target.result;
              const wb = XLSX.read(bufferArray, {type:'buffer'})
              const sheetname = wb.SheetNames[0];
              const sheet = wb.Sheets[sheetname];
              const data = XLSX.utils.sheet_to_json(sheet,{raw:false})
            let exceldata = {
                data
            }
            let res = await axios.post(process.env.REACT_APP_DURL + 'createTasksExcel', exceldata)
        }
      }

      let renderSteps = () => {
        return <Steps
        enabled={true}
        steps={steps}
        initialStep={0}
        onComplete={()=>window.location.href='dpr'}
        onExit={()=>{
            // setTutorial(false)
            // setTutorial(true)
            localStorage.setItem('tasktutorial','finished')
            window.location.href = 'createTask'
        }}
        options={{
            // hideNext: true,
            skipLabel:'Skip',
            showProgress: true,
            showButtons:true,
            showStepNumbers: true,
            exitOnOverlayClick:false,
            disableInteraction: false
        }}
        />
      }
    
    return <div className={styles.container}>
     <ReactTooltip />
     {tutorial && renderSteps()}
      <div className={styles.header}>
      <h1 className={styles.pageHeading}>Create New Task</h1>
      <div className={styles.bulkCreation}>
        <h2>Bulk creation</h2>
        <label id='uploadexcel' className="custom-file-upload">
            <input  type="file" onInput={(e)=> {
                            if (e.target.files[ 0 ].type == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet') {
                                const file = e.target.files[ 0 ];
                                readExcel(file);
                            }
            }}/>
           <img src={excellogo} /> Upload Excel
            </label>
            <a id='format' data-effect='solid' data-class={styles.toolclass} data-place='bottom' data-tip="Download Excel Format" className={styles.downloadFormat} href="https://storagetemlin.s3.ap-south-1.amazonaws.com/DashboardTemlin/Create+task+format.xlsx" download><img src={downloadlogo} /></a>
        {bulkCreated ? <p>Tasks Created!</p> : creationMessage ? <p style={{color:'gray',fontSize:'1rem'}}>Creating.. Please wait..</p> : ''}
        </div>
    </div>

    <Card>
    <form id='form'>
    <div id='taskname' className={styles.formGroup}>
        <label>Task Name<span>*</span></label>
        <input maxLength='35' value={taskname} onChange={(e)=>setTaskname(e.target.value)} placeholder="Task Name" type='text' autoComplete='off'/>
    </div>

    <div id='taskunit' className={styles.formGroup}>
        <label>Unit<span>*</span></label>

        <div className={styles.dropdownContainer}>
        <Dropdown arrowClassName={styles.dropdownArrow}  menuClassName={styles.dropdownMenu} controlClassName={styles.unitDropdown} onChange={(e)=>setUnit(e.value)} options={UnitArr.map(unit => {
           return {
            value: unit.value,
            label: unit.label }})} value={unit} />
        </div>
        </div>

        <div className={styles.formGroup}>
        <label>Plot<span>*</span></label>

        <div className={styles.dropdownContainer}>
        <Dropdown arrowClassName={styles.dropdownArrow}  menuClassName={styles.dropdownMenu} controlClassName={styles.unitDropdown} onChange={(e)=>setPlot(e.value)} options={plots.map(i => {
           return {
            value: i._id,
            label: i.name }})} value="Select Plot" />
        </div>
        </div>

        <div className={styles.formGroup}>
        <label>Floor<span>*</span></label>

        <div className={styles.dropdownContainer}>
        <Dropdown arrowClassName={styles.dropdownArrow}  menuClassName={styles.dropdownMenu} controlClassName={styles.unitDropdown} onChange={(e)=>setFloor(e.value)} 
        options={['BASEMENT','STILT','1st ROOF','2nd ROOF','3rd ROOF','4th ROOF']} value="Select Floor" />
        </div>
        </div>

    <div id='startdate' className={styles.formGroup}>
        <label>Start Date<span>*</span></label>
        <input value={startdate} onChange={(e)=>setStartdate(e.target.value)} id='startDate' type='date' autoComplete='off'/>
    </div> 
    <div id='enddate' className={styles.formGroup}>
        <label>End Date<span>*</span></label>
        <input value={enddate} onChange={(e)=>setEnddate(e.target.value)} id='endDate' type='date' autoComplete='off' placeholder='End date'/>
    </div> 

    <button  onClick={(e)=>{
        e.preventDefault();
        setShowMore(!showMore)
    }} className={styles.addMoreDetails}>{showMore ? <i className="fas fa-chevron-down"></i> : <i className="fas fa-chevron-right"></i>} <p id='addMoreDetails'>Add more details</p></button>

     {showMore && <div>

    <div className={styles.formGroup}>
        <label>Add Users</label>

        <div>
        <div style={{position:'relative'}}>
        <input  value={number} onChange={(e)=>setNumber(e.target.value)} minLength='10' maxLength='10' type='tel' autoComplete='off' placeholder='Phone Number'/>
        <button onClick={addNumberHandler} className={styles.addNumber}><i className="fas fa-plus"></i></button>
        </div>
        <div id='addedNumbers' style={{display:'flex',flexWrap:'wrap'}}>
                        {PrevNumbers.map(number => {
                            return <div className={styles.addedNumber}>{number}</div>
                        })}
        {numbers.map(number => {
            return <div className={styles.addedNumber}>{number}</div> 
           })}
              
        </div>
        </div>

        
  </div>

    <div className={styles.formGroup}>
        <label>Task Type</label>
        <div className={styles.dropdownContainer}>
        <Dropdown arrowClassName={styles.dropdownArrow} menuClassName={styles.dropdownMenu} controlClassName={styles.unitDropdown} onChange={(e)=>setTaskType(e.value)} 
           options={[{value:'Highway',label:'Highway'},{value:'Structure',label:'Structure'},{value:'Miscellaneous',label:'Miscellaneous'}]} value={taskType == '' ? 'Select Task Type' : taskType} />
        </div>
        </div>    
     <div className={styles.formGroup}>
        <label>Task Structure</label>
        <div className={styles.dropdownContainer}>
        <Dropdown arrowClassName={styles.dropdownArrow} menuClassName={styles.dropdownMenu} controlClassName={styles.unitDropdown} onChange={(e)=>setTaskStructure(e.value)} 
           options={Structure.map(i => {
               return i
           })} value={taskStructure == '' ? 'Select Task Structure' : taskStructure} />
        </div>
    </div>  

    <div className={styles.formGroup}>
        <label>Total Quantity</label>
        <input value={quantity} onChange={(e)=>setQuantity(e.target.value)} type='text' autoComplete='off' placeholder='Quantity'/>
    </div> 
    <div className={styles.formGroup}>
        <label>Rate <span className="rupee" style={{color:'#22242E'}}>(₹)</span></label>
        <input value={rate} onKeyUp={costCalc} onChange={(e)=>setRate(e.target.value)} type='text' autoComplete='off' placeholder='Rate'/>
    </div> 
    <div className={styles.formGroup}>
        <label>Planned Cost</label>
        <input value={plannedcost} onChange={(e)=>setPlannedcost(e.target.value)} type='text' autoComplete='off' placeholder='Planned Cost'/>
    </div> 
     </div>}

     <div style={{display:'flex',alignItems:'center',marginTop:'10px'}}>  
    <Button color="#0A60BD" id="submitbtn" onClick={props.match.params.id ? editHandler : submitHandler}>{props.buttonName}</Button>          
    {success ? renderSuccessMessage() : loader ? <img className={styles.loader} src = {logo}/> : ''}  
    </div> 

    </form>
    </Card>

    </div>
}

export default CreateTask