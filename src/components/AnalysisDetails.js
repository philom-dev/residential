import styles from "./AnalysisDetails.module.css"
// import layer from "../resources/layer.png"
import changeNumberFormat from "./CostFormat"
import { useState, useEffect, useContext } from "react"
import axios from "axios";
import SelectedProjectContext from "../context/selectdProjectContext";
import { Steps, Hints } from 'intro.js-react';
import { useHistory } from "react-router";

function AnalysisDetails(props) {

    // let history = useHistory()
    let steps = [
        {
            element: '#selectedtask',
            intro: '<h1>Select Task</h1><p>Select a task to see the chainages of that particulater task.</p>',
            tooltipClass: 'tooltip'
        },
        {
            element: '#row',
            intro: '<p>Here are the details of the chainages of the selected task.</p>',
            tooltipClass: 'tooltip'
        },
        {
            intro: '<p>You are good to go!</p>',
            tooltipClass: 'tooltip'
        }
    ]

    let renderSteps = () => {
        return <Steps
      showProgress={true}
      enabled={true}
      steps={steps}
      initialStep={0}
      onComplete={()=>{
        localStorage.setItem('tutorial3','finished')  
        window.location.href='analysis'}}
      onExit={()=>console.log('exit')}
      options={{
        skipLabel:'Skip',
        showProgress: true,
        showButtons:true,
        showStepNumbers: true,
        disableInteraction: false,
        exitOnOverlayClick:false,
        // hideNext: true
      }}
      />
    }

    const { SelectedProject, setSelectedProject } = useContext(SelectedProjectContext)
    const [plannedCost, setPlannedCost] = useState(30)
    const [actualCost, setActualCost] = useState(20)
    const [tasks, setTasks] = useState([])
    const [selectedTask, setSelectedTask] = useState();
    const [range1, setRange1] = useState(props.location.state.from)
    const [range2, setRange2] = useState(props.location.state.to)
    const [length, setLength] = useState('')
    const [breadth, setBreadth] = useState('')
    // const [tutorial, setTutorial] = useState(true)

    let _getTasks = async () => {
        if(range1 == '' || range2 == '') return
        const options = {
            headers: {
                'Content-type': 'application/json',
                "Authorization": 'Bearer ' + localStorage.getItem('token')
            }
        };
        let data = {
            range1: +range1,
            range2: +range2
        }
        let res = await axios.post(process.env.REACT_APP_DURL + 'getRangeWorkDone/'+ SelectedProject,data,options)
        if(res.status == 200){
            setTasks(res.data.filter(i => i.taskProgress.length !== 0));
          }
    }

    let task = selectedTask && selectedTask[0].taskProgress.reduce(function (r, a) {
        r[a.Date] = r[a.Date] || [];
        r[a.Date].push(a);
        return r;
    }, Object.create(null));
    task = task && Object.entries(task)

    let getTotalWork = (input) => {
        let count = 0;
        input.map(j => {
            count += +j.TaskProgressQuantity;
        })
        return count
    }
    let setDepth = (input) => {
        let l = 0;
        let b = 0;
        input.map(i => {
            l += +i.length;
            b += +i.Breadth;
        })
        setLength(l)
        setBreadth(b)
        
    }

    useEffect(()=>{
        _getTasks();
    },[range2,range1])

    return <div className={styles.details}>

        {props.location.state.nextstep && renderSteps()}

        <div className={styles.diagram}>
            <div className={styles.layers}>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <p style={{fontWeight:'bold', position:'absolute',top:'-30px',left:'50%',transform:'translateX(-50%)'}}>Length: {length}</p>
            <p style={{fontWeight:'bold', position:'absolute',top:'50%',left:'95%',transform:'translateY(-50%) rotate(90deg)',width:'fit-content'}}>Breadth: {breadth}</p>
            </div>


            {/* not shown ---- */}
            <div  style={{visibility:'hidden'}} className={styles.cost}>
             <div style={{display:'grid',gridTemplateColumns:'46% 46%',gridColumnGap:'8%'}}>
                    <div className='planned-cost'>Planned Cost<h3>
                     ₹ { plannedCost.toString().length <= 5 ? plannedCost : changeNumberFormat(plannedCost, 2) }</h3></div>
                 
                <div className='actual-cost'>Actual Cost<h3>
                 ₹ {actualCost.toString().length <= 5 ? actualCost : changeNumberFormat(actualCost,2)}</h3></div>
                </div>
                
                <div style={{textAlign:'center'}} className={`${parseInt(actualCost) <= parseInt(plannedCost) ? 'profit-class-total' : 'loss-class-total'}`}>
                    <h3 style={{fontSize:'1.8rem'}}>
                       {parseInt(actualCost) <= parseInt(plannedCost) ? 'Profit' : 'Loss'}
                     &nbsp;₹ {isNaN(Math.abs(parseInt(actualCost) - parseInt(plannedCost))) ? '0' : Math.abs(parseInt(actualCost) - parseInt(plannedCost)).toString().length <= 5 ? Math.abs(parseInt(actualCost) - parseInt(plannedCost)) : changeNumberFormat(Math.abs(parseInt(actualCost) - parseInt(plannedCost)),2)}</h3>
                    </div>
                </div>
            {/* ----------------- */}
            </div>

        <div className={styles.taskWiseChainage}>
        <div className={styles.chainagefilter}>
                <p>Select Chainage</p>
                <input value={range1} onInput={(e)=>setRange1(e.target.value)} placeholder='From' type='number' />
                <i className="fas fa-arrows-alt-h"></i>
                <input value={range2} onInput={(e)=>setRange2(e.target.value)} placeholder='To' type='number' />
                <button onClick={_getTasks} ><i style={{fontSize:'1rem'}} className="fas fa-search"></i> View</button>
            </div>
          
            {props.location.state.nextstep && <select id="selectedtask">
                <option>Test Task</option>
            </select>}

            {!props.location.state.nextstep && <select onChange={(e)=>setSelectedTask(tasks.filter(i => i.taskId == e.target.value))}>
                <option>Select a task..</option>
                {tasks.map((task,i) => {
                    return <option key={i} value={task.taskId}>{task.name}</option>
                })}
            </select>}
            
            <table cellPadding='10' cellSpacing='0'>
                <thead >
                    <tr>
                        <th>Date</th>
                        <th>Chainage</th>
                        <th>Work Done</th>
                    </tr>
                </thead>
                <tbody>

                    {props.location.state.nextstep && <tr id='row'>
                        <td>10-12-2021</td>
                        <td>
                        <p><i className="fas fa-circle"></i> 0 - 100m</p>
                        
                        </td>
                        <td>
                        500m
                        </td>
                        </tr>}

                    {!props.location.state.nextstep && task && task.map((i,index) => {
                        return <tr onClick={()=>{
                            setDepth(i[1])
                            }} key={index}>
                        <td>{i[0]}</td>
                        <td>
                        {i[1].map((j,index) => {
                            return <p key={index}><i className="fas fa-circle"></i> {j.From} - {j.To}m</p>
                        })}
                        </td>
                        <td>
                        {Math.abs(getTotalWork(i[1]))}m
                        </td>
                        </tr>
                    })}

                    {/* {selectedTask && selectedTask[0]?.taskProgress.map((i,index) => {
                        return <tr key={index}>
                        <td>{i.Date}</td>
                        <td><p><i className="fas fa-circle"></i> {i.From} - {i.To}m</p></td>
                        <td>{i.TaskProgressQuantity}m</td>
                    </tr>
                    })} */}
                    {/* <tr>
                        <td>10/10/2021</td>
                        <td><p><i className="fas fa-circle"></i> 100 - 100m</p><i className="fas fa-circle"></i> 200 - 200m<p></p><i className="fas fa-circle"></i> 300 - 300m<p></p></td>
                        <td>280m</td>
                    </tr> */}
                </tbody>
            </table>
        </div>
    </div>
}

export default AnalysisDetails