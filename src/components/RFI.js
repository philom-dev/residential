import React, { useEffect, useState } from "react";
import styles from "./RFI.module.css"
import {CustomContainer} from "./CustomContainer.style"
import { CircularProgressbar, buildStyles } from 'react-circular-progressbar';
import 'react-circular-progressbar/dist/styles.css';
import axios from "axios";
import Dropdown from 'react-dropdown';
import updateicon from "../resources/icons/updateicon.svg"
import { analytics } from "./Analytics"
import { logEvent } from "@firebase/analytics";
import moment from "moment";
import CustomCalander from "./CustomCalendar";
import {Card} from "./Card.style"
import { Oval } from  'react-loader-spinner'

function RFI(){

    const [rfiList, setRfiList] = useState([])
    const [tasksList, setTasksList] = useState([])
    const [taskId, setTaskId] = useState('All Tasks')
    const [update, setUpdate] = useState('')
    const [rfiNumber, setRfiNumber] = useState('')
    const [rfiTime, setRfiTime] = useState('')
    const [search, setSearch] = useState('')
    const [selectedDate, setSelectedDate] = useState('')
    const [loader, setLoader] = useState(true)
    const [showDetails, setShowDetails] = useState(false)
    const [selectedData, setSelectedData] = useState('')

    let _getRfiList = async () => {
        const options = {
            headers: {
                'Content-type': 'application/json',
                "Authorization": 'Bearer ' + localStorage.getItem('token')
            }
        };
        const res = await axios.get(process.env.REACT_APP_DURL + 'getRfi/' + localStorage.getItem('pid'),options)
        if(res.status == 200) {
        let arr = []
        if(selectedDate !== ''){
            res.data.map(i => {
                if(moment(i.raisedonDate).format('DD-MM-YYYY') == selectedDate){
                     arr.push(i)
                }
            })
        } else {
            arr = res.data
        }
        setRfiList(arr);
        setLoader(false)
        }
    }

    let _updateRfi = async (id, number, time) => {
        const options = {
            headers: {
                'Content-type': 'application/json',
                "Authorization": 'Bearer ' + localStorage.getItem('token')
            }
        };
        let data = {
            rfiid: id,
            rfinumber: rfiNumber || number,
            raisedonTime: rfiTime || time
        }
        const res = await axios.put(process.env.REACT_APP_DURL + 'upadterfi' ,data, options)
        if(res.status == 200){
            console.log("RFI updated successfully!");
            setRfiNumber('')
            setRfiTime('')
            setUpdate('')
        }
    }

    let _getProjectTasks = async () => {
        const options = {
            headers: {
                'Content-type': 'application/json',
                "Authorization": 'Bearer ' + localStorage.getItem('token')
            }
        };
        const res = await axios.get(process.env.REACT_APP_DURL + 'getAllTasks/' + localStorage.getItem('pid'),options)
        if(res.status == 200){
           setTasksList(res.data)
        }
    }


    let taskFormat = () => {
        let arr = [{
            label: 'All Tasks',
            value:'All Tasks'
        }]
        tasksList.map(i => {
            arr.push({
                label: i.name,
                value: i.taskId
            })
        })
        return arr
    }

    let changeHandler = (start) => {
        setSelectedDate(moment(start).format('DD-MM-YYYY'));
   }

    useEffect(()=>{
        _getRfiList();
        _getProjectTasks();
    },[localStorage.getItem('pid'),rfiNumber,update,selectedDate])

    return loader ? <Oval wrapperClass="pageLoading" strokeWidth={4} secondaryColor='#EAEDF0' color="#0A60BD" height={50} width={50} /> :
         <div className={styles.ratelist}>
          <div className={styles.cardsheader}>

            <Card padding='10px' border="2px solid #22242E" color="#22242E">
                <h1>{rfiList.length}</h1>
                <h2>Total RFI Raised</h2>
            </Card>
            <Card padding='10px' border="2px solid #2AC371" shadow='none'>
                <h3>{rfiList.filter(i => i.status == 'Approve').length}</h3>
                <h4>Approved</h4>
            </Card>
            <Card padding='10px' border="2px solid #D53539" shadow='none'>
                <h3>{rfiList.filter(i => i.status == 'Reject').length}</h3>
                <h4>Rejected</h4>
            </Card>
            <Card padding='10px' border="2px solid #FA9601" shadow='none'>
                <h3>{rfiList.filter(i => i.status == 'Pending').length}</h3>
                <h4>Pending</h4>
            </Card>
        </div>

        <div style={{display:'grid',gridTemplateColumns:'30% 30%',justifyContent:'space-between'}}>
            <div>
                <Dropdown arrowClassName={styles.dropdownArrow} className={styles.dropdownContainer} menuClassName={styles.dropdownMenu} controlClassName={styles.dropdown}
                 onChange={(e)=>{
                   setTaskId(e.value)
                 }} options={taskFormat()} value="All Tasks"/>
            </div>
            <div style={{display:'flex',alignItems:'center'}} >
            {/* <div>sort by filter</div> */}
            <div className={styles.calendar}>
                <CustomCalander onChange={changeHandler}/>
                <h5>{selectedDate ? selectedDate : 'Select Date'}</h5>
                <div onClick={()=>setSelectedDate('')} className={styles.clear}><i className="fas fa-times fa-2x"></i></div>
            </div>
          </div>
        </div>

        <div className={styles.table}>
            <div className={styles.tableHeader}>
                <p>RFI No.</p>
                <p>Task Name</p>
                <p>From</p>
                <p>To</p>
                <p>Side</p>
                <p>Task Type</p>
                <p>Raised Date</p>
                <p>Raised Time</p>
                <p>Status</p>
                <p>Edit</p>
            </div>
            <div className={styles.tableBody}>

                {rfiList.map(i => {
                    console.log(i.status);
                    return taskId == i.taskId[0]._id ? <div onClick={()=>{
                        setShowDetails(true)
                        setSelectedData(i._id)
                        if(selectedData == i._id){
                            setShowDetails(false)
                            setSelectedData('')
                        }
                        }} className={styles.tableRow}>
                    <p>{update == i._id ? <input type="text" onChange={(e)=>setRfiNumber(e.target.value)} defaultValue={i.rfinumber} /> : i.rfinumber}</p>
                    <p>{i.taskId[0].name}</p>
                    <p>{i.taskprogress.From}</p>
                    <p>{i.taskprogress.To}</p>
                    <p>{i.taskprogress.side}</p>
                    <p>{i.typeoftask}</p>
                    <p>{moment(i.raisedonDate,"DD-MM-YYYY hh:mm:ss A").format('DD-MM-YYYY')}</p>
                    <p>{update == i._id ? <input type='time' onChange={(e)=>{
                                setRfiTime(moment(e.target.value,"HH-mm").format("hh:mm:ss A"))
                                }} defaultValue={i.raisedonTime} /> : i.raisedonTime}</p>
                    <p style={{color: i.status == 'Approve' ? "#2AC371" : i.status == 'Reject' ? '#D53539' : i.status == 'Pending' ? '#FA9601' : '#22242E'}}>{i.status == 'Approve' ? "Approved" : i.status == 'Reject' ? 'Rejected' : i.status == 'Pending' ? 'Pending' : ''}</p>
                    <p style={{display:'flex',alignItems:'center'}}><img onClick={()=>setUpdate(i._id)} src={updateicon} /> {update == i._id && <div><i onClick={()=>{
                                _updateRfi(i._id, i.rfinumber, i.raisedonTime)
                                }} className="fas fa-check"></i> <i onClick={()=>setUpdate('')} className="fas fa-times"></i></div>}</p>
                    {showDetails && <p className={styles.remarks}><span>Remarks: </span>{i.remarks}</p>}
                    </div> : taskId == 'All Tasks' ? <div onClick={()=>{
                    setShowDetails(true)
                    setSelectedData(i._id)
                    if(selectedData == i._id){
                        setShowDetails(false)
                        setSelectedData('')
                    }
                    }} className={styles.tableRow}>
                <p>{update == i._id ? <input onChange={(e)=>setRfiNumber(e.target.value)} defaultValue={i.rfinumber} /> : i.rfinumber}</p>
                <p>{i.taskId[0].name}</p>
                <p>{i.taskprogress.From}</p>
                <p>{i.taskprogress.To}</p>
                <p>{i.taskprogress.side}</p>
                <p>{i.typeoftask}</p>
                <p>{moment(i.raisedonDate,"DD-MM-YYYY hh:mm:ss A").format('DD-MM-YYYY')}</p>
                <p>{update == i._id ? <input type='time' onChange={(e)=>{
                            setRfiTime(moment(e.target.value,"HH-mm").format("hh:mm:ss A"))
                            }} defaultValue={i.raisedonTime} /> : i.raisedonTime}</p>
                <p style={{color: i.status == 'Approve' ? "#2AC371" : i.status == 'Reject' ? '#D53539' : i.status == 'Pending' ? '#FA9601' : '#22242E'}}>{i.status == 'Approve' ? "Approved" : i.status == 'Reject' ? 'Rejected' : i.status == 'Pending' ? 'Pending' : ''}</p>
                <p style={{display:'flex',alignItems:'center'}}><img onClick={()=>setUpdate(i._id)} src={updateicon} /> {update == i._id && <div><i onClick={()=>{
                            _updateRfi(i._id, i.rfinumber, i.raisedonTime)
                            }} className="fas fa-check"></i> <i onClick={()=>setUpdate('')} className="fas fa-times"></i></div>}</p>
                {showDetails && selectedData == i._id && <p className={styles.remarks}><span>Remarks: </span>{i.remarks}</p>}
                </div> : null
                })}
            </div>
        </div>
    </div>
}

export default RFI;