import { useState, useEffect,useContext } from "react"
import styles from "./Reports.module.css"
import axios from "axios"
import moment from "moment"
import SelectedProjectContext from "../context/selectdProjectContext";

let Reports = function() {

    const [machine, setMachine] = useState(true)
    const [material, setMaterial] = useState(false)
    const [cost, setCost] = useState(false)
    const [machineList, setMachineList] = useState([])
    const [profitList, setProfitList] = useState([])
    const [lossList, setLossList] = useState([])
    const [materialList, setMaterialList] = useState([])
    const {SelectedProject, setSelectedProject} = useContext(SelectedProjectContext)

    const _getMachineReport =  async() => {
      const options = {
        headers: {
            'Content-type': 'application/json',
            "Authorization": 'Bearer ' + localStorage.getItem('token')
        }
    };
    let res = await axios.get(process.env.REACT_APP_DURL + 'getallreading/' + SelectedProject,options)
    if(res.status == 200) {
      setMachineList(res.data);
      }
    }

    const _getMaterialReport =  async() => {
      const options = {
        headers: {
            'Content-type': 'application/json',
            "Authorization": 'Bearer ' + localStorage.getItem('token')
        }
    };
    let res = await axios.get(process.env.REACT_APP_DURL + 'getallmaterial/' + SelectedProject,options)
    if(res.status == 200) {
      setMaterialList(res.data.AllMaterial);
      }
    }

    const _getCostReport = async () => {
      const options = {
        headers: {
            'Content-type': 'application/json',
            "Authorization": 'Bearer ' + localStorage.getItem('token')
        }
    };
    let res = await axios.get(process.env.REACT_APP_DURL + 'gettaskdashboard/' + SelectedProject,options)
    if(res.status == 200) {
      setProfitList(res.data.green)
      setLossList(res.data.red)
      }
    }
    
    useEffect(()=>{
      _getMachineReport()
      _getCostReport()
      _getMaterialReport()
    },[SelectedProject])

    console.log(materialList)

    return <div className={styles.reports}>

        <div className={styles.header} id='toggle' style={{ backgroundColor: '#fff', borderRadius: '8px',width:'fit-content' }} >
        <button style={{backgroundColor: machine ? '#22242E' : '#fff',color: machine ? '#fff' : '#22242E'}}
         onClick={() => {
             setMachine(true) 
             setMaterial(false)
             setCost(false)
          }}>Machine Report</button>
         <button style={{backgroundColor: material ? '#22242E' : '#fff',color: material ? '#fff' : '#22242E'}}
         onClick={() => {
             setMaterial(true)
             setMachine(false)
             setCost(false)
          }}>Material Report</button>
         <button style={{backgroundColor: cost ? '#22242E' : '#fff',color: cost ? '#fff' : '#22242E'}}
         onClick={() => {
             setCost(true) 
             setMachine(false)
             setMaterial(false)
          }}>Cost Report</button>
        </div>

          {machine && <div className={styles.machineTable}>
            <div className={styles.tableHeader}>
                <p>S.No.</p>
                <p>Date</p>
                <p>Name</p>
                <p>Opening Reading</p>
                <p>Closing Reading</p>
                <p>Operational Hours</p>
            </div>
            <div className={styles.tableBody}>
              {machineList.map((machine,index) => {
                  return <div className={styles.tableRow}>
                  <p>{index+1}</p>
                  <p>{moment(machine.createdDate,'DD-MM-YYYY').format('DD-MM-YYYY')}</p>
                  <p>{machine.MachineName}</p>
                  <p>{machine.OpeningReading}</p>
                  <p>{machine.ClosingReading}</p>
                  <p>{(+machine.ClosingReading - +machine.OpeningReading).toFixed(3)}</p>
                </div>
              })}
            </div>
          </div>}

          {material && <div className={styles.materialTable}>
            <div className={styles.tableHeader}>
                <p>S.No.</p>
                <p>Date</p>
                <p>Machinery Name</p>
                <p>Unit</p>
                <p>Produced Quantity</p>
                <p>Dispatched Quantity</p>
                <p>Wastage</p>
            </div>
            <div className={styles.tableBody}>
              {materialList.map((material,index) => {
                  return <div className={styles.tableRow}>
                  <p>{index+1}</p>
                  <p>{moment(material.createdDate,'DD-MM-YYYY').format('DD-MM-YYYY')}</p>
                  <p>{material.ProductName}</p>
                  <p>{materialList.Unit}</p>
                  <p>{material.Quantity}</p>
                  <p>{material.QuantityDispatched}</p>
                  <p></p>
                </div>
              })}
            </div>
          </div>}

          {cost && <div className={styles.costTable}>
            <div className={styles.tableHeader}>
                <p>S.No.</p>
                <p>Task Name</p>
                <p>Planned Cost till now</p>
                <p>Actual Cost till now</p>
                <p>Profit/Loss</p>
            </div>
            <div className={styles.tableBody}>
            {lossList.map((task,index) => {
              return <div className={styles.tableRow}>
              <p>{index+1}</p>
              <p>{task.taskname}</p>
              <p><span className="rupee">₹</span>{task.PlannedCost}</p>
              <p><span className="rupee">₹</span>{task.ActualCost}</p>
              <p style={{color:'#D53539'}}><span className="rupee">₹</span>{+task.ActualCost - +task.PlannedCost}</p>
            </div>
            })}
            {profitList.map((task,index)=>{
              return  <div className={styles.tableRow}>
              <p>{lossList.length + (index+1)}</p>
              <p>{task.taskname}</p>
              <p><span className="rupee">₹</span>{task.PlannedCost}</p>
              <p><span className="rupee">₹</span>{task.ActualCost}</p>
              <p style={{color:'#2AC371'}}><span className="rupee">₹</span>{+task.PlannedCost - +task.ActualCost}</p>
            </div>
            })}
            </div>
          </div>}
    
    </div>
}

export default Reports