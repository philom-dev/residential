import styled from 'styled-components';

export let Card = styled.div`
padding: 20px;
padding: ${(props) => props.padding};
border-radius: 8px;
border-radius: ${(props) => props.borderRadius};
background-color: #fff;
background-color: ${(props) => props.color};
box-shadow: rgba(0, 0, 0, 0.18) 0px 2px 4px;
box-shadow: ${(props)=>props.shadow};
margin: 20px 0;
background: ${(props)=>props.gradient}; 
margin: ${(props)=>props.margin};
border: ${(props) => props.border};
`